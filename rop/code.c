#include <stdio.h>
#include <stdlib.h>

/*
#include <math.h>
#include <complex.h>
#include <ctype.h>
#include <errno.h>
#include <fenv.h>
#include <float.h>
#include <inttypes.h>
#include <iso646.h>
#include <limits.h>
#include <locale.h>
#include <setjmp.h>
#include <signal.h>
#include <stdalign.h>
#include <stdarg.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <tgmath.h>
#include <time.h>
#include <uchar.h>
#include <wchar.h>
#include <wctype.h>
*/

/**
 * The open_shell function is implemented so we can know what gadgets to look for
 * 	collectively, the gadgets will perform the same thing as the open_shell function
 */

// REFERENCES
// GCC STACK PROTECTION https://stackoverflow.com/questions/39737813/disabling-stack-protection-in-gcc-not-working
// GDB STACK	https://stackoverflow.com/questions/7848771/how-can-one-see-content-of-stack-with-gdb
// *ROP EXAMPLE	https://www.youtube.com/watch?v=a8_fDdWB2-M
// *ROPgadget	https://github.com/JonathanSalwan/ROPgadget/tree/master	
// X86 STACK	https://eli.thegreenplace.net/2011/09/06/stack-frame-layout-on-x86-64
// x86 PROGRAMMING GUIDE	https://www.nayuki.io/page/a-fundamental-introduction-to-x86-assembly-programming#5-memory-addressing-reading-writing
// ROP ASSIGNMENT	https://www.cs.virginia.edu/~cr4bd/4630/S2017/assignments/rop.html

// GDB COMMANDS
// START	gdb [program] 
// STACK FRAME	info frame
// DISASSEMBLE	disas [function] 
// STACK	info stack
// REGISTERS	info registers
// STACK FUNCTIONS AND ARGS	bt
// STACK MEMORY	x/100x $sp

// REGISTERS
// $rip	return instruction pointer
// $sp	stack pointer
// $$rbp	stack base pointer

// OBJDUMP
// objdump -d [program] | head -n 800

// STRACE

// NOTE
// comments do not change compiled binary function addresses
// do not need to worry about returning to main with correct stack
// 	if I can get a shell open, then the program will not complete
// 	until the shell exits

void open_shell(){
	system("/bin/bash");
}

int main(int argc, char **argv){
	open_shell();
	return(0);
}
