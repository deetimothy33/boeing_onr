#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * This program has a buffer overflow vulnerability.
 * Libraries are compiled into the binary.
 * The goal of the program will be to open a shell
 * 	this will be done using gadgets
 */

void vulnerable_function(char *data){
	char vulnerable_buffer[10];
	strcpy(vulnerable_buffer, data);
}

int main(int argc, char **argv){
	char vulnerable_buffer[10];
	
	strncpy(vulnerable_buffer, argv[1], 609);

	printf("%s", argv[1]);
	vulnerable_function(argv[1]);
	return(0);
}
