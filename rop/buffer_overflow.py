#! /bin/python

import os
import struct

##
# causes segmentation fault
# this is demonstrated by running in gdb
#
# this program allows for examination of the stack
##

# buffer overflow.
payload =  "A"*0x6c

#os.system("./rop.x \"%s\"" % payload)
os.system("gdb --args rop.x \"%s\"" % payload)
