#gdb --args rop.x `./exploit.py`
#gdb rop.x run < exploit_string.txt

##
# test the explot using gdb
#
# the gdb script is necessary because we can't pass null bytes in bash
##

# generate the exploit_string
./exploit.py > exploit_string.txt

# use gdb test script to run exploit string
#gdb -x test.gdb

# start rop.x, use the run command to give 
gdb rop.x
# run < exploit_string.txt
