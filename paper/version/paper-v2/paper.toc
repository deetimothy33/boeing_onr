\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {paragraph}{Contribution}{2}{section*.3}
\contentsline {paragraph}{Paper Organization}{2}{section*.4}
\contentsline {section}{\numberline {2}Related Work}{2}{section.2}
\contentsline {section}{\numberline {3}Core data structure}{3}{section.3}
\contentsline {section}{\numberline {4}Adversary Model}{3}{section.4}
\contentsline {subsection}{\numberline {4.1}Buffer Overflow}{3}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Return Orieanted Programming (ROP)}{4}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Code Injection}{4}{subsection.4.3}
\contentsline {section}{\numberline {5}Two-Level Predictor}{4}{section.5}
\contentsline {section}{\numberline {6}Tools}{4}{section.6}
\contentsline {subsection}{\numberline {6.1}Performance Monitoring Unit}{4}{subsection.6.1}
\contentsline {subsubsection}{\numberline {6.1.1}Linux {\tt perf}}{4}{subsubsection.6.1.1}
\contentsline {subsection}{\numberline {6.2}PIN Tool}{4}{subsection.6.2}
\contentsline {section}{\numberline {7}Implementation}{4}{section.7}
\contentsline {subsection}{\numberline {7.1}Data Generation}{4}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Two-Level Predictor}{4}{subsection.7.2}
\contentsline {section}{\numberline {8}Results}{4}{section.8}
\contentsline {subsection}{\numberline {8.1}Performance}{4}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Resources}{4}{subsection.8.2}
\contentsline {subsubsection}{\numberline {8.2.1}Memory}{4}{subsubsection.8.2.1}
\contentsline {subsubsection}{\numberline {8.2.2}Computation}{4}{subsubsection.8.2.2}
\contentsline {section}{\numberline {9}Conclusion}{4}{section.9}
