\contentsline {section}{\numberline {I}Introduction}{2}{section.1}
\contentsline {paragraph}{\numberline {\unhbox \voidb@x \hbox {I-}0a}Contribution}{2}{paragraph.1.0.0.1}
\contentsline {paragraph}{\numberline {\unhbox \voidb@x \hbox {I-}0b}Paper Organization}{2}{paragraph.1.0.0.2}
\contentsline {section}{\numberline {II}Related Work}{2}{section.2}
\contentsline {section}{\numberline {III}Adversary Model}{3}{section.3}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {III-A}}Buffer Overflow}{3}{subsection.3.1}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {III-B}}Return Oriented Programming (ROP)}{3}{subsection.3.2}
\contentsline {section}{\numberline {IV}Core Data Structure}{3}{section.4}
\contentsline {section}{\numberline {V}Tools}{4}{section.5}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {V-A}}Performance Monitoring Unit}{4}{subsection.5.1}
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {V-A}1}Linux {\tt perf}}{4}{subsubsection.5.1.1}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {V-B}}PIN Tool}{4}{subsection.5.2}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {V-C}}Machine Learning}{4}{subsection.5.3}
\contentsline {section}{\numberline {VI}Implementation}{4}{section.6}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {VI-A}}Data Generation}{4}{subsection.6.1}
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {VI-A}1}Fine-Grained (FG)}{4}{subsubsection.6.1.1}
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {VI-A}2}Coarse-Grained (CG)}{5}{subsubsection.6.1.2}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {VI-B}}Two-Level Prediction Structure}{5}{subsection.6.2}
\contentsline {section}{\numberline {VII}Results}{5}{section.7}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {VII-A}}ArduPilot Loop Iteration Independence}{5}{subsection.7.1}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {VII-B}}Accuracy}{5}{subsection.7.2}
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {VII-B}1}Fine-Grained}{5}{figure.6}
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {VII-B}2}Coarse-Grained}{6}{subsubsection.7.2.2}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {VII-C}}Memory}{6}{subsection.7.3}
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {VII-C}1}Fine-Grained}{6}{subsubsection.7.3.1}
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {VII-C}2}Coarse-Grained}{6}{subsubsection.7.3.2}
\contentsline {subsection}{\numberline {\unhbox \voidb@x \hbox {VII-D}}Computation Time}{6}{subsection.7.4}
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {VII-D}1}Fine-Grained}{6}{subsubsection.7.4.1}
\contentsline {subsubsection}{\numberline {\unhbox \voidb@x \hbox {VII-D}2}Coarse-Grained}{6}{subsubsection.7.4.2}
\contentsline {section}{\numberline {VIII}Conclusion}{6}{section.8}
\contentsline {section}{\numberline {IX}EXTRA}{6}{section.9}
\contentsline {section}{References}{7}{section*.2}
