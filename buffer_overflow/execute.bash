#! /bin/bash

PMU_MODULE_DIR=../track_pid/pmu-module
BUFFER_OVERFLOW_DIR=../../buffer_overflow

PMU_TEST=../track_pid/pmu-module/pmu.x
CONTROL_LOOP_OVERFLOW=./control_loop.x

# check for number of arguments > 0
if [ $# -lt "1" ]; then
	echo "USAGE"
	echo "./execute [#] [n]"
	echo "---"
	echo "[#] is one of the following:"
	echo "[0] pmu-module/pmu.x (test)"
	echo "[1] log control_loop_overflow"
	echo "---"
	echo "[n] is the number of times to run [#]"
	echo "---"
	exit
fi

#TODO do any of the setup steps affect PERF?
#	I don't think they should, but there is potential there.

# perform system setup
echo "SETUP"
cd $PMU_MODULE_DIR
sudo ./disable_nmi_watchdog.bash
sudo ./enable_rdpmc.bash
sudo modprobe msr

# load, test, unload module
echo "LOAD AND TEST LOAD SUCCESS"
sudo ./load.bash

echo "EXECUTE USERSPACE PROGRAM"
cd $BUFFER_OVERFLOW_DIR
for i in `seq 1 $2`; do
	if [ $1 -eq 0 ]; then
		# for testing whether it is working
		# note pmu.x expects working director to be `pmu-module/`
		cd $PMU_MODULE_DIR
		sudo nice -n -20 ./pmu.x	
	elif [ $1 -eq 1 ]; then
		#echo $(pwd)
		sudo $CONTROL_LOOP_OVERFLOW
	fi
done 

echo "UNLOAD MODULE"
cd $PMU_MODULE_DIR
sudo ./unload.bash
