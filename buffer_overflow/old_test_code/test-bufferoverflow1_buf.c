/*

mybuf-attack  overflows datastring buffer and executes the attackcode
mybuf-normal is correct size input
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void bufferunderattack(char *data)
{

char datastring[10];

strcpy(datastring, data);

//printf("datastring is %s\n", datastring);

}

void attackcode ()
{

printf("you are being attacked\n");

exit(0);

}


int main(int argc, char **argv)
{

  char mybuf_attack[] = {0x41,0x42, 0x43, 0x44, 0x45,0x46,0x47,0x48, 0x49, 0x4a, //data byte
			 0x4b,0x4c, 0x4d, 0x4e, 0x4f,0x50,0x51, 0x52,0x53, 0x54, 
		         0x41,0x41, 0x41, 0x41,
			 0xd8,0x05,0x40, 0x00,0x00, 0x00, 0x00, 0x00}; //return address 0x00000000004005d8 -- attackcode
                                                                       // CHANGE THIS 8 bytes !!! to return address in your machine

 char mybuf_normal[10] = {0x41, 0x41, 0x41,0x41, 0x0};


 bufferunderattack(mybuf_attack);

 //puts(mybuf_normal);

return(0);

}


