#include "pmu.h"

void log_pmu(char* file_name){
	FILE *file;
	//pmc_value_t pmc;

	file=fopen(file_name,"w");

	// read counters by reading device
	// 	this retrieves the counter values stored in the kernel
	// 	they are retrieved in order
	for(int i=0; i<read_buffer_size; i++){
#if USE_PERF
		fprintf(file,"%lld\n",read_buffer[i]);
#else
		fprintf(file,"%lld\n",read_buffer[i]);
		
		//TODO figure out why reading them all at the end doesn't work
		//	it works in other programs
		//	mabe kernel memory is getting cleared by something else?
		//	BUT that wouldn't explain why it works in PIN tool
		//read(file_desc, &pmc, sizeof(pmc_value_t));
		//print_pmc_file(stdout,&pmc);
		//print_pmc_file(file,&pmc);
#endif
	}

	fclose(file);
}

#if USE_PERF

// use perf to start,stop,read pmu
	
struct perf_event_attr pe;
int fd;

static long perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
		int cpu, int group_fd, unsigned long flags)
{
	int ret;

	ret = syscall(__NR_perf_event_open, hw_event, pid, cpu,
			group_fd, flags);
	return ret;
}

void enable_pmu(){
	// set all bytes of struct to 0
	memset(&pe, 0, sizeof(struct perf_event_attr));
	pe.type = PERF_TYPE_HARDWARE;
	pe.size = sizeof(struct perf_event_attr);
	pe.config = PERF_COUNT_HW_INSTRUCTIONS;
	pe.disabled = 1;
	pe.exclude_kernel = 1;
	pe.exclude_hv = 1;
	fd = perf_event_open(&pe, 0, -1, -1, 0);
	if (fd == -1) {
		fprintf(stderr, "Error opening leader %llx\n", pe.config);
		exit(EXIT_FAILURE);
	}
}
void disable_pmu(){
	close(fd);	
}
void start_pmu(){
	ioctl(fd, PERF_EVENT_IOC_RESET, 0);
	ioctl(fd, PERF_EVENT_IOC_ENABLE, 0);
}
void stop_pmu(){
	ioctl(fd, PERF_EVENT_IOC_DISABLE, 0);
}
void read_pmu(){
	//TODO where to store the value of the read?
	//	the eventual logging will need to take place.
	//
	// Thinking ahead, the pyp will be trained upon a read
	// 	it doesn't make sence to spend a bunch of time on a complexe storage system
	// 	a staticly sized buffer may do the trick
	read(fd, read_buffer+read_buffer_size, sizeof(long long));
	//printf("%lld\n", read_buffer[read_buffer_size]);
	read_buffer_size++;
}

#else

// direct PMU access

void enable_pmu(){
}
void disable_pmu(){
}
void start_pmu(){
	file_desc = open(KERNEL_MODULE_DEVICE, 0);
	if (file_desc < 0) {
		printf("Can't open device file: %s\n", KERNEL_MODULE_DEVICE);
		exit(-1);
	}
	ioctl_start_counters(file_desc, msg);
	
	//printf("fd: %d\n",file_desc);
}
void stop_pmu(){
	ioctl_stop_counters(file_desc, msg);
	ioctl_stop_counters(file_desc, msg);
	close(file_desc);
}
void read_pmu(){
	// read the counters
	// the kernel will store the values
	ioctl_read_counters(file_desc, msg);
	
	// TODO figure out why
	// for some reason reading the coutners at the end doesn't work
	// 	but reading them immediatly does
	pmc_value_t pmc;
	read(file_desc, &pmc, sizeof(pmc_value_t));
	read_buffer[read_buffer_size]=pmc.fc0;	
	read_buffer_size++;
	
	// DEBUG
	//print_pmc_file(file,&pmc);
	//printf("%d\n",file_desc);
}

#endif


