#! /bin/Rscript

# create a log file containing
# (asm instruction),{counter difference-pmu_overhead


LOG_FILE="../pin_tracker/log/4_8687"
LOG_FILE="test.csv"
PMU_OVERHEAD_FILE="../pin_tracker/log/pmu_overhead"

source('difference.r')


data <- read.csv(file=LOG_FILE, header=FALSE, sep=",");
print(data)
quit()
data <- difference_data(data)
write.csv(data, file="difference.csv")

#cat(paste("PMU COUNTER READ STATISTICS (mu, sigma) n=",length(data[[1]])),file="out/stat.txt",sep="\n")
#cat(paste("measurement time (ns) (",mean(data[[1]]),",",sd(data[[1]]),")"),file="out/stat.txt",sep="\n",append=TRUE)
