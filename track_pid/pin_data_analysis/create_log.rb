#! /bin/ruby

# create a log file containing
# (asm instruction),{counter difference-pmu_overhead


LOG_FILE="../pin_tracker/log/4_8687"
#LOG_FILE="test.csv"
PMU_OVERHEAD_FILE="../pin_tracker/log/pmu_overhead"
OUT_FILE="difference.csv"


#TODO 
# differences are not what they are expected to be -- investigate
#TODO


def difference(l0, l1)
	# compute l1-l0
	# assume each element of the line is a number
	# input is 2 arrays with same number of elements
	# return is an array
	difference_a=Array.new
	(0..l0.length()-1).each do |i|
		difference_a[i]=l1[i].to_i-l0[i].to_i
	end
	return difference_a
end

def compute_overhead
	# compute the average overhead for each counter
	l0=nil
	line_n=0
	sum_a=Array.new
	File.open(PMU_OVERHEAD_FILE,'r') do |f|
		f.each_line do |l|
			# sum the values at each index of the array
			# divide by the total number of lines
			j=0
			l.chomp().split(',').each do |i|
				if sum_a[j].nil?
					sum_a[j]=0
				end
				sum_a[j]+=i.to_i
				j+=1
			end
			line_n+=1
		end	
	end
				
	#TODO the overhead seems too high
	#	return 0's instead

	# compute the average
	j=0
	sum_a.each do |i|
		sum_a[j]=(i.to_f/line_n).to_i
		#sum_a[j]=0
		j+=1
	end

	return sum_a
end

def main
	overhead=([0,0] << compute_overhead()).flatten!

	l0=nil
	out_file=File.new(OUT_FILE,'w')
	File.open(LOG_FILE,'r') do |f|
		f.each_line do |l|
			# remove the assembly instruction from the beginning of the line
			l_a=l.split('),');
			l_a[0]+=')'
			l_a[1]=l_a[1].chomp().split(',')
			if not l0.nil?
				# output the difference between consecutive lines minus read overhead
				# compute l1-l0-overhead
				d_a=difference(l0,l_a[1])
				d_a=difference(overhead,d_a)

				#print("#{l_a[1]}\n")
				#print("#{l0}\n")
				#print("#{overhead}\n")
				#print("#{d_a}\n")
				#print("\n")

				out_file.write("#{l_a[0]}")
				d_a.each do |i|
					out_file.write(",#{i}")
				end
				out_file.write("\n")
			end
			l0=l_a.nil? ? Array.new : l_a[1]
		end
	end
	out_file.close()
end

main
