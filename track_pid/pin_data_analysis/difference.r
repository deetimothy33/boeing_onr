 #! /usr/bin/Rscript

# provides a function to take the difference
# between successive data set lines.
#
# other files will source(differnece.r)
# in order to use this function

difference_data<-function(data){
	# subtract each row from the previous row
	# this gives the difference in time and counter measurmenets
	# from reading to reading
	ddata<-data
	for (i in 1:length(data[[1]])) {
		for (j in 1:length(data)) { 
			if(i==1){
				# first row should get set to 0	
				ddata[[j]][i]<-0
			}else{
				# asm instruction is column 1, 
				# test for time
				if(j==1){
					# time increases monotonically from 0 for counter values
					ddata[[j]][i]<-data[[j]][i]-data[[j]][1]
				}else{
					# otherwise take the difference
					# current row - last row
					ddata[[j]][i]<-data[[j]][i]-data[[j]][i-1]
				}
			}
		}
	}
	data<-ddata

	return(data)
}
