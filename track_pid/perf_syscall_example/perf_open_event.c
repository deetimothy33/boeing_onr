// EXAMPLE GIVEN BY ANANDA

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/perf_event.h>
#include <asm/unistd.h>

static long perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
		int cpu, int group_fd, unsigned long flags)
{
	int ret;

	ret = syscall(__NR_perf_event_open, hw_event, pid, cpu,
			group_fd, flags);
	return ret;
}

#define MSIZE 500

/*Initialize Matrices*/
int matrix_a[MSIZE][MSIZE],matrix_b[MSIZE][MSIZE],matrix_r[MSIZE][MSIZE];
void initialize_matrices()
{
	int i, j;
	for (i = 0; i < MSIZE; i++) {
		for (j = 0; j < MSIZE; j++) {
			matrix_a[i][j] = 1;
			matrix_b[i][j] = 1;
			matrix_r[i][j] = 0;
		}
	}
}

int main(int argc, char **argv)
{
	struct perf_event_attr pe;
	long long count;
	int fd, i, j, k;

	// set all bytes of struct to 0
	memset(&pe, 0, sizeof(struct perf_event_attr));
	pe.type = PERF_TYPE_HARDWARE;
	pe.size = sizeof(struct perf_event_attr);
	pe.config = PERF_COUNT_HW_INSTRUCTIONS;
	pe.disabled = 1;
	pe.exclude_kernel = 1;
	pe.exclude_hv = 1;
	fd = perf_event_open(&pe, 0, -1, -1, 0);
	if (fd == -1) {
		fprintf(stderr, "Error opening leader %llx\n", pe.config);
		exit(EXIT_FAILURE);
	}
	initialize_matrices();

	ioctl(fd, PERF_EVENT_IOC_RESET, 0);
	ioctl(fd, PERF_EVENT_IOC_ENABLE, 0);

	/* Place payload code here*/

	ioctl(fd, PERF_EVENT_IOC_DISABLE, 0);
	read(fd, &count, sizeof(long long));
	printf("%lld\n", count);

	close(fd);
}
