/**
 * inline asm to use
 * 	wdmsr
 * 	rdmsr
 * to
 * 	set up pmu
 * 	read events from pmu
 */

#include "types.h"
#include "pmc.h"
#include <asm/msr.h>

//#define USE_PERF

/**
 * wrmsr and rdmsr
 */
/*
inline uint64_t rdmsr(uint32_t msr_id){
    uint64_t msr_value;
    __asm__ volatile ( "rdmsr" : "=A" (msr_value) : "c" (msr_id) );
    return msr_value;
}

inline void wrmsr(uint32_t msr_id, uint64_t msr_value){
    asm volatile ( "wrmsr" : : "c" (msr_id), "A" (msr_value) );
}
*/


/**
 * setup lbr
 */
static inline void 
pmu_setup_lbr(void){
	struct DebugCtlRegister dcr;
	struct LBRSelectRegister lsr;

	// read current value
	rdmsrl(IA32_DEBUGCTL, dcr.value);
	rdmsrl(LBR_SELECT, lsr.value);

	// mask bits to enable everything
	dcr.fields.lbr = 1;
	dcr.fields.frz_lbrs_on_pmi = 0;

	// clear lbrselect register (no filtering)
	lsr.fields.cpl_eq_0 = 0;
	lsr.fields.cpl_neq_0 = 0;
	lsr.fields.jcc = 0;
	lsr.fields.near_rel_call = 0;
	lsr.fields.near_ind_call = 0;
	lsr.fields.near_ret = 0;
	lsr.fields.near_ind_jmp = 0;
	lsr.fields.near_rel_jmp = 0;
	lsr.fields.far_branch = 0;
	
	// write new value of global control register
	wrmsrl(IA32_DEBUGCTL, dcr.value);
	wrmsrl(LBR_SELECT, lsr.value);
}

/**
 * read counters
 */

static inline unsigned long long
pmu_read_counter(unsigned long long counter){
	unsigned long long value;
	//rdmsrl(counter, value);
	value = __rdmsr(counter);
	return value;
}

static inline void
pmu_read_fc0(pmc_value_t* pmc){
	pmc->fc0 = pmu_read_counter(IA32_PMC0);
}
static inline void
pmu_read_fc1(pmc_value_t* pmc){
	pmc->fc1 = pmu_read_counter(IA32_PMC0);
}
static inline void
pmu_read_fc2(pmc_value_t* pmc){
	pmc->fc2 = pmu_read_counter(IA32_PMC0);
}

static inline void
pmu_read_pc0(pmc_value_t* pmc){
	pmc->pc0 = pmu_read_counter(IA32_PMC0);
}
static inline void
pmu_read_pc1(pmc_value_t* pmc){
	pmc->pc1 = pmu_read_counter(IA32_PMC0);
}
static inline void
pmu_read_pc2(pmc_value_t* pmc){
	pmc->pc2 = pmu_read_counter(IA32_PMC0);
}
static inline void
pmu_read_pc3(pmc_value_t* pmc){
	pmc->pc3 = pmu_read_counter(IA32_PMC0);
}


/**
 * enable / disable
 */

static inline void 
pmu_enable(void){
	unsigned long long gctl, mask;
	
	rdmsrl(IA32_CR_PERF_GLOBAL_CTRL, gctl);

	// mask bits to disable everything
	mask = 0x000000070000000F;	 
	gctl |= mask;
	
	// write to disable
	wrmsrl(IA32_CR_PERF_GLOBAL_CTRL, gctl);
}

static inline void 
pmu_disable(void){
	unsigned long long gctl, mask;
	
	rdmsrl(IA32_CR_PERF_GLOBAL_CTRL, gctl);

	// mask bits to disable everything
	mask = 0x000000070000000F;	 
	gctl &= ~mask;
	
	// write to disable
	wrmsrl(IA32_CR_PERF_GLOBAL_CTRL, gctl);
}

// static PEBS buffer writen into by PEBS hardware
// note that this buffer will become disable after
// PEBS index reaches PEBS Abs Max
// it is not a circular buffer
#define PEBS_RECORD_SIZE 16
static pebs_record_t pebs_record[PEBS_RECORD_SIZE];

static inline void 
pebs_enable(void){
	ds_area_t ds_area;
	unsigned long long gctl, mask;
	
	rdmsrl(IA32_PEBS_ENABLE, gctl);
	mask = 0x000000000000000F;	 
	//mask = 0x0000000F0000000F;	 
	gctl |= mask;
	wrmsrl(IA32_PEBS_ENABLE, gctl);

	// setup ds_area
	unsigned long long counter_max = 0xFFFFFFFFFFFFFFFF; // 64 1's

	ds_area.pebs_buffer_base = (unsigned long long int)&pebs_record;
	ds_area.pebs_index = (unsigned long long int)&pebs_record;
	ds_area.pebs_abs_max = (unsigned long long int)&pebs_record + sizeof(pebs_record_t)*PEBS_RECORD_SIZE;
	ds_area.pebs_int_thresh = 0;
	ds_area.pebs_counter_0_reset = counter_max - 100;
	ds_area.pebs_counter_1_reset = counter_max - 100;
	ds_area.pebs_counter_2_reset = counter_max - 100;
	ds_area.pebs_counter_3_reset = counter_max - 100;

	//TODO these may need to increment by 0x8 instead of 0x1
	// write relevent fields of ds_area
	wrmsrl(IA32_DS_AREA + 4, ds_area.pebs_buffer_base);
	wrmsrl(IA32_DS_AREA + 5, ds_area.pebs_index);
	wrmsrl(IA32_DS_AREA + 6, ds_area.pebs_abs_max);
	wrmsrl(IA32_DS_AREA + 7, ds_area.pebs_int_thresh);
	wrmsrl(IA32_DS_AREA + 8, ds_area.pebs_counter_0_reset);
	wrmsrl(IA32_DS_AREA + 9, ds_area.pebs_counter_1_reset);
	wrmsrl(IA32_DS_AREA + 10, ds_area.pebs_counter_2_reset);
	wrmsrl(IA32_DS_AREA + 11, ds_area.pebs_counter_3_reset);
}

static inline void 
pebs_disable(void){
	unsigned long long gctl, mask;
	
	rdmsrl(IA32_PEBS_ENABLE, gctl);
	// 0x1111 in [35:32] captures load latency information (on overflow)
	// 0x1111 in [3:0] captures machine state (on overflow)
	mask = 0x000000000000000F;	 
	//mask = 0x0000000F0000000F;	 
	gctl &= ~mask;
	wrmsrl(IA32_PEBS_ENABLE, gctl);
}

/**
 * start and stop counters
 */

static inline void 
pmu_start_fixed_counters(void){
	struct FixedEventControlRegister fec_reg;
	unsigned long long fctl;
	rdmsrl(IA32_CR_FIXED_CTR_CTRL, fctl);
	
	fec_reg.value = fctl;
	fec_reg.fields.os0 = 0b1; // count events at privledge level 0
	fec_reg.fields.usr0 = 0b1; // count events at privledge level 1,2,3
	fec_reg.fields.any_thread0 = 0b0; // if clear, only count events on own thread
	fec_reg.fields.enable_pmi0 = 0b0; // if clear, overflow interrupts disabled
	fec_reg.fields.os1 = 0b1;
	fec_reg.fields.usr1 = 0b1;
	fec_reg.fields.any_thread1 = 0b0;
	fec_reg.fields.enable_pmi1 = 0b0;
	fec_reg.fields.os2 = 0b1;
	fec_reg.fields.usr2 = 0b1;
	fec_reg.fields.any_thread2 = 0b0;
	fec_reg.fields.enable_pmi2 = 0b0;
	
	fctl = fec_reg.value;
	wrmsrl(IA32_CR_FIXED_CTR_CTRL, fctl);
}

static inline void 
pmu_stop_fixed_counters(void){
	// don't need to do this, 
	// disabling at global ctl will accomplish this
}

static inline void 
pmu_start_programmable_counter(int j, unsigned char event, unsigned char umask){
	struct EventSelectRegister r;

	// read current value
	rdmsrl(IA32_PERFEVTSEL0_ADDR + j, r.value);

	// event to be counted
	r.fields.event_select = event;
	r.fields.umask = umask;

	r.fields.usr = 1; // count usermode events
	r.fields.os = 1; // count kernelmode events
	r.fields.edge = 1; // increments on de-asserted -> asserted event transition
	//r.fields.pin_control; reserved pin
	r.fields.apic_int = 0; // generate exception on overflow
	r.fields.any_thread = 0; // increment on any thread event
	r.fields.enable = 1; // enable counting
	r.fields.invert = 0; // invert when interrupt is generated by cmask
	r.fields.cmask = 0; // determines a count value on which overflow is generated
	//r.fields.in_tx = 0; reserved
	//r.fields.in_txcp = 0; reserved 
	//r.fields.reservedX = 0;reserved 

	// write
	wrmsrl(IA32_PERFEVTSEL0_ADDR + j, r.value);
}

static inline void 
pmu_stop_programmable_counter(int j){
	struct EventSelectRegister r;

	// read current value
	rdmsrl(IA32_PERFEVTSEL0_ADDR + j, r.value);

	// disable counting 
	r.fields.enable = 0;

	// write
	wrmsrl(IA32_PERFEVTSEL0_ADDR + j, r.value);
}


// decide between PERF or DIRECT PMU MSR READ
// NOTE: these methods are called from KERNEL SPACE
#ifdef PERF_SYSTEM_CALL

// the list of system calls
// used in making perf system calls
// includes __NR_perf_event_open
#include <linux/unistd.h>
#include <linux/perf_event.h>
#include <linux/syscalls.h>
//#include <asm/paravirt.h>

//TODO sys_call_table may not actually be exported
//	one solution is to locate it
void **sys_call_table;
asmlinkage int (*perf_event_open) (struct perf_event_attr *hw_event, pid_t pid,int cpu, int group_fd, unsigned long flags);
asmlinkage int (*close)(int);
asmlinkage int (*read)(int,void*,size_t);
asmlinkage int (*ioctl)(int,unsigned long,...);

// returned from perf syscall
int fd;
bool sys_call_setup=false;

// get the address of the sys_call_table
static unsigned long **aquire_sys_call_table(void){
	unsigned long int offset = PAGE_OFFSET;
	unsigned long **sct;

	while (offset < ULLONG_MAX) {
		sct = (unsigned long **)offset;

		if (sct[__NR_close] == (unsigned long *) sys_close) 
			return sct;

		offset += sizeof(void *);
	}
	
	return NULL;
}

// set up system calls needed to start and stop and read
// counters with perf
static void hook_sys_call_table(void){
	// find the location of sys_call_table
	sys_call_table=(void **)aquire_sys_call_table();
	
	// set up system calls
	perf_event_open=sys_call_table[__NR_perf_event_open];
	read=sys_call_table[__NR_read];
	close=sys_call_table[__NR_close];
	ioctl=sys_call_table[__NR_ioctl];
}

static void
pmu_start_all_counters(void){
	struct perf_event_attr pe;
	pid_t pid=0;
	int cpu=-1;
	int group_fd=-1;
	unsigned long flags=0;
	
	// THIS TURNED OUT TO BE UNNECESSARY
	// <linux/syscalls.h> contains all necessary calls
	// set up system calls
	if(!sys_call_setup){
		hook_sys_call_table();
		sys_call_setup=true;
	}	
	
	// set up system calls
	// (mutually exclusive with the above hook_sys_call_table())
	//perf_event_open=(void *)sys_perf_event_open;
	//close=(void *)sys_close;
	//read=(void *)sys_read;
	//ioctl=(void *)sys_ioctl;

	// set all bytes of struct to 0
	memset(&pe, 0, sizeof(struct perf_event_attr));
	pe.type = PERF_TYPE_HARDWARE;
	pe.size = sizeof(struct perf_event_attr);
	pe.config = PERF_COUNT_HW_INSTRUCTIONS;
	pe.disabled = 1;
	pe.exclude_kernel = 1;
	pe.exclude_hv = 1;
	
	// perform the syscall
	fd=perf_event_open(&pe, pid, cpu, group_fd, flags);
	// error check
	if(fd<0) return;

	//TODO use and ARRAY OF FD to get more events
	
	// ioctl calls are replaced with direct calls to perf_ioctl 
	// defined in kernel/events/core.c

	//ioctl(fd, PERF_EVENT_IOC_RESET, 0);
	ioctl(fd, PERF_EVENT_IOC_RESET, 0);
	
	//ioctl(fd, PERF_EVENT_IOC_ENABLE, 0);
	ioctl(fd, PERF_EVENT_IOC_ENABLE, 0);
}

static void
pmu_stop_all_counters(void){
	//ioctl(fd, PERF_EVENT_IOC_DISABLE, 0);
	ioctl(fd, PERF_EVENT_IOC_DISABLE, 0);
	
	//TODO
	
	// close the file descriptor
	close(fd);
}

static inline void
pmu_read_all_counters(pmc_value_t* pmc){
	//read(fd, &count, sizeof(long long));
	long long count;
	read(fd, &count, sizeof(long long));
	//TODO
}

static inline unsigned long long
pmu_read_lbr(lbr_t *lbr){
	//TODO
}


#else


static void
pmu_start_all_counters(void){
	pmu_enable();

	pmu_start_fixed_counters();

	pmu_start_programmable_counter(0, BR_INST_RETIRED, BR_INST_RETIRED_ALL_MASK);
	pmu_start_programmable_counter(1, UOPS_RETIRED, UOPS_RETIRED_ALL_MASK);
	pmu_start_programmable_counter(2, INST_RETIRED, INST_RETIRED_ALL_MASK);
	pmu_start_programmable_counter(3, RAT_STALLS, RAT_STALLS_ALL_MASK);

	//pmu_start_programmable_counter(3, INST_RETIRED, INST_RETIRED_MMX_MASK);
	//pmu_start_programmable_counter(1, MEM_INST_RETIRED, MEM_INST_RETIRED_LOAD_MASK);
	//pmu_start_programmable_counter(2, MEM_INST_RETIRED, MEM_INST_RETIRED_STORE_MASK);
	//pmu_start_programmable_counter(1, BR_MISP_RETIRED, BR_MISP_RETIRED_ALL_MASK);
	//pmu_start_programmable_counter(0, MEM_LOAD_RETIRED_L3_MISS_EVTNR, MEM_LOAD_RETIRED_L3_MISS_UMASK);
	//pmu_start_programmable_counter(0, ARCH_LLC_MISS_EVTNR, ARCH_LLC_MISS_UMASK);
	//pmu_start_programmable_counter(1, MEM_LOAD_RETIRED_L2_HIT_EVTNR, MEM_LOAD_RETIRED_L2_HIT_UMASK);
	//pmu_start_programmable_counter(3, ITLB_MISS_RETIRED, ITLB_MISS_RETIRED_MASK);
	//pmu_start_programmable_counter(3, ITLB_MISS_RETIRED, ITLB_MISS_RETIRED_MASK);
	//pmu_start_programmable_counter(3, FP_ASSISTS, FP_ASSISTS_ALL_MASK);
}

static void
pmu_stop_all_counters(void){
	pmu_stop_fixed_counters();
	pmu_stop_programmable_counter(0);
	pmu_stop_programmable_counter(1);
	pmu_stop_programmable_counter(2);
	pmu_stop_programmable_counter(3);

	pmu_disable();
}

static inline void
pmu_read_all_counters(pmc_value_t* pmc){
	pmc->fc0 = pmu_read_counter(INST_RETIRED_ANY_ADDR);
	pmc->fc1 = pmu_read_counter(CPU_CLK_UNHALTED_THREAD_ADDR);
	pmc->fc2 = pmu_read_counter(CPU_CLK_UNHALTED_REF_ADDR);
	pmc->pc0 = pmu_read_counter(IA32_PMC0);
	pmc->pc1 = pmu_read_counter(IA32_PMC1);
	pmc->pc2 = pmu_read_counter(IA32_PMC2);
	pmc->pc3 = pmu_read_counter(IA32_PMC3);
}

/**
 * read lbr, get branch history
 */
static inline unsigned long long
pmu_read_lbr(lbr_t *lbr){
	//TODO test that LBR is read correctly
	
	// determine where to begin reading
	rdmsrl(MSR_LASTBRANCH_TOS, lbr->tos.value);

	int i;
	for(i=0; i<16; i++){ 
		// !!! misprediction bit is in the from register
		rdmsrl(MSR_LASTBRANCH_FROM_IP + i, (lbr->from)[i].value); 
		rdmsrl(MSR_LASTBRANCH_TO_IP + i, (lbr->to)[i].value); 
	}	
}

#endif
