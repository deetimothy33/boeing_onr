#include "pmuctl.h"

/**
 * test sequential read
 */
int ioctl_sequential_read(int file_desc){
	ioctl(file_desc, IOCTL_SEQUENTIAL_READ, getpid());
	return 0;
}

/**
 * set RT prioirty of this thread
 */
int ioctl_set_rt_priority(int file_desc){
	int ret_val = ioctl(file_desc, IOCTL_SET_RT_PRIORITY, 11);

	if (ret_val < 0) {
		printf("ioctl_set_msg failed:%d\n", ret_val);
		exit(-1);
	}

	return ret_val;
}

/**
 * reschedule tasks other than this one and the kernel process
 */
int ioctl_reschedule_other(int file_desc){
	int ret_val;

	ret_val = ioctl(file_desc, IOCTL_RESCHEDULE_OTHER, getpid());

	if (ret_val < 0) {
		printf("ioctl_set_msg failed:%d\n", ret_val);
		exit(-1);
	}

	return ret_val;
}

/**
 * start / stop the pmu counters
 */
int ioctl_read_counters(int file_desc, char *message)
{
	return ioctl(file_desc, IOCTL_READ_COUNTERS, message);
}

int ioctl_read_pc_lbr_counters(int file_desc, char *message)
{
	return ioctl(file_desc, IOCTL_READ_PC_LBR_COUNTERS, message);
}

int ioctl_start_counters(int file_desc, char *message)
{
	int ret_val;

	ret_val = ioctl(file_desc, IOCTL_START_COUNTERS, message);

	if (ret_val < 0) {
		printf("ioctl_set_msg failed:%d\n", ret_val);
		exit(-1);
	}

	return ret_val;
}

int ioctl_stop_counters(int file_desc, char *message)
{
	int ret_val;

	ret_val = ioctl(file_desc, IOCTL_STOP_COUNTERS, message);

	if (ret_val < 0) {
		printf("ioctl_set_msg failed:%d\n", ret_val);
		exit(-1);
	}

	return ret_val;
}

/**
 * print current asm instruction and PMU counters
 */
int ioctl_get_inst_counters(int file_desc, char *message)
{
	int ret_val;

	ret_val = ioctl(file_desc, IOCTL_GET_INST_COUNTERS, message);

	if (ret_val < 0) {
		printf("ioctl_set_msg failed:%d\n", ret_val);
		exit(-1);
	}

	return ret_val;
}

/* 
 * Function will print "Genuine Intel" for Intel CPUs in kernel log message --swamy
 */
int ioctl_detect_cpu(int file_desc, char *message)
{
	int ret_val;

	ret_val = ioctl(file_desc, IOCTL_GET_PMU_CAPABILITY, message);

	if (ret_val < 0) {
		printf("ioctl_set_msg failed:%d\n", ret_val);
		exit(-1);
	}

	return ret_val;
}

int ioctl_set_msg(int file_desc, char *message)
{
	int ret_val;

	ret_val = ioctl(file_desc, IOCTL_SET_MSG, message);

	if (ret_val < 0) {
		printf("ioctl_set_msg failed:%d\n", ret_val);
		exit(-1);
	}

	return ret_val;
}

int ioctl_get_msg(int file_desc)
{
	int ret_val;
	char message[100];

	/* 
	 * Warning - this is dangerous because we don't tell
	 * the kernel how far it's allowed to write, so it
	 * might overflow the buffer. In a real production
	 * program, we would have used two ioctls - one to tell
	 * the kernel the buffer length and another to give
	 * it the buffer to fill
	 */
	ret_val = ioctl(file_desc, IOCTL_GET_MSG, message);

	if (ret_val < 0) {
		printf("ioctl_get_msg failed:%d\n", ret_val);
		exit(-1);
	}

	printf("get_msg message:%s\n", message);

	return ret_val;
}

int ioctl_get_nth_byte(int file_desc)
{
	int i;
	char c;

	printf("get_nth_byte message:");

	i = 0;
	do {
		c = ioctl(file_desc, IOCTL_GET_NTH_BYTE, i++);

		if (c < 0) {
			printf
			    ("ioctl_get_nth_byte failed at the %d'th byte:\n",
			     i);
			exit(-1);
		}

		putchar(c);
	} while (c != 0);
	putchar('\n');

	return 0;
}

// test print counter values
void print_pmc(pmc_value_t* pmc){
	printf("%lld,%lld,%lld,%lld,%lld,%lld,%lld\n", 
			pmc->fc0, pmc->fc1, pmc->fc2, pmc->pc0, pmc->pc1, pmc->pc2, pmc->pc3);
}

// test print counter values
void print_pmc_file(FILE *fd, pmc_value_t* pmc){
	fprintf(fd,"%lld,%lld,%lld,%lld,%lld,%lld,%lld\n", 
			pmc->fc0, pmc->fc1, pmc->fc2, pmc->pc0, pmc->pc1, pmc->pc2, pmc->pc3);
}
