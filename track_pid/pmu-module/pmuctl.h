#ifndef IOCTL_H
#define IOCTL_H

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>		/* open */
#include <unistd.h>		/* exit */
#include <sys/ioctl.h>		/* ioctl */

#include "pmu-device2.h"
#include "pmc.h"

/**
 * functions for utilizing pmu-device2
 */

/**
 * test sequential read
 */
int ioctl_sequential_read(int file_desc);

/**
 * set RT prioirty of this thread
 */
int ioctl_set_rt_priority(int file_desc);

/**
 * reschedule tasks other than this one and the kernel process
 */
int ioctl_reschedule_other(int file_desc);

/**
 * read / start / stop the pmu counters
 */
int ioctl_read_counters(int file_desc, char *message);
int ioctl_read_pc_lbr_counters(int file_desc, char *message);
int ioctl_start_counters(int file_desc, char *message);
int ioctl_stop_counters(int file_desc, char *message);

/**
 * print current asm instruction and PMU counters
 */
int ioctl_get_inst_counters(int file_desc, char *message);

/* 
 * Function will print "Genuine Intel" for Intel CPUs in kernel log message --swamy
 */
int ioctl_detect_cpu(int file_desc, char *message);

int ioctl_set_msg(int file_desc, char *message);
int ioctl_get_msg(int file_desc);
int ioctl_get_nth_byte(int file_desc);

// test print counter values
void print_pmc(pmc_value_t* pmc);
void print_pmc_file(FILE *fd, pmc_value_t* pmc);

#endif
