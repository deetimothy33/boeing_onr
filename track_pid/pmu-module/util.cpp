#include "util.h"

// construct bhr given lbr_t
// the mispredict bits are in the from register
unsigned long long construct_bhr_mispredict(lbr_t &lbr){
	unsigned long long bhr=0;

	//TODO make sure this access is correct
	for(int i=0; i<16; i++){
		// cyclic register
		bhr |= (lbr.from[(i + lbr.tos.fields.tos) % 16].fields.mispred) & 1;
		bhr = bhr << 1;
	}

	return bhr;
}

// construct bhr given lbr_t
// bhr is constructed based on branch (taken / not taken)
unsigned long long construct_bhr(lbr_t &lbr){
	unsigned long long bhr=0;

	//TODO make sure this access is correct
	for(int i=0; i<16; i++){
		//cout << "from: " << lbr.from[i].fields.data << "\tto: " << lbr.to[i].fields.data << endl;
		//cout << "to-from: " << (lbr.to[i].fields.data - lbr.from[i].fields.data) << endl;
		//cout << "from-to: " << (lbr.from[i].fields.data - lbr.to[i].fields.data) << endl;

		// cyclic register
		// from-to == 16 implies not taken
		bhr |= ((lbr.from[(i + lbr.tos.fields.tos) % 16].fields.data) - (lbr.to[(i + lbr.tos.fields.tos) % 16].fields.data)) == 16l;
		bhr = bhr << 1;
	}

	//cout << "bhr: " << bhr << endl;

	return bhr;
}
