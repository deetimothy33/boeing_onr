#include <linux/kernel.h>	/* We're doing kernel work */
#include <linux/module.h>	/* Specifically, a module */
#include <linux/fs.h>
//#include <asm/uaccess.h>	/* for get_user and put_user */
#include <asm/perf_event.h>   //added by swamy

// used for getting current instruction
#include <asm/current.h>
#include <linux/sched.h>
#include <asm/processor.h>

// used to disable premption while monitored program is executing
//#include <linux/preempt.h>
//#include <linux/sched.h>
//#include <linux/smp.h>

// for allocating memory
//#include <linux/slab.h>

// for this module
#include <linux/interrupt.h>
#include <linux/device.h>
#include <linux/of_device.h>
#include <linux/of_platform.h>
#include <linux/of_address.h>
#include <linux/of_irq.h>
#include <linux/platform_device.h>

#include <linux/ioport.h>
#include <linux/printk.h>

// from pmi.c
#include <linux/slab.h>
#include <linux/completion.h>
#include <linux/spinlock.h>
#include <linux/workqueue.h>

#include <asm/io.h>
//#include <asm/pmi.h>
#include <asm/prom.h>

/**
 * PMI INTERRUPT HANDLER
 */

/**
 * NOTE
 * the current macro points to the interrupted process
 * proc/interrupts is populated with statistics related to interrupts on the system
 * disabling interrupts guarantees that an interrupt handler will not preempt the current code
 * *disabling interrupts also disables kernel premption
 * a lock needs to be obtained to prevent other processers from accessing shared data simultaneously
 * 
 * objdmp -t pmi_interrupt.ko can be used to determine what symbols are in module
 * nm -u pmi_interrupt.o
 */

/**
 * USEFUL RESOURCES
 * https://github.com/torvalds/linux/blob/master/arch/powerpc/sysdev/pmi.c
 * https://notes.shichao.io/lkd/ch7/
 * https://github.com/JuliaCI/BenchmarkTools.jl/blob/master/doc/linuxtips.md
 * (MODULE_DEVICE_TABLE) http://www.linuxjournal.com/node/5604/print
 */

static unsigned int pmi_irq;
//static struct platform_device *pmi_device;
u8 __iomem *pmi_reg;

/**
 * setup pmu for pmi
 */
static void init_pmi_pmu(void){
	//TODO
}

/**
 * handle pmi interrupts
 */
static irqreturn_t pmi_interrupt(int irq, void *dev_id){
	/*	
	u8 type;
	int rc;

	spin_lock(&data->pmi_spinlock);

	type = ioread8(data->pmi_reg + PMI_READ_TYPE);
	pr_debug("pmi: got message of type %d\n", type);

	if (type & PMI_ACK && !data->completion) {
		printk(KERN_WARNING "pmi: got unexpected ACK message.\n");
		rc = -EIO;
		goto unlock;
	}

	if (data->completion && !(type & PMI_ACK)) {
		printk(KERN_WARNING "pmi: expected ACK, but got %d\n", type);
		rc = -EIO;
		goto unlock;
	}

	data->msg.type = type;
	data->msg.data0 = ioread8(data->pmi_reg + PMI_READ_DATA0);
	data->msg.data1 = ioread8(data->pmi_reg + PMI_READ_DATA1);
	data->msg.data2 = ioread8(data->pmi_reg + PMI_READ_DATA2);
	rc = 0;
unlock:
	spin_unlock(&data->pmi_spinlock);

	if (rc == -EIO) {
		rc = IRQ_HANDLED;
		goto out;
	}

	if (data->msg.type & PMI_ACK) {
		complete(data->completion);
		rc = IRQ_HANDLED;
		goto out;
	}

	schedule_work(&data->work);

	rc = IRQ_HANDLED;
out:
	return rc;
	*/
	//TODO
	return IRQ_HANDLED;
}

/**
 * magic from 
 * pmi.c
 * in kernel
 */
static const struct of_device_id pmi_match[] = {
	{ .type = "ibm,pmi", .name = "ibm,pmi" },
	{ .type = "ibm,pmi" },
	{},
};

MODULE_DEVICE_TABLE(of, pmi_match);

/**
 * request that the interrupt line be inserted
 */
static int init_pmi_interrupt(struct platform_device *pmi_device){
//int init_pmi_interrupt(void){
	//TODO how to get PMI irq?
	struct device_node *np = pmi_device->dev.of_node;
	//int rc;

	if (pmi_reg) {
		printk(KERN_ERR "pmi: driver has already been initialized.\n");
		goto out;
	}

	pmi_reg = of_iomap(np, 0);
	if (!pmi_reg) {
		printk(KERN_ERR "pmi: invalid register address.\n");
		goto out;
	}
	
	pmi_irq = irq_of_parse_and_map(np, 0);
	if (!pmi_irq) {
		printk(KERN_ERR "pmi: invalid interrupt.\n");
		goto error_cleanup_iomap;
	}

	// the last argument only needs to be unique
	// it is passed into pmi_interrupt on every invocation
	// IRQF_SHARED => this interrupt line is shared
	// 0 => IRQF_DISABLED => this handler does not share interrupt line
	//if (request_irq(pmi_irq, pmi_interrupt, 0, "PMI", (void *)&pmi_device)) {
	if (request_irq(pmi_irq, pmi_interrupt, 0, "PMI", NULL)) {
		printk(KERN_ERR "rtc: cannot register IRQ %d\n", pmi_irq);
		goto error_cleanup_iomap;
	}

	// setup the pmu for pmi's
	init_pmi_pmu();

error_cleanup_iomap:
	//iounmap(pmi_reg);
	//TODO

out:
	return 0;
}
	
static int cleanup_pmi_interrupt(struct platform_device *dev){
//void cleanup_pmi_interrupt(void){
	//free_irq(pmi_irq, (void *)&pmi_device);
	if(pmi_irq) free_irq(pmi_irq, NULL);
	if(pmi_reg) iounmap(pmi_reg); //TODO added untested

	//kfree(all things) //TODO
	
	return 0;
}

/* module_platform_driver() - Helper macro for drivers that don't do
 * anything special in module init/exit.  This eliminates a lot of
 * boilerplate.  Each module may only use this macro once, and
 * calling it replaces module_init() and module_exit()
 */

static struct platform_driver pmi_of_platform_driver = {
	.probe		= init_pmi_interrupt,
	.remove		= cleanup_pmi_interrupt,
	.driver = {
		.name = "pmi",
		.of_match_table = pmi_match,
	},
};
module_platform_driver(pmi_of_platform_driver);

