#ifndef UTIL_H
#define UTIL_H

#include "pmc.h"

using namespace std;

// construct bhr given lbr_t
// the mispredict bits are in the from register
unsigned long long construct_bhr_mispredict(lbr_t &lbr);

// construct bhr given lbr_t
// bhr is constructed based on branch (taken / not taken)
unsigned long long construct_bhr(lbr_t &lbr);

#endif
