//simple driver for PMU
//author - swamy d ponpandi
//date - June 8, 2017
//
//notes - adapated from Linux kernel module programming

/**
 * REFERENCES
 */
// https://software.intel.com/en-us/articles/pin-a-dynamic-binary-instrumentation-tooli
// https://software.intel.com/sites/landingpage/pintool/docs/81205/Pin/html/

/*
 *  ioctl.c - the process to use ioctl's to control the kernel module
 *
 *  Until now we could have used cat for input and output.  But now
 *  we need to do ioctl's, which require writing our own process.
 */

/* 
 * device specifics, such as ioctl numbers and the
 * major device file. 
 */
#include "pmu-device2.h"
//#include "lbr.h"
//#include "types.h"
#include "pmc.h"
#include "pmuctl.h"

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>		/* open */
#include <unistd.h>		/* exit */
#include <sys/ioctl.h>		/* ioctl */

#define MONITOR_SECONDS 3
#define PROGRAM_TRACE "../asm_pmu/out/asm.csv"

// output log file
#define LOG_FILE "out/log.csv"
	
/* 
 * Main - Call the ioctl functions 
 */
int main(){
	int file_desc, ret_val;
	char *msg = "Message passed by ioctl\n";
	pmc_value_t pmc, pmc_test;

	file_desc = open(DEVICE_FILE_NAME, 0);
	//	file_desc = open("/dev/PMU_DEVICE2_SWAMY", 0);
	if (file_desc < 0) {
		printf("Can't open device file: %s\n", DEVICE_FILE_NAME);
		exit(-1);
	}

	//	ioctl_set_msg(file_desc, msg);
	//	ioctl_get_nth_byte(file_desc);
	//	ioctl_get_msg(file_desc);

	//print PMU counters and look for it in kern.log file or create another log file
	// start counter using ioctl  
	ioctl_start_counters(file_desc, msg);

	//look in kern.log file /var/log for output
	ioctl_detect_cpu(file_desc, msg); 

	// perform sequential read test
	ioctl_sequential_read(file_desc);

	/**
	 * calling ioctl_read_counters() will read the pmu counters and store them in the device
	 *
	 * the values read can be retrieved by calling read() on the device
	 */
	int n = 5;
	for(int i=0; i<n; i++){	
		ioctl_read_counters(file_desc, msg);
	}
	
	// read counters by reading device
	for(int i=0; i<n+1; i++){
		read(file_desc, &pmc, sizeof(pmc_value_t));
		print_pmc(&pmc);
	}

	//TODO run program binary and read counters

	// stop counter using ioctl
	// (the last ioctl must be executed twice?)
	ioctl_stop_counters(file_desc, msg);
	ioctl_stop_counters(file_desc, msg);
			
	close(file_desc);
	return 0;
}


// reschedule tasks to other processors
//ioctl_reschedule_other(file_desc);

// set rt priority high
//ioctl_set_rt_priority(file_desc);

//TODO cpuset (linux/cpuset.h) is another option to isolate a core

// read counters (deprecated way of reading counters... too slow)
//ioctl_get_inst_counters(file_desc, &pmc);
//ioctl_get_inst_counters(file_desc, msg);

/**
 * execute the given instruction
 *
static inline void execute(unsigned long long inst){
	//asm(inst);
	//TODO
}

static inline void execute(char** inst){
	//__asm__(*inst);
	//TODO
}
*/

/**
 * read in a sample program execution
 * TODO this doesn't and can't work
 */
void sample_execution(int file_desc, char *msg){
	// read in a sample proram execution
	FILE *file;
	char *lineptr, *endptr=0;
	size_t n;
	int c;
	unsigned long long ull;
	file = fopen(PROGRAM_TRACE, "r");
	if(file){
		while((c = getline(&lineptr, &n, file)) != EOF){
			// remove the \n character
			lineptr[c-1] = 0;
			ull = strtoull(lineptr, &endptr, 10);

			// test print program
			//fprintf(stdout, "%s\n", lineptr);
			//fprintf(stdout, "%llu\n", ull);
			
			// read counters and inst
			ioctl_get_inst_counters(file_desc, msg);
			//sleep(MONITOR_SECONDS);
			//execute(ull);
			//execute(&lineptr);
			ioctl_get_inst_counters(file_desc, msg);
		}
		fclose(file);
	} else printf("cannot open file");
}
