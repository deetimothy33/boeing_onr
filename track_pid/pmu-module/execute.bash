#! /bin/bash

# check for number of arguments > 0
if [ $# -lt "1" ]; then
	echo "USAGE"
	echo "./execute [#]"
	echo "where # is one of the following:"
	echo "[1] pmu.x"
	echo "[2] track.x"
	echo "[3] test.x"
	exit
fi

# perform system setup
echo "SETUP"
sudo ./disable_nmi_watchdog.bash
sudo ./enable_rdpmc.bash
sudo modprobe msr

# load, test, unload module
echo "LOAD AND TEST LOAD SUCCESS"
sudo ./load.bash

echo "EXECUTE USERSPACE PROGRAM"
if [ $1 -eq 1 ]; then
	sudo nice -n -20 ./pmu.x
elif [ $1 -eq 2 ]; then
	sudo nice -n -20 ./track.x
elif [ $1 -eq 3 ]; then
	sudo nice -n -20 ./test.x
fi

echo "UNLOAD MODULE"
sudo ./unload.bash
