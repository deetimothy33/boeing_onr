#ifndef PMC_H
#define PMC_H

//#include "lbr.h"
#include "types.h"

// use perf system calls if defined
//#define PERF_SYSTEM_CALL 1

// MSR LOCATION DEFINITIONS

#define IA32_DS_AREA 0x600

#define BR_INST_RETIRED 0xC4
#define BR_INST_RETIRED_ALL_MASK 0x04

#define BR_MISP_RETIRED 0xC5
#define BR_MISP_RETIRED_ALL_MASK 0x04

#define FP_ASSISTS 0xF7
#define FP_ASSISTS_ALL_MASK 0x04

#define ITLB_MISS_RETIRED 0xC8
#define ITLB_MISS_RETIRED_MASK 0x20

#define INST_RETIRED 0xC0
#define INST_RETIRED_ALL_MASK 0x01
#define INST_RETIRED_MMX_MASK 0x04

#define MEM_LOAD_RETIRED 0xCB
#define MEM_LOAD_HIT_L1 0x01

#define UOPS_RETIRED 0xC2
#define UOPS_RETIRED_ALL_MASK 0x01

#define RAT_STALLS 0x59
#define RAT_STALLS_ALL_MASK 0x0F

#define SEG_RENAME_STALLS 0x0 //TODO 
#define SEG_RENAME_STALLS_MASK 0x01

// REFERENCES
// EVENTS	http://oprofile.sourceforge.net/docs/intel-corei7-events.php
// COUNTER ADDR https://docs.rs/x86/0.8.1/x86/
// PEBS CODE	https://github.com/andikleen/pmu-tools/blob/master/simple-pebs/simple-pebs.c
// COUNTERS AND MASKS	http://www.bnikolic.co.uk/blog/hpc-prof-events.html

/**
 * inline asm to use
 * 	wdmsr
 * 	rdmsr
 * to
 * 	set up pmu
 * 	read events from pmu
 */

/**
 * store last branch register (lbr) data
 */
typedef struct{
	struct LastBranchTOSRegister tos;
	struct LastBranchRegister from[16];
	struct LastBranchRegister to[16];
} lbr_t;

typedef struct{
	unsigned long long fc0;
	unsigned long long fc1;
	unsigned long long fc2;
	unsigned long long pc0;
	unsigned long long pc1;
	unsigned long long pc2;
	unsigned long long pc3;
	unsigned long long pc;
	lbr_t lbr;
} pmc_value_t;

typedef struct{
	pmc_value_t pmc;
	unsigned long long pc;
	unsigned long long bhr;
	unsigned long long bhr_mispredict;
} log_data_t;

typedef struct{
	unsigned long long bts_buffer_base;
	unsigned long long bts_index;
	unsigned long long bts_abs_max;
	unsigned long long bts_int_thresh;
	unsigned long long pebs_buffer_base; // linear address of the first byte of the PEBS buffer (alloc by softw)
	unsigned long long pebs_index; // index of current pebs record
	unsigned long long pebs_abs_max; // maximum number of pebs records
	unsigned long long pebs_int_thresh; // linear address where PEBS exception indicates PEBS buffer exhausted
	unsigned long long pebs_counter_0_reset; // reset counter this value after overflow
	unsigned long long pebs_counter_1_reset;
	unsigned long long pebs_counter_2_reset;
	unsigned long long pebs_counter_3_reset;
} ds_area_t;

typedef struct{
	unsigned long long eflags;
	unsigned long long eip;
	unsigned long long eax;
	unsigned long long ebx;
	unsigned long long ecx;
	unsigned long long edx;
	unsigned long long esi;
	unsigned long long edi;
	unsigned long long ebp;
	unsigned long long esp;
	unsigned long long r8;
	unsigned long long r9;
	unsigned long long r10;
	unsigned long long r11;
	unsigned long long r12;
	unsigned long long r13;
	unsigned long long r14;
	unsigned long long r15;
	unsigned long long perf_global_status;
	unsigned long long data_linear_address;
	unsigned long long data_source_encoding;
	unsigned long long latency_value;
} pebs_record_t;

/**
 * start and stop counters
 *

static inline void pmu_start_fixed_counters(void);
static inline void pmu_stop_fixed_counters(void);
static inline void pmu_start_programmable_counter(int counter, int event);
static inline void pmu_stop_programmable_counter(int counter, int event);
static inline void pmu_start_all_counters(void);
static inline void pmu_stop_all_counters(void);

**
 * read counters
 *

static inline int pmu_read_fixed_counter(int counter);
static inline int pmu_read_programmable_counter(int counter);
*/

#endif
