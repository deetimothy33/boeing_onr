#include <chrono>
#include <thread>
#include <ctime>
#include <cstdlib>
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>		/* open */
#include <unistd.h>		/* exit */
#include <sys/ioctl.h>		/* ioctl */
#include <sys/wait.h>
#include <sys/ptrace.h>
#include <sys/user.h>
#include <sys/reg.h>

#include "pmu-device2.h"
#include "pmc.h"
#include "pmuctl.h"
#include "util.h"

// output log folder
#define LOG_FOLDER "log"
#define PID 1 // fork()ed child will have different pid each time it is run
#define N 10000 // number of records to log
#define MEASUREMENT_INTERVAL 1 // period of measurement in microseconds

// select a program to monitor
//#define PROG_NAME "track_prog/work"
#define PROG_NAME "../../benchmark/sysbench/src/sysbench"
//#define PROG_ARGS {"","cpu","run", NULL} // PID 0
#define PROG_ARGS {"","memory","run", NULL} // PID 1

/**
 * UNIXBENCH
 * runs 1 copy of each test per cpu
 */

/**
 * SYSBENCH
 * common benchmarking utility
 */

/**
 * PTRACE
 * https://github.com/torvalds/linux/blob/master/arch/x86/kernel/ptrace.c
 * line	info
 * 200 	PTRACE_GETREGS
 * 394	CONFIG_X86_64
 * 996	R32(eip, ip)
 */

/**
 * overhead vs accuracy
 * depends on MEASUREMENT_INTERVAL
 * more frequent interruptions cause higher overhead and higher accuracy
 * less frequent interruptions cause lower overhead and lower accuracy
 */

/**
 * creates a log of the tracked program according to the following:
 * directory:	[LOG_FOLDER]
 * filename:	[pid]_[random number]
 * contents:	[PC],[BHR],[pmu value list]
 */

/**
 * This program does the following:
 * 	1. fork, PTRACE attach, and run a child
 * 	2. use PTRACE to track the child
 * 	3. uses pmu-module2 to
 * 		a. setup pmu counters
 * 		b. read pmu counters
 * 	4. pmu counters are read every time PTRACE returns
 *
 * 	The program is interupped at the specified MEASUREMENT_INTERVAL
 * 	pmu counters, bhr, and pc are read at this time
 */

/**
 * TODO
 * struggling to get PC
 * https://blog.tartanllama.xyz/writing-a-linux-debugger-registers/
 */

// test print a file to stdout
void test_print_file(string name){
	string value;
	ifstream testfile = ifstream(name);
	while(getline(testfile, value)){	
		cout << value << endl;
	}
	testfile.close();
}

// start the specified program
// return the PID (of the program that was started)
int start_program(char* program, char** arguments){
	int pid = fork();
	
	// fork returns 0 in the child
	if(pid == 0){
		// child
		// as in CFIMon,
		// use ptrace to enable modifications of the child's state
		ptrace(PTRACE_TRACEME, 0, NULL, NULL);
	
		//execv(program, args);
		execv(program, arguments);
		
		cerr << "ERROR: program not found" << endl;
		exit(2);
	}
	
	return pid;
}

//TODO extract bhr
//TODO analyze log file (to get accuracy of prediction scheme)
//TODO generate writeup describing idea, tools, experiment setup, results
//	and tradeoff between overhead and accuracy

//TODO sometimes it breaks and all but the first value read are the same
//		perhaps the block returned by reading the device is not getting advanced?

//TODO regs.rip is always the same value!
//TODO test that LBR is read correctly (in pmc.c)
//TODO test that LBR is read correctly (in construct_bhr)

//TODO construct_bhr uses misprediction instead of branch take or not taken

//TODO I THINK THE PROGRAM IS GETTING SWITCH BETWEEN CORES
//	the effect is to make the pmu_values 0 sometimes

//TODO we could also use these as part of PYP prediction scheme?
/* X86_64 registers
enum class reg {
    rax, rbx, rcx, rdx,
    rdi, rsi, rbp, rsp,
    r8,  r9,  r10, r11,
    r12, r13, r14, r15,
    rip, rflags,    cs,
    orig_rax, fs_base,
    gs_base,
    fs, gs, ss, ds, es
};
*/	

//TODO perhaps the monitored program always gets interrupted at the same location?
//	this could actually be a good thing!
//	OR it could be a product of the benchmark suite

int main(){
	vector<log_data_t*> log_data_v;
	struct user_regs_struct regs;
	log_data_t* ld;
	
	// for working with pmu-module2
	int file_desc;
	char *msg = "Message passed by ioctl\n";
	
	// open pmu-module2 device
	file_desc = open(DEVICE_FILE_NAME, 0);
	if (file_desc < 0) {
		printf("Can't open device file: %s\n", DEVICE_FILE_NAME);
		exit(-1);
	}

	// start counter using ioctl  
	ioctl_start_counters(file_desc, msg);
	
	//look in kern.log file /var/log for output
	ioctl_detect_cpu(file_desc, msg); 

	// perform sequential read test
	ioctl_sequential_read(file_desc);
	
	// start the process to be monitored
	//char* args[] = {"", NULL};
	char* args[] = PROG_ARGS;
	int pid = start_program(PROG_NAME, args);
	
	// wait for monitored program
	wait(NULL);

	//long orig_eax = ptrace(PTRACE_PEEKUSER, pid, 4*11, NULL);
	//printf("The child made a system call %ld\n", orig_eax);
	
	// record PC,BHR,PMU values for each instruction
	int n = N;
	for(int i=0; i<n; i++){	
		ld = new log_data_t;

		// populate log data
		//ld->bhr = 0l;

		// get the tracee's registers
		ptrace(PTRACE_GETREGS, pid, NULL, &regs);
		//ptrace(PTRACE_GETREGSET, pid, 0, &regs);
		//ptrace(PTRACE_GETFPREGS, pid, NULL, &regs);

		// grab the instruction pointer 
		// TODO always the same value
		ld->pc = regs.rip;
		//TODO
		
		// right now i'm just doing this to get a difference in the log files
		// the +i should not be there
		//ld->pc = regs.rip + i;

		// read pmu counters
		/**
		 * calling ioctl_read_counters() will read the pmu counters and store them in the device
		 *
		 * the values read can be retrieved by calling read() on the device
		 */
		//ioctl_read_counters(file_desc, msg);
		ioctl_read_pc_lbr_counters(file_desc, msg);

		log_data_v.push_back(ld);

		//TODO for some reason PTRACE_SINGLESTEP breaks it
		
		// stop after 1 instruction
		//ptrace(PTRACE_SINGLESTEP, pid, NULL, NULL);
				
		// stop after the next exit from a systemcall 
		//ptrace(PTRACE_SYSCALL, pid, NULL, NULL);

		// stop the tracee.
		// this can be used to take measurements every so many milliseconds
		ptrace(PTRACE_CONT, pid, NULL, NULL);	
		this_thread::sleep_for(chrono::microseconds(MEASUREMENT_INTERVAL));
		ptrace(PTRACE_INTERRUPT, pid, NULL, NULL);

		//cout << "READING" << endl;
	}
	
	// tell the child to continue executing
	ptrace(PTRACE_CONT, pid, NULL, NULL);	

	// wait for child to finish
	int status;
	while(waitpid(pid, &status, WNOHANG) == 0){
		sleep(1);
	}
		
	// read counters by reading device
	for(int i=0; i<n; i++){
		read(file_desc, &(log_data_v[i]->pmc), sizeof(pmc_value_t));
		//log_data_v[i]->pc = log_data_v[i]->pmc.pc;
		log_data_v[i]->bhr = construct_bhr(log_data_v[i]->pmc.lbr);
		
		//print_pmc(&(log_data_v[i]->pmc));
	}

	// log data
	srand(time(NULL));
	stringstream ss;
	ss << LOG_FOLDER << "/" << PID << "_" << (rand() % 10000);
	ofstream file = ofstream(ss.str());
	for(vector<log_data_t*>::iterator it=log_data_v.begin(); it!=log_data_v.end(); ++it){
		file  << (*it)->pc << ",";
		file  << (*it)->bhr << ",";
		file  << (*it)->pmc.fc0 << ",";
		file  << (*it)->pmc.fc1 << ",";
		file  << (*it)->pmc.fc2 << ",";
		file  << (*it)->pmc.pc0 << ",";
		file  << (*it)->pmc.pc1 << ",";
		file  << (*it)->pmc.pc2 << ",";
		file  << (*it)->pmc.pc3 << endl;

		//print_pmc(&((*it)->pmc));
	}
	file.close();

	// test file reading it line by line
	//test_print_file(ss.str());
	
	// stop counter using ioctl
	// (the last ioctl must be executed twice?)
	ioctl_stop_counters(file_desc, msg);
	ioctl_stop_counters(file_desc, msg);
			
	close(file_desc);

	// free
	for(vector<log_data_t*>::iterator it=log_data_v.begin(); it!=log_data_v.end(); ++it){
		delete *it;
	}

	return status;
}
