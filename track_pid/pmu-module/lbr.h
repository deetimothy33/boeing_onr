#ifndef LBR_H
#define LBR_H

typedef unsigned long long uint64;

struct DebugCtlRegister{
	union{
		struct{
			uint64 lbr : 1;
			uint64 btf : 1;
			uint64 reserved_0 : 4;
			uint64 tr : 1;
			uint64 bts : 1;
			uint64 btint : 1;
			uint64 bts_off_os : 1;
			uint64 bts_off_usr : 1;
			uint64 frz_lbrs_on_pmi : 1;
			uint64 frz_perfmon_on_pmi : 1;
			uint64 uncore_pmi_en : 1;
			uint64 smm_frz : 1;
			uint64 reserved_1 : 49;
		} fields;
		uint64 value;
	};
};

// implements Filtering LBR,
// setting a bit causes that information not to be recorded
#define LBR_SELECT 0x1C8
struct LBRSelectRegister{
	union{
		struct{
			uint64 cpl_eq_0 : 1;
			uint64 cpl_neq_0 : 1;
			uint64 jcc : 1;
			uint64 near_rel_call : 1;
			uint64 near_ind_call : 1;
			uint64 near_ret : 1;
			uint64 near_ind_jmp : 1;
			uint64 near_rel_jmp : 1;
			uint64 far_branch : 1;
			uint64 reserved : 55;
		} fields;
		uint64 value;
	};
};

/**
 * PEBS events
 */
#define IA32_PEBS_ENABLE (0x3F1)

/**
 * miscelanious registers
 */
#define IA32_MISC_ENABLE (0X1A0)

/**
 * LBR registers
 *
 * there are 16 of each 
 * 	from: (0x680 - 0x68F) 
 * 	to: (0x6C0 - 0x6CF)
 * these addresses are the beginning
 */
#define MSR_LASTBRANCH_FROM_IP 0x680
#define MSR_LASTBRANCH_TO_IP 0x6C0
// mispred bit is only in the from register!
struct LastBranchRegister{
	union{
		struct{
			uint64 data : 48;
			uint64 sign_ext : 15;
			uint64 mispred : 1; // set => branch predicted correctly
		} fields;
		uint64 value;
	};
};

// tells you where to begin reading in LBR stack
// read beginning 
#define MSR_LASTBRANCH_TOS 0x1C9
struct LastBranchTOSRegister{
	union{
		struct{
			uint64 tos : 4;
			uint64 reserved : 60;
		} fields;
		uint64 value;
	};
};

#endif
