#! /bin/bash

#PIN=lib/pin-3.4-97438-gf90d1f746-gcc-linux/pin
PIN=lib/pin-3.7-97619-g0d0c92f4f-gcc-linux/pin
TRACKER=src/obj-intel64/pin_tracker.so

SYSBENCH=../../benchmark/sysbench/src/sysbench
SYSBENCH_CPU="cpu run"
SYSBENCH_MEM="memory run"
BUFFER_OVERFLOW=../../buffer_overflow/overflow.x

# check for number of arguments > 0
if [ $# -lt "1" ]; then
	echo "USAGE"
	echo "./execute [#] [n]"
	echo "---"
	echo "[#] is one of the following:"
	echo "[0] ../pmu-module/pmu.x (test)"
	echo "[1] PIN log of Sysbench CPU"
	echo "[2] PIN log of Sysbench MEM"
	echo "[3] PIN tool instrument ls (test)"
	echo "[4] ASM disassembly of Sysbench CPU"
	echo "[5] PIN log of buffer overflow attack"
	echo "[6] ASM disassembly of ls"
	echo "---"
	echo "[n] is the number of times to run [#]"
	echo "---"
	exit
fi

# UNNECESSARY
# set default number of iterations 1
#if [ $# -lt "2" ]; then
	#$2=1
#fi


# perform system setup
echo "SETUP"
cd ../pmu-module
sudo ./disable_nmi_watchdog.bash
sudo ./enable_rdpmc.bash
sudo modprobe msr

# load, test, unload module
echo "LOAD AND TEST LOAD SUCCESS"
sudo ./load.bash

echo "EXECUTE USERSPACE PROGRAM"
cd ../pin_tracker
for i in `seq 1 $2`; do
	if [ $1 -eq 0 ]; then
		# for testing whether it is working
		cd ../pmu-module
		sudo nice -n -20 ./pmu.x
	elif [ $1 -eq 1 ]; then
		#-granularity 1 (default)
		#-mmax 1000000 (default)
		$PIN -t $TRACKER -id 0 -- $SYSBENCH $SYSBENCH_CPU
	elif [ $1 -eq 2 ]; then
		$PIN -t $TRACKER -id 1 -- $SYSBENCH $SYSBENCH_MEM
	elif [ $1 -eq 3 ]; then
		$PIN -t $TRACKER -- ls
	elif [ $1 -eq 4 ]; then
		$PIN -t $TRACKER -id 4 -asm 1 -- $SYSBENCH $SYSBENCH_CPU
	elif [ $1 -eq 5 ]; then
		$PIN -t $TRACKER -id 5  -- $BUFFER_OVERFLOW
	elif [ $1 -eq 6 ]; then
		$PIN -t $TRACKER -id 6  -asm 1 -- ls
	fi
done 

echo "UNLOAD MODULE"
cd ../pmu-module
sudo ./unload.bash
