
/*! @file
 *  This is an example of the PIN tool that demonstrates some basic PIN APIs 
 *  and could serve as the starting point for developing your first PIN tool
 */

// REFERENCES
// https://reverseengineering.stackexchange.com/questions/12404/intel-pin-how-to-access-the-ins-object-from-inside-an-analysis-function
//

#include "pin.H"
#include "tracker_util.h"

//TODO I may not be able to use any of these :(
//TODO need to use PINcrt API for System Calls
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <string>

//#include "../../pmu-module/pmuctl.h"
//#include "../../pmu-module/util.h"

#define LOG_FOLDER "log"
#define ASM_FOLDER "asm"

// use a special LOG_DATA_T->PC value 
// to indicate that the attack has begun / ended
#define ATTACK_BEGIN -1
#define ATTACK_END -2

using namespace std;

/* ================================================================== */
// Global variables 
/* ================================================================== */

UINT64 insCount = 0;        //number of dynamically executed instructions
UINT64 bblCount = 0;        //number of dynamically executed basic blocks
UINT64 threadCount = 0;     //total number of threads, including main thread

std::ostream * out = &cerr;

// TIM
vector<log_data_t*> log_data_v;
int file_desc;
char *msg = {NULL};
std::ostream *file;

//stringstream asm_ss;
std::ostream *asm_file;
UINT64 dissassembly_count = 0;
static map<ADDRINT, string> assembly_map;

/* ===================================================================== */
// Command line switches
/* ===================================================================== */
KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE,  "pintool",
		"o", "", "specify file name for MyPinTool output");

KNOB<BOOL>   KnobCount(KNOB_MODE_WRITEONCE,  "pintool",
		"count", "1", "count instructions, basic blocks and threads in the application");

// log id number  
// filename: [id]_[rand]
KNOB<INT>   KnobID(KNOB_MODE_WRITEONCE,  "pintool",
		"id", "0", "log id number");

// n determines the measurement granularity
// n := the inter-brach read interval
// n = 1 implies every branch is read
KNOB<UINT64>   KnobGranularity(KNOB_MODE_WRITEONCE,  "pintool",
		"granularity", "1", "take measurement every [granularity] branches");

KNOB<UINT64>   KnobMaxMeasurements(KNOB_MODE_WRITEONCE,  "pintool",
		"mmax", "100000", "maximum number of measurements");

KNOB<BOOL>   KnobAssembly(KNOB_MODE_WRITEONCE,  "pintool",
		"asm", "0", "print out a log of assembly instructions run");

/* ===================================================================== */
// Utilities
/* ===================================================================== */

/*!
 *  Print out help message.
 */
INT32 Usage()
{
	cerr << "This tool prints out the number of dynamically executed " << endl <<
		"instructions, basic blocks and threads in the application." << endl << endl;

	cerr << KNOB_BASE::StringKnobSummary() << endl;

	return -1;
}

/* ===================================================================== */
// Analysis routines
/* ===================================================================== */

/*!
 * Increase counter of the executed basic blocks and instructions.
 * This function is called for every basic block when it is about to be executed.
 * @param[in]   numInstInBbl    number of instructions in the basic block
 * @note use atomic operations for multi-threaded applications
 */
VOID CountBbl(UINT32 numInstInBbl)
{
	bblCount++;
	insCount += numInstInBbl;
}

//TODO figure out why PIN tool is only taking one measurement
//TODO I think it is because it gets stuck at the 'write()' in the measurement code

//TODO NOTE: PEBS events can be used to get the instruction IP in the real PMU

// use system call (to kernel module)
// to read IP, LBR, PMU FIXED AND PROGRAMMABLE COUNTER VALUES
UINT64 read_count=0;
VOID read_counters(VOID *ip){
	//*out << ip << endl;

	// check conditions on which we read counters
	if(read_count%KnobGranularity.Value() == 0 && 
			read_count < (KnobMaxMeasurements.Value())*KnobGranularity.Value()){
		// cause device to read counter values
		ioctl_read_pc_lbr_counters(file_desc, msg);

		// record the IP of this branch
		log_data_t *ld = new log_data_t;
		ld->pc = (long long unsigned) ip;
		log_data_v.push_back(ld);	
	}

	read_count++;
}

// print out the assembly code run.
// The goal is to make sure the instructions are being inserted in the correct place
//VOID disassemble(INS ins, VOID *v){
VOID disassemble(VOID *ip, ADDRINT addr){
	//TODO dissassemble only if it is call or return

	//TODO this causeses segfault (I am probabally overflowing the stream)	
	//	a solution is to simply print to the output file throughout execution
	//asm_ss << INS_Disassemble(ins) << endl;

	//TODO printing it out to the file makes it go significantly slower.
	// I think this has soething to do with how the benchmark works
	// I think it times the number of operations it can do over 10 seconds or something

	//TODO disas-error

	// output disassembled instruction to file	
	//*asm_file << assembly_map[addr] << endl;
	*asm_file << assembly_map[(ADDRINT)ip] << endl;
	//*asm_file << INS_Disassemble(ins) << endl;
	//*asm_file << "dummy inst" << endl;

	dissassembly_count++;
}

/* ===================================================================== */
// Instrumentation callbacks
/* ===================================================================== */

// print out the assembly code run.
// The goal is to make sure the instructions are being inserted in the correct place
VOID disassemble_on_call(INS ins, VOID *v){
	// only disassemble once at instrumentation time
	// populate a map of *instruction to disassembled instruction
	assembly_map[INS_Address(ins)] = INS_Disassemble(ins);

	// read the assembly only if the instruction is called
	// NOTE:
	// to pass an arbitrary argument to the analysis function,
	// instrumentation function requires two arguments [ argument_type, argument_value ]
	INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)disassemble, IARG_INST_PTR, IARG_ADDRINT, INS_Address(ins), IARG_END);
}

/*!
 * Insert call to the CountBbl() analysis routine before every basic block 
 * of the trace.
 * This function is called every time a new trace is encountered.
 * @param[in]   trace    trace to be instrumented
 * @param[in]   v        value specified by the tool in the TRACE_AddInstrumentFunction
 *                       function call
 */
VOID Trace(TRACE trace, VOID *v)
{
	// Visit every basic block in the trace
	for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl))
	{
		// Insert a call to CountBbl() before every basic bloc, passing the number of instructions
		BBL_InsertCall(bbl, IPOINT_BEFORE, (AFUNPTR)CountBbl, IARG_UINT32, BBL_NumIns(bbl), IARG_END);
	}
}

// BEGIN USED

// insert instructions to add "attack begin", "attack end" to the log file
int rdtsc_count=0;
VOID attack_begin_end(INS ins, VOID *v){
	// "rdtsc" indicates beginning and end
	if(INS_IsRDTSC(ins)){
		//TODO
		// there are 8 rdtsc instructions before the attack begins
		// rdtsc 9 and 10 surround the attack
		//TODO
		if(rdtsc_count<9) { 
			//*out << "NOT ATTACK" << endl;
			rdtsc_count++; 
		}else{
			*out << "ATTACK" << endl;
			// insert special log_data_t used in logging
			log_data_t *ld = new log_data_t;
			ld->pc = (unsigned long long)ATTACK_BEGIN;
			log_data_v.push_back(ld);	
		}
	}
	// "clflush" indicates end
	//if(false){
	//	*out << "ATTACK END" << endl;
	//	// insert special log_data_t used in logging
	//	log_data_t *ld = new log_data_t;
	//	ld->pc = (unsigned long long)ATTACK_END;
	//	log_data_v.push_back(ld);	
	//}

}

// trace of every branch encountered
VOID pmu_branch_trace(TRACE trace, VOID *v){
	// Visit every basic block in the trace
	for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl))
	{
		// Insert a call to CountBbl() before every basic bloc, passing the number of instructions
		BBL_InsertCall(bbl, IPOINT_BEFORE, (AFUNPTR)read_counters, IARG_INST_PTR, IARG_END);
		//BBL_InsertCall(bbl, IPOINT_AFTER, (AFUNPTR)read_counters, IARG_INST_PTR, IARG_END);
	}
}

// END USED

// this function prints the dissassembly for every instruction encountered
//TODO this is not currently working
VOID dissassembly_trace(TRACE trace, VOID *v){
	// Visit every basic block in the trace
	for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl)){
		// visit each instruction in the bbl
		for(INS ins = BBL_InsHead(bbl); INS_Valid(ins); ins=INS_Next(ins)){
			disassemble_on_call(ins, 0);

			// read the assembly only if the instruction is called
			//INS_InsertIfCall(ins, IPOINT_BEFORE, (AFUNPTR)disassemble, IARG_END);
		}
	}
}

// this function is called every time a new instruction is encountered
VOID pmu_inst_instrument(INS ins, VOID *v){
	//TODO both the branch check and the insertcall work independently, 
	//TODO but not when both are used together.
	//TODO they cause segfault i think.
	//TODO in either case the program is killed

	// if it is a branch instruction
	//INS_Valid(ins) && 
	//TODO why won't this work?
	/*
	   if(INS_IsBranch(ins)){
	// read PMU counters
	INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)read_counters, IARG_INST_PTR, IARG_END);
	//INS_InsertCall(ins, IPOINT_AFTER, (AFUNPTR)read_counters, IARG_INST_PTR, IARG_END);
	//INS_InsertCall(ins, IPOINT_ANYWHERE, (AFUNPTR)read_counters, IARG_INST_PTR, IARG_END);
	//INS_InsertCall(ins, IPOINT_TAKEN_BRANCH, (AFUNPTR)read_counters, IARG_INST_PTR, IARG_END);
	}
	*/

	// inserts call for every instruction
	INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)read_counters, IARG_INST_PTR, IARG_END);
	//INS_InsertCall(ins, IPOINT_AFTER, (AFUNPTR)read_counters, IARG_INST_PTR, IARG_END);
}

/*!
 * Increase counter of threads in the application.
 * This function is called for every thread created by the application when it is
 * about to start running (including the root thread).
 * @param[in]   threadIndex     ID assigned by PIN to the new thread
 * @param[in]   ctxt            initial register state for the new thread
 * @param[in]   flags           thread creation flags (OS specific)
 * @param[in]   v               value specified by the tool in the 
 *                              PIN_AddThreadStartFunction function call
 */
VOID ThreadStart(THREADID threadIndex, CONTEXT *ctxt, INT32 flags, VOID *v)
{
	threadCount++;
}

/*!
 * Print out analysis results.
 * This function is called when the application exits.
 * @param[in]   code            exit code of the application
 * @param[in]   v               value specified by the tool in the 
 *                              PIN_AddFiniFunction function call
 */
VOID Fini(INT32 code, VOID *v)
{
	*out <<  "===============================================" << endl;
	*out <<  "MyPinTool analysis results: " << endl;
	*out <<  "Number of instructions: " << insCount  << endl;
	*out <<  "Number of basic blocks: " << bblCount  << endl;
	*out <<  "Number of threads: " << threadCount  << endl;
	*out <<  "===============================================" << endl;
}
VOID stop_and_free(INT32 code, VOID *v){
	// close log assembly instructions
	if(KnobAssembly.Value()){
		delete asm_file;
	}

	// stop counter using ioctl
	// (the last ioctl must be executed twice?)

	ioctl_stop_counters(file_desc, msg);
	ioctl_stop_counters(file_desc, msg);

	close(file_desc);

	// free
	for(vector<log_data_t*>::iterator it=log_data_v.begin(); it!=log_data_v.end(); ++it){
		delete *it;
	}    
}

VOID pmu_overhead_log(INT32 code, VOID *v){
	int RECORD_N=1000;
	
	// start counter using ioctl  
	ioctl_start_counters(file_desc, msg);

	// compute the overheaad of PMU reading code
	//
	// take 1001 readings
	// print the difference between consecutive lines to a log file
	vector<log_data_t*> data_v;
	for(int i=0;i<RECORD_N;i++){
		// cause device to read counter values
		ioctl_read_pc_lbr_counters(file_desc, msg);
		ioctl_read_pc_lbr_counters(file_desc, msg);
	}
	for(int i=0;i<2*RECORD_N;i++){
		// record the IP of this branch
		log_data_t *ld = new log_data_t;
		data_v.push_back(ld);	
	}

	// read counters by reading device
	for(unsigned int i=0; i<data_v.size(); i++){
		read(file_desc, &(data_v[i]->pmc), sizeof(pmc_value_t));
	}

	stringstream ss;
	ss << LOG_FOLDER << "/pmu_overhead";
	file = new ofstream(ss.str().c_str());
	vector<log_data_t*>::iterator it_p=0;
	for(vector<log_data_t*>::iterator it=data_v.begin(); it!=data_v.end(); ++it){
		if(it_p==0){
			it_p=it;
			continue;
		}
		// compute the difference between consecutive lines
		// output difference to log file
		*file  << (*it)->pmc.fc0-(*it_p)->pmc.fc0 << ",";
		*file  << (*it)->pmc.fc1-(*it_p)->pmc.fc1 << ",";
		*file  << (*it)->pmc.fc2-(*it_p)->pmc.fc2 << ",";
		*file  << (*it)->pmc.pc0-(*it_p)->pmc.pc0 << ",";
		*file  << (*it)->pmc.pc1-(*it_p)->pmc.pc1 << ",";
		*file  << (*it)->pmc.pc2-(*it_p)->pmc.pc2 << ",";
		*file  << (*it)->pmc.pc3-(*it_p)->pmc.pc3; 
		*file  << endl; 

		// readings come in pairs
		it_p=0;
	}
	delete file;

	// free
	for(vector<log_data_t*>::iterator it=data_v.begin(); it!=data_v.end(); ++it){
		delete *it;
	}    
}

VOID output_log(INT32 code, VOID *v){
	*out <<  "===============================================" << endl;
	*out << "id: " << KnobID.Value() << endl;
	*out << "granularity: " << KnobGranularity.Value() << endl;
	*out << "measurements: " << log_data_v.size() << endl;
	*out << "max measurements: " << KnobMaxMeasurements.Value() << endl;
	*out << "dissassembly count: " << dissassembly_count << endl;
	*out <<  "===============================================" << endl;

	// read counters by reading device
	for(unsigned int i=0; i<log_data_v.size(); i++){
		read(file_desc, &(log_data_v[i]->pmc), sizeof(pmc_value_t));

		// PIN is used to grab the PC value at the branch
		//log_data_v[i]->pc = log_data_v[i]->pmc.pc;

		log_data_v[i]->bhr = construct_bhr(log_data_v[i]->pmc.lbr);
		log_data_v[i]->bhr_mispredict = construct_bhr_mispredict(log_data_v[i]->pmc.lbr);
	}

	// log data
	stringstream ss;
	ss << LOG_FOLDER << "/" << KnobID.Value() << "_" << (rand() % 10000);
	file = new ofstream(ss.str().c_str());
	for(vector<log_data_t*>::iterator it=log_data_v.begin(); it!=log_data_v.end(); ++it){
		// attack begun or ended
		if((*it)->pc == (unsigned long long)ATTACK_BEGIN){
			*file << "attack" << endl;
			continue;
		}
		// attack ended
		//if((*it)->pc == (unsigned long long)ATTACK_END){
		//	*file << "attack end" << endl;
		//	continue;
		//  }

		// asm instruction executed
		if(KnobAssembly.Value()){
			*file  <<"("<< assembly_map[(ADDRINT)((*it)->pc)] << "),";
		}
		// origional log contents
		*file  << (*it)->pc << ",";
		*file  << (*it)->bhr << ",";
		*file  << (*it)->pmc.fc0 << ",";
		*file  << (*it)->pmc.fc1 << ",";
		*file  << (*it)->pmc.fc2 << ",";
		*file  << (*it)->pmc.pc0 << ",";
		*file  << (*it)->pmc.pc1 << ",";
		*file  << (*it)->pmc.pc2 << ",";
		*file  << (*it)->pmc.pc3 << ",";
		// additional log contents
		*file  << (*it)->bhr_mispredict << ",";
		// print all lbr in order 
		// from most resent
		// to least resent
		for(int i=0; i<16; i++){
			*file  << lbr_from((*it)->pmc.lbr, i) << ",";
			*file  << lbr_to((*it)->pmc.lbr, i);
			// do not put a ',' on the last item
			if(i!=15) *file << ",";
		}
		*file  << endl;

		//print_pmc(&((*it)->pmc));
	}
	//file->close();
	delete file;
}

/**
 * setup function
 * adds pin instrumentation to read pmu counters every N branches
 */
void setup(){	
	// seed rand for any randomness generation
	srand(time(NULL)+getpid());

	// open pmu-module2 device
	stringstream module_file_name;
	module_file_name << "../pmu-module/" << DEVICE_FILE_NAME;
	file_desc = open(module_file_name.str().c_str(), O_RDWR);
	if (file_desc < 0) {
		printf("Can't open device file: %s\n", DEVICE_FILE_NAME);
		exit(-1);
	}

	// start counter using ioctl  
	ioctl_start_counters(file_desc, msg);

	// PIN instrumentation functions

	// add PMU reads at every branch
	INS_AddInstrumentFunction(pmu_inst_instrument, 0);

	// add pmu reads at the end of BBL
	// TODO origional function used
	//TRACE_AddInstrumentFunction(pmu_branch_trace, 0);

	// mark beginning and end of attack in log file
	//INS_AddInstrumentFunction(attack_begin_end, 0);

	// if an assembly log is requested,
	// disassemble every line to a log file
	if(KnobAssembly.Value()){
		// open the log file
		stringstream ss;
		//ss.str("");
		ss << ASM_FOLDER << "/" << KnobID.Value() << "_" << (rand() % 10000);
		asm_file = new ofstream(ss.str().c_str());
		//*file << asm_ss.str();

		// insert dissassembly instruction
		INS_AddInstrumentFunction(disassemble_on_call, 0);
		//TRACE_AddInstrumentFunction(dissassembly_trace, 0);

		//INS_AddInstrumentFunction(disassemble, 0);
	}

	PIN_AddFiniFunction(output_log, 0);
	PIN_AddFiniFunction(pmu_overhead_log, 0);
	PIN_AddFiniFunction(stop_and_free, 0);
}

/*!
 * The main procedure of the tool.
 * This function is called when the application image is loaded but not yet started.
 * @param[in]   argc            total number of elements in the argv array
 * @param[in]   argv            array of command line arguments, 
 *                              including pin -t <toolname> -- ...
 */
int main(int argc, char *argv[])
{
	// Initialize PIN library. Print help message if -h(elp) is specified
	// in the command line or the command line is invalid 
	if( PIN_Init(argc,argv) )
	{
		return Usage();
	}

	string fileName = KnobOutputFile.Value();

	if (!fileName.empty()) { out = new std::ofstream(fileName.c_str());}

	if (KnobCount)
	{
		// Register function to be called to instrument traces
		//TRACE_AddInstrumentFunction(Trace, 0);

		// Register function to be called for every thread before it starts running
		//PIN_AddThreadStartFunction(ThreadStart, 0);

		// Register function to be called when the application exits
		//PIN_AddFiniFunction(Fini, 0);

		// TIM    
		setup();
	}

	cerr <<  "===============================================" << endl;
	cerr <<  "This application is instrumented by MyPinTool" << endl;
	if (!KnobOutputFile.Value().empty()) 
	{
		cerr << "See file " << KnobOutputFile.Value() << " for analysis results" << endl;
	}
	cerr <<  "===============================================" << endl;

	// Start the program, never returns
	PIN_StartProgram();

	return 0;
}

/* ===================================================================== */
/* eof */
/* ===================================================================== */
