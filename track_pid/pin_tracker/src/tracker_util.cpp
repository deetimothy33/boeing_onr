#include "tracker_util.h"

/**
 * reading and writing to the character device causes it to
 * 's' -- start
 * 't' -- stop
 * 'r' -- read
 * the pmu counters.
 *
 * This method of interacting with the system was selected
 * becuase PIN does not allow system calls (like ioctl).
 * However, PIN does provide an OS-agnositc interface for 
 * interfacing with files.
 * This is sufficient for writing and reading to the character device.
 *
 * note: strings of the character commands are written on purpose
 * 		as they are automatically null-terminated
 * 	2 bytes are written to include the null byte
 */

int ioctl_read_pc_lbr_counters(int file_desc, char *message){
	//return ioctl(file_desc, IOCTL_READ_PC_LBR_COUNTERS, message);
	
	write(file_desc, "r", 2);
	return 0;
}

int ioctl_start_counters(int file_desc, char *message){
	//return ioctl(file_desc, IOCTL_START_COUNTERS, message);
	
	write(file_desc, "s", 2);
	return 0;
}

int ioctl_stop_counters(int file_desc, char *message){
	//return ioctl(file_desc, IOCTL_STOP_COUNTERS, message);
	
	write(file_desc, "t", 2);
	return 0;
}

unsigned long long lbr_from(lbr_t &lbr, int i){
	return lbr.from[(i + lbr.tos.fields.tos) % 16].fields.data;
}

unsigned long long lbr_to(lbr_t &lbr, int i){
	return lbr.to[(i + lbr.tos.fields.tos) % 16].fields.data;
}

unsigned long long construct_bhr_mispredict(lbr_t &lbr){
	unsigned long long bhr=0;

	//TODO make sure this access is correct
	for(int i=0; i<16; i++){
		// cyclic register
		bhr |= (lbr.from[(i + lbr.tos.fields.tos) % 16].fields.mispred) & 1;
		bhr = bhr << 1;
	}

	return bhr;
}

unsigned long long construct_bhr(lbr_t &lbr){
	unsigned long long bhr=0;

	//TODO make sure this access is correct
	for(int i=0; i<16; i++){
		//cout << "from: " << lbr.from[i].fields.data << "\tto: " << lbr.to[i].fields.data << endl;
		//cout << "to-from: " << (lbr.to[i].fields.data - lbr.from[i].fields.data) << endl;
		//cout << "from-to: " << (lbr.from[i].fields.data - lbr.to[i].fields.data) << endl;

		// cyclic register
		// from-to == 16 implies not taken
		bhr |= ((lbr.from[(i + lbr.tos.fields.tos) % 16].fields.data) - (lbr.to[(i + lbr.tos.fields.tos) % 16].fields.data)) == 16l;
		//bhr |= ((lbr.to[(i + lbr.tos.fields.tos) % 16].fields.data) - (lbr.from[(i + lbr.tos.fields.tos) % 16].fields.data)) == 16l;
		bhr = bhr << 1;	
	}

	// test difference
	//bhr = ((lbr.to[(0 + lbr.tos.fields.tos) % 16].fields.data) - (lbr.from[(0 + lbr.tos.fields.tos) % 16].fields.data));
	//bhr = ((lbr.from[(0 + lbr.tos.fields.tos) % 16].fields.data) - (lbr.to[(0 + lbr.tos.fields.tos) % 16].fields.data));


	//cout << "bhr: " << bhr << endl;

	return bhr;
}

