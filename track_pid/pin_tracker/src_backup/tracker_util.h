#ifndef UTIL_H
#define UTIL_H

#include "pmu-device2.h"
#include "pmc.h"

//TODO these will need to be removed
//#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

using namespace std;

// system call to (read pc, lbr, and pmu counter data), (start counters), (stop counters)
int ioctl_read_pc_lbr_counters(int file_desc, char *message);
int ioctl_start_counters(int file_desc, char *message);
int ioctl_stop_counters(int file_desc, char *message);

// 0 -- returns the most resent last branch
// 15 -- returns the oldest last branch
// uses top to tell which branch is which
unsigned long long lbr_from(lbr_t &lbr, int i);
unsigned long long lbr_to(lbr_t &lbr, int i);

// construct bhr given lbr_t
// the mispredict bits are in the from register
unsigned long long construct_bhr_mispredict(lbr_t &lbr);

// construct bhr given lbr_t
// bhr is constructed based on branch (taken / not taken)
unsigned long long construct_bhr(lbr_t &lbr);

#endif
