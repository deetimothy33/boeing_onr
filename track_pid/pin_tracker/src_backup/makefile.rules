##############################################################
#
# This file includes all the test targets as well as all the
# non-default build rules and test recipes.
#
##############################################################

##############################################################
#
# Test targets
#
##############################################################

###### Place all generic definitions here ######

# This defines tests which run tools of the same name.  This is simply for convenience to avoid
# defining the test name twice (once in TOOL_ROOTS and again in TEST_ROOTS).
# Tests defined here should not be defined in TOOL_ROOTS and TEST_ROOTS.
TEST_TOOL_ROOTS := MyPinTool

# This defines the tests to be run that were not already defined in TEST_TOOL_ROOTS.
TEST_ROOTS :=

# This defines the tools which will be run during the the tests, and were not already defined in
# TEST_TOOL_ROOTS.
TOOL_ROOTS :=

# This defines the static analysis tools which will be run during the the tests. They should not
# be defined in TEST_TOOL_ROOTS. If a test with the same name exists, it should be defined in
# TEST_ROOTS.
# Note: Static analysis tools are in fact executables linked with the Pin Static Analysis Library.
# This library provides a subset of the Pin APIs which allows the tool to perform static analysis
# of an application or dll. Pin itself is not used when this tool runs.
SA_TOOL_ROOTS :=

# This defines all the applications that will be run during the tests.
APP_ROOTS :=

# This defines any additional object files that need to be compiled.
OBJECT_ROOTS :=

# This defines any additional dlls (shared objects), other than the pintools, that need to be compiled.
DLL_ROOTS :=

# This defines any static libraries (archives), that need to be built.
LIB_ROOTS :=

###### Define the sanity subset ######

# This defines the list of tests that should run in sanity. It should include all the tests listed in
# TEST_TOOL_ROOTS and TEST_ROOTS excluding only unstable tests.
SANITY_SUBSET := $(TEST_TOOL_ROOTS) $(TEST_ROOTS)


##############################################################
#
# Test recipes
#
##############################################################

# This section contains recipes for tests other than the default.
# See makefile.default.rules for the default test rules.
# All tests in this section should adhere to the naming convention: <testname>.test


##############################################################
#
# Build rules
#
##############################################################

# This section contains the build rules for all binaries that have special build rules.
# See makefile.default.rules for the default build rules.

# define necessary object source files
#PMC=../../pmu-module/pmc.c
#PMUCTL=../../pmu-module/pmuctl.c
#UTIL=../../pmu-module/util.cpp

# define build variables
#TOOL_CXXFLAGS+=-Wall -g -O3 -Wno-unknown-pragmas -std=c++11 -pipe
#TOOL_CXXFLAGS+=-I../../pmu-module
#TOOL_LDFLAGS_NOOPT+=-lglib-2.0 -lc
#TOOL_LDFLAGS_NOOPT=

# modify VARS to allow linking to system libraries
#TOOL_LPATHS=
#MOD_TOOL_LIBS=$(filter-out -nostdlib, $(TOOL_LIBS))

#note, modifying the above does not help... it creates conflicts with pincrt library... which I need.

UTIL=tracker_util.cpp
TOOL_CXXFLAGS+=-I../../pmu-module
#DEBUG=1

# Build the intermediate object file.
$(OBJDIR)pin_tracker$(OBJ_SUFFIX): pin_tracker.cpp
	$(CXX) $(TOOL_CXXFLAGS) $(COMP_OBJ)$@ $<

# Build the intermediate object file.
$(OBJDIR)util$(OBJ_SUFFIX): $(UTIL) $(UTIL:.cpp=.h)
	$(CXX) $(TOOL_CXXFLAGS) $(COMP_OBJ)$@ $<

#$(OBJDIR)pmuctl$(OBJ_SUFFIX): $(PMUCTL) $(PMUCTL:.c=.h)
#	$(CC) $(filter-out -fno-rtti, $(TOOL_CXXFLAGS)) $(COMP_OBJ)$@ $<

#$(OBJDIR)util$(OBJ_SUFFIX): $(UTIL) $(UTIL:.cpp=.h)
#	$(CXX) $(TOOL_CXXFLAGS) $(COMP_OBJ)$@ $<

# turns out this one is not needed
#$(OBJDIR)pmc$(OBJ_SUFFIX): $(PMC) $(PMC:.c=.h)
#	$(CXX) $(TOOL_CXXFLAGS) $(COMP_OBJ)$@ $<
	#$(CCX) $(TOOL_CXXFLAGS) $(COMP_OBJ)$@ $<

# Build the tool as a dll (shared object).
$(OBJDIR)pin_tracker$(PINTOOL_SUFFIX): $(OBJDIR)pin_tracker$(OBJ_SUFFIX) $(OBJDIR)util$(OBJ_SUFFIX) $(UTIL:.cpp=.h)
	$(LINKER) $(TOOL_LDFLAGS_NOOPT) $(LINK_EXE)$@ $(^:%.h=) $(TOOL_LPATHS) $(TOOL_LIBS)

#$(OBJDIR)pin_tracker$(PINTOOL_SUFFIX): $(OBJDIR)pin_tracker$(OBJ_SUFFIX) $(OBJDIR)pmuctl$(OBJ_SUFFIX) $(PMUCTL:.c=.h) $(OBJDIR)util$(OBJ_SUFFIX) $(UTIL:.cpp=.h)
#	$(LINKER) $(TOOL_LDFLAGS_NOOPT) $(LINK_EXE)$@ $(^:%.h=) $(TOOL_LPATHS) $(TOOL_LIBS)
##origional#$(LINKER) $(TOOL_LDFLAGS_NOOPT) $(LINK_EXE)$@ $(^:%.h=) $(TOOL_LPATHS) $(TOOL_LIBS)
##ld $(TOOL_LDFLAGS_NOOPT) $(LINK_EXE)$@ $(^:%.h=) $(TOOL_LPATHS) $(TOOL_LIBS)

# not needing pmc.o
#$(OBJDIR)pin_tracker$(PINTOOL_SUFFIX): $(OBJDIR)pin_tracker$(OBJ_SUFFIX) $(OBJDIR)pmc$(OBJ_SUFFIX) $(PMC:.c=.h) $(OBJDIR)pmuctl$(OBJ_SUFFIX) $(PMUCTL:.c=.h) $(OBJDIR)util$(OBJ_SUFFIX) $(UTIL:.c=.h)
#	$(LINKER) $(TOOL_LDFLAGS_NOOPT) $(LINK_EXE)$@ $(^:%.h=) $(TOOL_LPATHS) $(TOOL_LIBS)

