#track_pid
track a program with the given PID
###pmu-module
contains a kernel module and a user space program
#####kernel module
takes a command to begin tracking a PID
it sets up the PMU to generate PMI
every time it receives a PMI it records 
	current->pid (current task process id)
	current->eip (current task instruction pointer)
!! perhaps it could also respond to a timer interrupt to grab the pmu values?
This tells us the approximate pmu values for the given process over time.
!! can it also register an interrupt for the context switch timer?
	if so, we can get an exact count
###pin\_tracker
running make in pmu-module will copy the compiled kernel module to this folder also
execute pin tool which uses kernel module system calls to read pmu counters
output is logged to a log folder.
the logs look the same as pmu-module logs.
#####./execute.bash 1
run pin tool for Sysbench CPU benchmark.
#####./execute.bash 2
run pin tool for Sysbench MEM benchmark.
#####lib
contains intel PIN tool file
#####src/pin\_tracker.cpp
Program implementing tracking code
#####example
this is a soft link to a directory in the intel PIN tools directory.
#####web resources
https://software.intel.com/sites/landingpage/pintool/docs/97438/Pin/html/index.html#EXAMPLES
