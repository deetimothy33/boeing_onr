#! /usr/bin/ruby

# there was a mistake in data generation
# the result was that attack site 3 files ended up having attack site 2 in some of their lines
# this script modifies the files to correct this
#
# in/
# 	site_3/
# 		*.csv
#
# out/
# 	site_3/
# 		*.csv

require 'csv'

INPUT="in/site3"
OUTPUT="out/site3"


def read_file(file_name)
	puts "parsing #{file_name}"
	
	# read the file
	# change the attack site on every line where it occurred
	# output the file

	output=CSV.open("#{OUTPUT}/#{file_name}",'w')

	# read the input file
	# parse each line into a category based on the attack cite
	File.readlines("#{INPUT}/#{file_name}").each do |line|
		# extract attack_site from the line
		split_line=line.split(',')
		attack_site=split_line[0]

		# if the attack site is 2,
		# then change it from 2 to 3
		if attack_site=="2"
			split_line[0]=3
		end

		# the last thing in the row was being output as "0"
		split_line[7]=split_line[7].to_i
		
		# add line to the entry for the attack site
		output << split_line
	end

	output.close
end

def main()
	Dir.foreach("#{INPUT}") do |item|
		next if item=='.' or item=='..'
		read_file(item)
	end
end

main()
