These are the files we started with. The Makefile creates a kernel module that we installed after every reboot, it allowed user access to PMU counters. The array_sum.cpp is a test program for reading PMU counters.

Note: We established by the end of Fall 2017 that this method is not suitable as perf is part of the linux kernel, and we have no control over perf running in the background. Hence we kept seeing inconsistencies using this method. We are better off using perf_open system calls.
