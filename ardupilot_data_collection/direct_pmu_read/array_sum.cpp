/*---------------------------------------------------------------*/
/* Performance Monitoring Unit (PMU) Example Code for Cortex-A/R */
/*                                                               */
/* Copyright (C) ARM Limited, 2010-2012. All rights reserved.    */
/*---------------------------------------------------------------*/

#include<iostream>
#include<stdlib.h>
#include<bitset>
#include <unistd.h>
#include "Armv8_PMU.h"

#define MSIZE 5


/*Initialize Matrices*/
int matrix_a[MSIZE][MSIZE],matrix_b[MSIZE][MSIZE],matrix_r[MSIZE][MSIZE];
	void initialize_matrices()
	{
	int i, j;
	for (i = 0; i < MSIZE; i++) {
		for (j = 0; j < MSIZE; j++) {
			matrix_a[i][j] = 1;
			matrix_b[i][j] = 1;
			matrix_r[i][j] = 0;
			}
		}
 	}

	
int main()
{
	unsigned long v,k;
	int i,j;
	/* Initialize Matrix*/
	initialize_matrices();
	enable_pmu();
	pmn_config(0, 0x11);
//	pmn_config(1, 0x08);
//	pmn_config(2, 0x60);
	
	for(v=0;v<10000;v++){
	/*Set up PMU*/
	reset_pmn();
	enable_pmn(0);
//	enable_pmn(1);
//	enable_pmn(2);

/*_____________________________________________________________________________________*/
	
	/*Add Matrices*/
	for (i = 0; i < MSIZE; i++) {
		for (j = 0; j < MSIZE; j++) {
			matrix_r[i][j] = matrix_a[i][j] + matrix_b[i][j];
			}
		}
//	printf("Matrix element 1,1 = %d\n",matrix_r[1][1]);
	asm volatile("mrc p15, 0, %0, c9, c13, 1": "=r"(k));
//	printf("CNTENSETR=%lu\n",k);
/*_____________________________________________________________________________________*/
	
	disable_pmn(0);
//	disable_pmn(1);
//	disable_pmn(2);
	printf("%lu\n",read_pmn(0));
	}
	return 0;
}
	



