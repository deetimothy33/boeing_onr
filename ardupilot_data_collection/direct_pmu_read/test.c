/*This is the Kernel Module to enable PMU access from userland*/

#include <linux/module.h>    // included for all kernel modules
#include <linux/kernel.h>    // included for KERN_INFO
#include <linux/init.h>      // included for __init and __exit macros

static void enable_ccnt_read(void* data)
{

 unsigned int counter ;
//enable user mode
     asm ("mrc p15, 0, %0, c9, c14, 0" : "=r" (counter));
     counter |= 0x00000001;
     asm ("mcr p15, 0, %0, c9, c14, 0" : : "r"(counter));
 printk(KERN_INFO "PMCR  user mode = %u\n", counter);
    // asm ("mcr p15, 0, %0, c9, c14, 1" : : "r"(0x8000000f));
// printk(KERN_INFO "Address %d Data %d\n",*p,p[4]);
}


static int __init hello_init(void)
{

//enable user mode
  
int x = on_each_cpu(enable_ccnt_read, NULL, 1);
if(x!=0)
{
	printk("Module could not be loaded\n");
	return 1;
}
else
    return x;    // Non-zero return means that the module couldn't be loaded.
}

static void __exit hello_cleanup(void)
{
  printk(KERN_INFO "Cleaning up module.\n");
}

module_init(hello_init);
module_exit(hello_cleanup);
