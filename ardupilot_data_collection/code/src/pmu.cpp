#include "pmu.h"

using namespace pmu;

static long perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
		int cpu, int group_fd, unsigned long flags)
{
	int ret;

	ret = syscall(__NR_perf_event_open, hw_event, pid, cpu,
			group_fd, flags);
	return ret;
}

static long fd[COUNTER_N];
void enable_pmu(){
	struct perf_event_attr pe;

	// set all bytes of struct to 0
	memset(&pe, 0, sizeof(struct perf_event_attr));
	pe.size = sizeof(struct perf_event_attr);
	pe.disabled = 1;
	pe.exclude_kernel = 1;
	pe.exclude_hv = 1;
	
	// open all counters
	for(int i=0; i<COUNTER_N; ++i){
		pe.type = event[i].type;
		pe.config = event[i].config;
		
		fd[i] = perf_event_open(&pe, 0, -1, -1, 0);
		if (fd[i] == -1) {
			fprintf(stderr, "Error opening leader %llx, counter %d\n", pe.config, i);
			exit(EXIT_FAILURE);
		}
	}
}
void disable_pmu(){
	for(int i=0; i<COUNTER_N; ++i){
		close(fd[i]);	
	}
}
void reset_pmu(){
	for(int i=0; i<COUNTER_N; ++i){
		ioctl(fd[i], PERF_EVENT_IOC_RESET, 0);
	}
}
void start_pmu(){
	for(int i=0; i<COUNTER_N; ++i){
		ioctl(fd[i], PERF_EVENT_IOC_ENABLE, 0);
	}
}
void stop_pmu(){
	for(int i=0; i<COUNTER_N; ++i){
		ioctl(fd[i], PERF_EVENT_IOC_DISABLE, 0);
	}
}


// COUNTER API TO HARDWARE THREAD


void pmu::counter_setup(){
	enable_pmu();
	reset_pmu();
	start_pmu();
}

void pmu::counter_start(){
	start_pmu();
}

void pmu::counter_stop(){
	stop_pmu();
}

void pmu::counter_disable(){
	stop_pmu();
	stop_pmu();
	disable_pmu();
}

void pmu::counter_read(counter_t *counter){
	//std::cout << counter->c0 << " ";
	if(fd[0] != 0) read(fd[0], &(counter->c0), sizeof(long long));
	//std::cout << counter->c0 << std::endl;
	if(fd[1] != 0) read(fd[1], &(counter->c1), sizeof(long long));
	if(fd[2] != 0) read(fd[2], &(counter->c2), sizeof(long long));
	if(fd[3] != 0) read(fd[3], &(counter->c3), sizeof(long long));
	if(fd[4] != 0) read(fd[4], &(counter->c4), sizeof(long long));
	if(fd[5] != 0) read(fd[5], &(counter->c5), sizeof(long long));
	//if(fd[6] != 0) read(fd[6], &(counter->c6), sizeof(long long));
}
