#include "hardware_counter_thread.h"

HardwareCounterThread::HardwareCounterThread(){
}

void HardwareCounterThread::start(){
	counter_v.clear();
	running=true;
	t=std::thread(&HardwareCounterThread::read_loop,this);
}

void HardwareCounterThread::stop(){
	stop_thread=true;
	t.join();
}

void HardwareCounterThread::counter_enable(){
	pmu::counter_setup();
	running=true;
	count=0;
}

void HardwareCounterThread::counter_disable(){
	pmu::counter_disable();
	running=false;
}

void HardwareCounterThread::counter_start(){
	if(running) pmu::counter_start();
}

void HardwareCounterThread::counter_stop(){
	if(running) pmu::counter_stop();
}

void HardwareCounterThread::counter_read(int attack){
	pmu::counter_t c;
	//std::cout<< "Inside counter_read() bool_attack "<< attack <<std::endl;
	if(running){
		pmu::counter_read(&c);
		(*this).counter_v.push_back(c);
		(*this).attack_bool_v.push_back(attack);
		(*this).branch_history_v.push_back(branch_history_map[attack]);
		//std::cout << branch_history_map[attack] << std::endl;
		count++;
	}
}

void HardwareCounterThread::counter_log(){
	log_counters();
	counter_v.clear();
	attack_bool_v.clear();
	branch_history_v.clear();
}

void HardwareCounterThread::read_loop(){
	count=0;
	auto time=std::chrono::high_resolution_clock::now();

	pmu::counter_setup();
	while(count<number && !(*this).stop_thread){
		// read counters
		// store values in memory
		//pmu::counter_t c;
		// measure every 'frequency' nanoseconds
		std::this_thread::sleep_for(std::chrono::nanoseconds((*this).frequency) - 
				(std::chrono::high_resolution_clock::now()-time));
		time=std::chrono::high_resolution_clock::now();
		HardwareCounterThread::counter_read(-1);
		//pmu::counter_read(&c);
		//(*this).counter_v.push_back(c);
		//(*this).attack_bool_v.push_back(false);

		count++;
	}
	pmu::counter_disable();

	// log values to file
	log_counters();
	this->running=false;
}

void HardwareCounterThread::log_counters(){
	std::ofstream f;
	f.open(log_file_name);
	for(unsigned int i=0; i<counter_v.size(); ++i){
		f<<attack_bool_v[i];
		f<<",";
		f<<counter_v[i].c0;
		f<<",";
		f<<counter_v[i].c1;
		f<<",";
		f<<counter_v[i].c2;
		f<<",";
		f<<counter_v[i].c3;
		f<<",";
		f<<counter_v[i].c4;
		f<<",";
		f<<counter_v[i].c5;
		f<<",";
		f<<branch_history_v[i];
		f<<std::endl;
	}
	f.close();
}
		
void HardwareCounterThread::branch_record(int identifier, bool taken){
	// keep a running branch history.
	// it will be recorded when the pmu counters are read.
	// Shift in a 0 or 1 depending on whether the branch is taken or not taken
	branch_history=branch_history<<1;
	branch_history|=(taken ? 1 : 0);
	//std::cout << branch_history << std::endl;

	// keep a map associating branch history with an attack cite or no attack cite
	// the IDENTIFIER is the same as the attack cite for all branches before an attack
	// (after the previous attack)
	// -1 for no attack
	// 0 to 4 for attack
	branch_history_map[identifier]=branch_history;
}
