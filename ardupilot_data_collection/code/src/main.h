#ifndef MAIN_H
#define MAIN_H

#include <chrono>
#include <thread>
#include "hardware_counter_thread.h"
#include "buffer_overflow.h"
       
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/perf_event.h>
#include <asm/unistd.h>

int main();

void pmu_example();
static long perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
		int cpu, int group_fd, unsigned long flags);


#endif
