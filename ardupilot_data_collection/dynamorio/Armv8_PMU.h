/*---------------------------------------------------------------*/
/* Performance Monitoring Unit (PMU) Example Code for Cortex-A/R */
/*                                                               */
/* Copyright (C) ARM Limited, 2010-2012. All rights reserved.    */
/*---------------------------------------------------------------*/


#ifndef Armv8_PMU
#define Armv8_PMU

#define	ARMV8_PMNC_N_MASK	0x1f
#define	ARMV8_PMNC_MASK		0x3f	 /* Mask for writable bits */

#define	ARMV8_MAX_COUNTERS	32
#define	ARMV8_COUNTER_MASK	(ARMV8_MAX_COUNTERS - 1)

// event   = The event code (from appropriate TRM or ARM Architecture Reference Manual)



void pmn_config(unsigned long counter, unsigned long eventx)

{

	        counter &=ARMV8_PMNC_N_MASK;

		asm volatile("mcr p15, 0, %0, c9, c12, 5" : : "r"(counter));

		asm volatile("mcr p15, 0, %0, c9, c13, 1" : : "r"(eventx));		
}

//EXPORT ccnt_divider
//; Enables / disables the divider(1 / 64) on CCNT
//; void ccnt_divider(int divider)
//; divider(in r0) = If 0 disable divider, else enable divider
void ccnt_divider(int r0)
{
	unsigned long counter;
	unsigned long r1;
	asm volatile("mrc p15, 0, %0, c9, c12, 0" : "=r" (counter)); //Read PMCR
	if (r0 == 0)
		counter |= 1 << 3;//Set the D bit(enables the divisor)
	else
		counter &= ~(1 << 3);//Clear the D bit(disables the divisor)

	asm volatile("mcr p15, 0, %0, c9, c12, 0" : : "r"(counter));// Write PMCR

}


//

// Enables and disables

//

unsigned long return_PMCR(void)
{
		
	unsigned long counter;
	asm volatile("mrc p15, 0, %0, c9, c12, 0" : "=r" (counter));
	return counter;
}


// Global PMU enable

// On ARM11 this enables the PMU, and the counters start immediately

// On Cortex this enables the PMU, there are individual enables for the counters

void enable_pmu(void)
{
		
	unsigned long counter;
	asm volatile("mrc p15, 0, %0, c9, c12, 0" : "=r" (counter));
        counter |= 1 << 0;
	asm volatile("mcr p15, 0, %0, c9, c12, 0" : : "r" (counter));
}


// Global PMU disable

// On Cortex, this overrides the enable state of the individual counters

void disable_pmu(void)

{

	unsigned long counter;
	asm volatile("mrc p15, 0, %0, c9, c12, 0" : "=r" (counter));
	counter &= ~(1 << 0);
	asm volatile("mcr p15, 0, %0, c9, c12, 0" : : "r" (counter));
}



// Enable the CCNT

void enable_ccnt(void)

{

	unsigned long counter = 0x80000000;

	asm volatile("mcr   p15, 0, %0, c9, c12, 1" : : "r"(counter));// Write PMCNTENSET Register

}





// Disable the CCNT

void disable_ccnt(void)

{

	unsigned int counter = 0x80000000;

	asm volatile("mcr   p15, 0, %0, c9, c12, 2" : : "r"(counter));// Write PMCNTENSET Register
}

//enable_pmn PROC
 // MOV     r1, #0x1
 // MOV     r1, r1, LSL r0
 // MCR     p15, 0, r1, c9, c12, 1  ; Write PMCNTENSET Register
 // BX      lr
 // ENDP

// Enable PMN{n}

// counter = The counter to enable (e.g. 0 for PMN0, 1 for PMN1)

void enable_pmn(unsigned long counter)
{
	unsigned long r0 = 0x1;
	r0= r0 << counter;
	asm volatile("mcr   p15, 0, %0, c9, c12, 1" : : "r"(r0));// Write PMCNTENSET Register
	//asm volatile("mcr   p15, 0, %0, c9, c12, 1" : : "r"(counter));// Write PMCNTENSET Register


}



//

//

//EXPORT disable_pmn

//; Disable PMN{ n }

//; void disable_pmn(unsigned long counter)

//; counter(in r0) = The counter to disable(e.g. 0 for PMN0, 1 for PMN1)



// Enable PMN{n}

// counter = The counter to enable (e.g. 0 for PMN0, 1 for PMN1)

void disable_pmn(unsigned long counter)

{
	asm volatile("mcr   p15, 0, %0, c9, c12, 2" : : "r"(counter));// Write PMCNTENSET Register

}





//

// Read counter values

//

// Returns the value of CCNT

unsigned long read_ccnt(void)

{

	unsigned long val;

	asm volatile("mrc p15, 0, %0, c9, c13, 0" : "=r"(val));

	return val;

}



// Returns the value of PMN{n}

// counter = The counter to read (e.g. 0 for PMN0, 1 for PMN1)

unsigned long read_pmn(unsigned long counter)

{

unsigned long val;

counter &= ARMV8_PMNC_N_MASK;

asm volatile("mcr   p15, 0, %0, c9, c12, 5" : : "r"(counter));

asm volatile("mrc p15, 0, %0, c9, c13, 2" : "=r"(val));

return val;

}





//

// Overflow and interrupts

//



//

//// Returns the value of the overflow flags

//unsigned int read_flags(void);

//

//// Writes the overflow flags

//void write_flags(unsigned int flags);

//

//// Enables interrupt generation on overflow of the CCNT

//void enable_ccnt_irq(void);

//

//// Disables interrupt generation on overflow of the CCNT

//void disable_ccnt_irq(void);

//

//// Enables interrupt generation on overflow of PMN{x}

//// counter = The counter to enable the interrupt for (e.g. 0 for PMN0, 1 for PMN1)

//void enable_pmn_irq(unsigned int counter);

//

//// Disables interrupt generation on overflow of PMN{x}

//// counter =  The counter to disable the interrupt for (e.g. 0 for PMN0, 1 for PMN1)

//void disable_pmn_irq(unsigned int counter);





//

// Counter reset functions

//



//EXPORT reset_pmn

//; Resets all programmable counters to zero

//; void reset_pmn(void)

//reset_pmn PROC

//MRC     p15, 0, r0, c9, c12, 0; Read PMCR

//ORR     r0, r0, #0x2; Set P bit(Event counter reset)

//MCR     p15, 0, r0, c9, c12, 0; Write PMCR

//BX      lr

//ENDP



// Resets the programmable counters

 void reset_pmn(void)

{

	unsigned long val;

	
	asm volatile("mrc p15, 0, %0, c9, c12, 0" : "=r"(val));

	val |= 1 << 1;

	asm volatile("mcr   p15, 0, %0, c9, c12, 0" : : "r"(val));


}



//

//

//EXPORT  reset_ccnt

//; Resets the CCNT

//; void reset_ccnt(void)

//reset_ccnt PROC

//MRC     p15, 0, r0, c9, c12, 0; Read PMCR

//ORR     r0, r0, #0x4; Set C bit(Clock counter reset)

//MCR     p15, 0, r0, c9, c12, 0; Write PMCR

//BX      lr

//ENDP



// Resets the CCNT

void reset_ccnt(void)

{

	unsigned long val;

	asm volatile("mrc p15, 0, %0, c9, c12, 0" : "=r"(val));

	val |= 1 << 2;

	asm volatile("mcr   p15, 0, %0, c9, c12, 0" : : "r"(val));


}

unsigned long read_flags()

{
	unsigned long val;
	asm volatile("mrc p15, 0, %0, c9, c12, 3" : "=r"(val));
	return val;

}



#endif // _V7_PMU_H
