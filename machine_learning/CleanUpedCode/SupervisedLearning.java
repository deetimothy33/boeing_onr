package CleanUpedCode;

import weka.classifiers.AbstractClassifier;
import weka.core.Attribute;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Guo on 2/8/18.
 */
public class SupervisedLearning {
    private static int numOfRows;
    private static int numOfColumns;
    private static File[] files;
    private Boolean isSignatureTable;
    private ArrayList<Integer> selectedAttri;


    public SupervisedLearning(String normalFolder, String underAttackFolder, String targetFolder,
                              ArrayList<Integer> selectedAttri, String type) {
        ArrayList<Attribute> atts = Utils.getLabeledAttributes(selectedAttri.size());
        TrainData trainData = new TrainData(selectedAttri, normalFolder, underAttackFolder);
        AbstractClassifier[] classifiers = trainData.getClassifiers(type, atts);
        ClassifyData classifyData  = new ClassifyData(selectedAttri, targetFolder, Utils.ATTACK_LABEL);
        System.out.println("Classify Accuracy: " + classifyData.getAccuracy(atts, classifiers, trainData.getDataSets()));
    }
}
