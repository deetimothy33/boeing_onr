package CleanUpedCode;
import java.util.ArrayList;
/**
 * Created by Guo on 2/8/18.
 */
public class SupervisedLearningTest {
    public static void main(String[] args) {
        String TrainAttack = "signatureTable/attack_train/";
        String TestAttack = "signatureTable/attack_test/";
        String TrainRef = "signatureTable/ref_train/";
        String TestRef = "signatureTable/ref_test/";

        ArrayList<Integer> selectedAtts = new ArrayList<>();
        for (int i = 0; i < 16; i++) {
            selectedAtts.add(i);
        }
        SupervisedLearning supervisedLearning = new SupervisedLearning(TrainRef, TrainAttack, TestAttack,
                selectedAtts, Utils.SVM_LABEL);
    }
}
