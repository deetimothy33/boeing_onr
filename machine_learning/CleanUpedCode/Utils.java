package CleanUpedCode;
import weka.core.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
/**
 * Created by Guo on 2/8/18.
 */
public class Utils {
    public static final String ATTACK_LABEL = "attack";
    public static final String NOATTACK_LABEL = "noattack";

    public static final String SVM_LABEL = "SVM";
    public static final String RF_LABEL = "RF";

    public static ArrayList<Attribute> getNonLabelAttributes (int numOfAttributes) {
        ArrayList<Attribute> atts = new ArrayList<Attribute>();
        for (int i = 0; i < numOfAttributes; i++) {
            Attribute x = new Attribute("Column_"+i);
            atts.add(x);
        }
        return atts;
    }

    public static ArrayList<Attribute> getLabeledAttributes (int numOfAttributes) {
        ArrayList<Attribute> atts = new ArrayList<Attribute>();
        FastVector my_nominal_values = new FastVector(1);
        my_nominal_values.addElement(ATTACK_LABEL);
        my_nominal_values.addElement(NOATTACK_LABEL);
        Attribute labelAtt = new Attribute("label", my_nominal_values);
        atts.add(labelAtt);
        for (int i = 0; i < numOfAttributes; i++) {
            Attribute x = new Attribute("Column_"+i);
            atts.add(x);
        }
        return atts;
    }

    public static Instances[] getLabeledTrainingSet(ArrayList<Integer> selectedAtt,
                                           ArrayList<ArrayList<Double>>[] trainNoAttackDataSet,
                                           ArrayList<ArrayList<Double>>[] trainAttackDataSet,
                                           ArrayList<Attribute> atts,
                                           int numOfRows) {
        Instances[] res = new Instances[numOfRows];
        int numOfFiles = trainAttackDataSet.length + trainNoAttackDataSet.length;
        for (int i = 0; i < numOfRows; i++) {
            Instances adataset = new Instances("aDataSet", atts, numOfFiles);
            adataset.setClassIndex(0);
            res[i] = adataset;
        }
        addLabelFolder(selectedAtt, atts, trainNoAttackDataSet, NOATTACK_LABEL, res);
        addLabelFolder(selectedAtt, atts, trainAttackDataSet, ATTACK_LABEL, res);
        return res;
    }


    public static void addLabelFolder(ArrayList<Integer> selectedAtt,
                                      ArrayList<Attribute> atts,
                                      ArrayList<ArrayList<Double>>[] trainDataSet,
                                      String label, Instances[] res) {
        for (int i = 0; i < trainDataSet.length; i++) {
            ArrayList<ArrayList<Double>> file = trainDataSet[i];
            for (int j = 0; j < file.size(); j++) {
                ArrayList<Double> line = file.get(j);
                Instance inst = getLabeledInstance(atts, line, label, selectedAtt, res[j]);
                if (inst != null) {
                    res[j].add(inst);
                }
            }
        }
    }

    public static Instance getLabeledInstance(ArrayList<Attribute> atts, ArrayList<Double> line,
                                              String label, ArrayList<Integer> selectedAtt, Instances dataSet) {
        Instance inst = new DenseInstance(atts.size());
        inst.setDataset(dataSet);
        inst.setValue(0, label);
        if (line.size() >= selectedAtt.size()) {
            for (int i = 0; i < selectedAtt.size(); i++) {
                inst.setValue(i + 1, line.get(selectedAtt.get(i)));
            }
            return inst;
        } else {
            return null;
        }
    }

    public static Instances[] getNoLabeledTrainingSet(ArrayList<Integer> selectedAtt,
                                                    ArrayList<ArrayList<Double>>[] trainNoAttackDataSet,
                                                    ArrayList<ArrayList<Double>>[] trainAttackDataSet,
                                                    ArrayList<Attribute> atts,
                                                    int numOfRows) {
        Instances[] res = new Instances[numOfRows];
        int numOfFiles = trainAttackDataSet.length + trainNoAttackDataSet.length;
        for (int i = 0; i < numOfRows; i++) {
            Instances adataset = new Instances("aDataSet", atts, numOfFiles);
            adataset.setClassIndex(0);
            res[i] = adataset;
        }
        addNoLabelFolder(selectedAtt, atts, trainNoAttackDataSet, res);
        addNoLabelFolder(selectedAtt, atts, trainAttackDataSet, res);
        return res;
    }

    public static void addNoLabelFolder(ArrayList<Integer> selectedAtt,
                                        ArrayList<Attribute> atts,
                                        ArrayList<ArrayList<Double>>[] trainDataSet,
                                        Instances[] res) {
        for (int i = 0; i < trainDataSet.length; i++) {
            ArrayList<ArrayList<Double>> file = trainDataSet[i];
            for (int j = 0; j < file.size(); j++) {
                ArrayList<Double> line = file.get(j);
                Instance inst = getNonLabelInstance(atts, line, selectedAtt);
                inst.setDataset(res[j]);
                res[j].add(inst);
            }
        }
    }

    public static Instance getNonLabelInstance(ArrayList<Attribute> atts, ArrayList<Double> line,
                                               ArrayList<Integer> selectedAtt) {
        Instance inst = new DenseInstance(atts.size());
        for (int i = 0; i < selectedAtt.size(); i++) {
            inst.setValue(i, line.get(selectedAtt.get(i)));
        }
        return inst;
    }

    public static Instance getNonLabelInstance(Attribute atts, ArrayList<Double> line) {
        return null;
    }

    public static ArrayList<ArrayList<Double>> readInFile(File file) {
        ArrayList<ArrayList<Double>> res = new ArrayList<>();
        try {
            Scanner scan = new Scanner(file);
            while (scan.hasNextLine()) {
                String curLine = scan.nextLine();
                String[] list = curLine.split(",");
                ArrayList<Double> values = new ArrayList<>();
                for (int i = 0; i < list.length; i++) {
                    if (isNumeric(list[i])) {
                        values.add(Double.valueOf(list[i]));
                    }
                }
                res.add(values);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static boolean isNumeric(String str) {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

    public static int getNumOfRows(File file) {
        return readInFile(file).size();
    }

    public static int getNumOfColumns (File file) {
        ArrayList<ArrayList<Double>> data = readInFile(file);
        return (data == null || data.size() == 0) ? 0 : data.get(0).size();
    }

    public static File[] getAllFiles(String folder) {
        return new File(folder).listFiles();
    }
}
