package CleanUpedCode;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.functions.SMO;
import weka.classifiers.trees.RandomForest;
import weka.core.Attribute;
import weka.core.Instances;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by Guo on 2/8/18.
 */
public class TrainData {
    private ArrayList<Integer> selectedAttri = new ArrayList<>();
    private ArrayList<ArrayList<Double>>[] trainNoAttackDataSet;
    private ArrayList<ArrayList<Double>>[] trainAttackDataSet;
    Instances[] datasets;
    private int numOfRows;
    private int numOfColumns;
    private long trainingTime;

    public TrainData(ArrayList<Integer> selectedAttri, String normalFolder, String underAttackFolder) {
        this.selectedAttri.addAll(selectedAttri);
        File[] nFolder = Utils.getAllFiles(normalFolder);
        File[] aFolder = Utils.getAllFiles(underAttackFolder);
        int numOfFileNoAttack = nFolder.length;
        int numOfFileAttack = aFolder.length;
       // System.out.println(numOfFileAttack + " " + numOfFileNoAttack);
        trainNoAttackDataSet = new ArrayList[numOfFileNoAttack];
        trainAttackDataSet = new ArrayList[numOfFileAttack];

        for (int i = 0; i < numOfFileNoAttack; i++) {
            trainNoAttackDataSet[i] = Utils.readInFile(nFolder[i]);
        }
        for (int i = 0; i < numOfFileAttack; i++) {
           trainAttackDataSet[i] = Utils.readInFile(aFolder[i]);
        }

        numOfRows = trainAttackDataSet[0].size();
        numOfColumns = trainAttackDataSet[0].get(0).size();
        trainingTime = 0;
    }

    public AbstractClassifier[] getClassifiers(String type, ArrayList<Attribute> atts) {
        long startTime = System.currentTimeMillis();
        datasets = Utils.getLabeledTrainingSet(selectedAttri, trainNoAttackDataSet,
                trainAttackDataSet, atts, numOfRows);
        AbstractClassifier[] res = new AbstractClassifier[numOfRows];
        for (int i = 0; i < numOfRows; i++) {
            res[i] = getClassifier(type);
            try {
                res[i].buildClassifier(datasets[i]);
            } catch (Exception e) {
                System.out.println(e.fillInStackTrace());
            }
        }
        trainingTime = System.currentTimeMillis() - startTime;
        return res;
    }

    public AbstractClassifier getClassifier(String type) {
        if (type.equals(Utils.SVM_LABEL)) {
            return new SMO();
        } else if (type.equals(Utils.RF_LABEL)) {
            return new RandomForest();
        } else {
            System.out.println("Error: Classifier type can't be recognized!");
            return null;
        }
    }

    public long getTrainingTime() {
        if (trainingTime  == 0) {
            System.out.println("Error: Trying to get training time before training! ");
        }
        return trainingTime;
    }

    public Instances[] getDataSets() {
        return datasets;
    }

}
