package CleanUpedCode;

import weka.classifiers.AbstractClassifier;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Guo on 2/8/18.
 */
public class ClassifyData {
    private ArrayList<Integer> selectedAttri = new ArrayList<>();
    private ArrayList<ArrayList<Double>>[] targetDataSet;
    private int numOfRows;
    private int numOfColumns;
    private double classifyTime;
    private int numOfRunning;
    private String label;

    public ClassifyData(ArrayList<Integer> selectedAttri, String targetFolder, String label) {
        this.selectedAttri.addAll(selectedAttri);
        File[] tFolder = Utils.getAllFiles(targetFolder);
        int numOfTargets = tFolder.length;
        targetDataSet = new ArrayList[numOfTargets];
        for (int i = 0; i < numOfTargets; i++) {
            targetDataSet[i] = Utils.readInFile(tFolder[i]);
        }
        numOfRows = targetDataSet[0].size();
        numOfColumns = targetDataSet[0].get(0).size();
        classifyTime = 0;
        numOfRunning = 0;
        this.label = label;
    }

    public double getAccuracy (ArrayList<Attribute> atts, AbstractClassifier[] classifiers, Instances[] datasets) {
        int count = 0;
        for (int i = 0; i < targetDataSet.length; i++) {
            if (classifyFile(atts, i, classifiers, datasets)) {
                count++;
            }
        }
        return (double)(count)/((double)(targetDataSet.length));
    }

    public boolean classifyFile(ArrayList<Attribute> atts, int fileIndex,
                                AbstractClassifier[] classifiers, Instances[] datasets) {
        long startTime = System.currentTimeMillis();
        int count = 0;
        for (int i = 0; i < numOfRows; i++) {
            if (classifyOneRow(i, fileIndex, atts, classifiers[i], datasets[i])) {
                count++;
            }
        }
        long totalTime = System.currentTimeMillis() - startTime;
        numOfRunning++;
        classifyTime = (classifyTime * (numOfRunning - 1) + totalTime) * numOfRunning;
        return count >= (double)(numOfRows * 0.7);
    }

    public boolean classifyOneRow(int rowIndex, int fileIndex, ArrayList<Attribute> atts,
                                  AbstractClassifier classifier, Instances dataset) {
        Instance inst = Utils.getLabeledInstance(atts, targetDataSet[fileIndex].get(rowIndex),label,
                selectedAttri, dataset);
        if (inst == null) {
            return false;
        }
        //System.out.println(dataset.numInstances());
        String label = null;
        try {
            label = classifier.classifyInstance(inst) == 0 ? Utils.ATTACK_LABEL : Utils.NOATTACK_LABEL;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error: Instance can't be classified!");
        }
        return this.label.equals(label);
    }



}
