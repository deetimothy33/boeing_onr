package PCAanalysis; /**
 * Created by Guo on 2/6/18.
 */
import weka.attributeSelection.PrincipalComponents;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Arrays;
public class PCAanalysis {
    private static int numOfRows;
    private static int numOfColumns;
    private static File[] files;
    private ArrayList<Attribute> attributes;
    private Boolean isSignatureTable;

    public PCAanalysis(String[] folderName, boolean isSignatureTable) {
        this.isSignatureTable = isSignatureTable;
        this.files = readInData(folderName);
        if (this.files != null && this.files.length != 0) {
            int[] size = dataSize(this.files[0]);
            this.numOfRows = size[0];
            this.numOfColumns = size[1];
        } else {
            System.out.println("Error: Folder is empty!");
        }
        if (numOfRows != 0) {
            if (this.isSignatureTable) {
                numOfColumns--;
            }
            attributes = getAttributes(numOfColumns);
        }
        System.out.println("Original Number of Attribute: " + numOfColumns);
    }

    public void PCA(){
        Instances data = getInstances();
        PrincipalComponents principalComponents = new PrincipalComponents();
        try {
            principalComponents.buildEvaluator(data);
            System.out.println(" ");
            System.out.println("PCA test result: ");
            //principalComponents.setOptions(new String[]{"-A"});
            for (int i = 0; i < 10; i++) {
                System.out.println("Merit of attribute " + data.attribute(i).name() + ": " + principalComponents.evaluateAttribute(i));
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public Instances getInstances() {
        Instances dataset = new Instances("dataSet", attributes, files.length);
        dataset.setClassIndex(0);
        for (int i = 0; i < files.length; i++) {
            ArrayList<ArrayList<Double>> data = getFileContent(files[i]);
            if (data.size() != numOfRows) {
                System.out.println("File size not correct: " + files[i].getName());
            }
            for (int j = 0; j < data.size(); j ++) {
                ArrayList<Double> curline = data.get(j);
                if (isSignatureTable  && curline.size() != numOfColumns + 1) {
                    System.out.println("Line size not correct: " + files[i].getName() + " the " + j + " th line.");
                } else {
                    Instance inst = new DenseInstance(numOfColumns);
                    inst.setDataset(dataset);
                    int k = 0;
                    if (isSignatureTable) {
                        k = 1;
                    }
                    for (; k < data.get(j).size(); k++) {
                        if (isSignatureTable) {
                            inst.setValue(k - 1, curline.get(k));
                        } else {
                            inst.setValue(k, curline.get(k));
                        }
                    }
                    dataset.add(inst);
                }
            }
        }
        return dataset;
    }

    public static boolean isNumeric(String str) {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

    public ArrayList<ArrayList<Double>> getFileContent(File file) {
        ArrayList<ArrayList<Double>> res = new ArrayList<>();
        try {
            Scanner scan = new Scanner(file);
            while (scan.hasNextLine()) {
                ArrayList<Double> newline = new ArrayList<>();
                String line = scan.nextLine();
                String[] list = line.split(",");
                for (int i = 0; i < list.length; i++) {
                    if (isNumeric(list[i])) {
                        newline.add(Double.valueOf(list[i]));
                    } else {
                        newline.add(0.0);
                    }
                }
                res.add(newline);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return res;
    }

    public ArrayList<Attribute> getAttributes (int numOfAttributes) {
        ArrayList<Attribute> atts = new ArrayList<Attribute>();
        for (int i = 0; i < numOfAttributes; i++) {
            Attribute x = new Attribute("a"+i);
            atts.add(x);
        }
        return atts;
    }

    public int[] dataSize(File firstFile) {
        int[] result = new int[2];
        try {
            Scanner scan = new Scanner(firstFile);
            while (scan.hasNextLine()) {
                result[0]++;
                String line = scan.nextLine();
                if (result[0] == 1) {
                    result[1] = line.split(",").length;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }

    public File[] readInData(String[] folder) {
        ArrayList<File> allFiles = new ArrayList<File>();
        for (int i = 0; i < folder.length; i++) {
            allFiles.addAll(Arrays.asList(new File(folder[i]).listFiles()));
        }
        return allFiles.toArray(new File[allFiles.size()]);
    }
}

