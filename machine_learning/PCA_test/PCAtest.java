package PCAanalysis;

/**
 * Created by Guo on 2/7/18.
 */
public class PCAtest {
    public static void main(String[] args){
        String folder1 = "/Users/Guo/boeing_onr/log/signature_table/set1/reference/";
        String folder2 = "/Users/Guo/boeing_onr/log/signature_table/set1/attack/";
        String[] list = new String[]{folder1, folder2};
        PCAanalysis pcaanalysis = new PCAanalysis(list, true);
        pcaanalysis.PCA();
    }
}
