import weka.clusterers.EM;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by Guo on 12/4/17.
 */
public class test2 {
    public static final int numOfAttribute = 8;

    public static void main(String[] args) {
        String trainFolder = "/Users/Guo/boeing_onr/log/signature_table/set1/reference/";
        String targetFolder = "/Users/Guo/boeing_onr/log/signature_table/set1/attack/";
        getClassify(trainFolder, targetFolder);

    }

    public static boolean getClassify(String trainFolder, String targetFolder) {
        // Read from training and target folder
        File[] trainFiles = getAllFiles(trainFolder);
        File[] targetFiles = getAllFiles(targetFolder);
        ArrayList[] trainData = new ArrayList[trainFiles.length];
        ArrayList[] targetData = new ArrayList[targetFiles.length];
        for (int i = 0; i < trainFiles.length; i++) {
            trainData[i] = getfile(trainFiles[i]);
        }
        for (int j = 0; j < targetFiles.length; j++) {
            targetData[j] = getfile(targetFiles[j]);
        }
        int numOfRecords = trainData[0].size();
        int numOfTraining = Math.min(trainFiles.length, targetFiles.length) / 2;


        // Create Attributes
        ArrayList<Attribute> atts = consAttributes();

        // Initialize Clusterer
        String[] options = new String[2];
        options[0] = "-I";                 // max. iterations
        options[1] = "100";
        EM clusterer = new EM();   // new instance of clusterer
        try {
            clusterer.setOptions(options);     // set the options
        } catch (Exception e) {
            System.out.println(e.getStackTrace());
        }

        // start training with training and target data
        Instances[] normalData = new Instances[numOfTraining];
        for (int i = 0; i < numOfTraining; i++) {
            normalData[i] = StoInstance(trainData[i], atts);
        }
        int numOfValidIndex = 0;
        for (int k = 0; k < numOfRecords; k++) {
            // get Instances of current signature table index
            Instances trainIns = TtoInstances(trainData, k, numOfTraining, atts);
            try {
                clusterer.buildClusterer(trainIns);    // build the clusterer
                int numOfClusters = clusterer.numberOfClusters();
                HashMap<Integer, Integer> table = new HashMap<Integer, Integer>();
                int numOfValidClusters = 0;
                for (int j = 0; j < numOfTraining; j++) {
                    Instance curIns = normalData[j].instance(k);
                    int curIndex = clusterer.clusterInstance(curIns);
                    if (table.containsKey(curIndex)) {
                        table.put(curIndex, table.get(curIndex) + 1);
                    } else {
                        table.put(curIndex, 1);
                        numOfValidClusters++;
                    }
                }
                int thre = (int) (numOfClusters * 0.5);
                if (numOfValidClusters > thre) {
                    numOfValidIndex++;
                }
                // System.out.println("numberOfCluster: "+ clusterer.numberOfClusters());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("Number of valid index: " + numOfValidIndex);
        if (numOfValidIndex > numOfRecords *  0.9) {
            System.out.println("No attack is detected!");
            return false;
        } else {
            System.out.println("Under attack!");
            return true;
        }

    }



    public static ArrayList<Attribute> consAttributes () {
        // create attributes
        ArrayList<Attribute> atts = new ArrayList<Attribute>();
        Attribute a = new Attribute("a");
        Attribute b = new Attribute("b");
        Attribute c = new Attribute("c");
        Attribute d = new Attribute("d");
        Attribute e = new Attribute("e");
        Attribute f = new Attribute("f");
        Attribute h = new Attribute("h");
        Attribute i = new Attribute("i");

        atts.add(a);
        atts.add(b);
        atts.add(c);
        atts.add(d);
        atts.add(e);
        atts.add(f);
        atts.add(h);
        atts.add(i);
        return atts;
    }


    public static Instances StoInstance(ArrayList<ArrayList<Double>> curFile, ArrayList<Attribute> atts) {
        Instances adataset = new Instances("aDataSet", atts, curFile.size());
        //adataset.setClassIndex(0);
        // construct inst
        Instance inst = new DenseInstance(numOfAttribute);
        inst.setDataset(adataset);
        for (int j = 0; j < curFile.size(); j++) {
            for (int i = 0; i < numOfAttribute; i++) {
                inst.setValue(i, curFile.get(j).get(i));
            }
            adataset.add(inst);
        }
        return adataset;
    }

    /**
     * Get all training data and convert them into instances
     * @param trainFiles set of training data
     * @param index signature table index
     * @return Instances
     */
    public static Instances TtoInstances(ArrayList<ArrayList<Double>>[] trainFiles,  int index, int numOfFile, ArrayList<Attribute> atts) {
        //System.out.println("numOfFiles: " + numOfFile);

        // Create dataset
        Instances adataset = new Instances("aDataSet", atts, numOfFile);
        //adataset.setClassIndex(0);
        // Add instance
        for (int j = numOfFile; j < numOfFile + 20; j++) {
            ArrayList<ArrayList<Double>> firstFile = trainFiles[j];
            ArrayList<Double> curline = firstFile.get(index);

            // construct inst
            Instance inst = new DenseInstance(numOfAttribute);
            inst.setDataset(adataset);
            for (int i = 0; i < numOfAttribute; i++) {
                inst.setValue(i, curline.get(i));
            }
            adataset.add(inst);
        }
        return adataset;
    }

    /**
     * Get all files in targe folder
     * @param folderName folder name
     * @return a set of files
     */
    public static File[] getAllFiles(String folderName) {
        File f = new File(folderName);
        return f.listFiles();
    }


    /**
     * Read data from a file
     * @param file file contains data
     * @return data in Arraylist
     */
    public static ArrayList<ArrayList<Double>> getfile(File file) {
        ArrayList<ArrayList<Double>> res = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] attributes = line.split(",");
                ArrayList<Double> list = new ArrayList<>();
                for (int i = 0; i < attributes.length - 1; i++) {
                    String cur = attributes[i];
                    if (cur.equals("-nan")) {
                        list.add(-1.0);
                    } else {
                        list.add(Double.parseDouble(cur));
                    }
                }
                res.add(list);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found: " + file.getName());
        }
        return res;
    }

}
