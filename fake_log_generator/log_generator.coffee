fs = require "fs"

PID=[0,1]
NUMBER_OF_LOG=2
INST_NUMBER=20

# log file contents
# PC,BHR,pmu_value

# generate a fake log file for testing

# random int between a and b
random_int = (a,b) -> a + Math.floor(Math.random()*(b-a))

# add a taken/not take to the bhr
update_bhr = (bhr) -> (bhr << 1) | (random_int(0,100) % 2)

# generate a string value for a line of output
line = (pc,bhr,pmu_value) -> "#{pc},#{bhr},#{pmu_value}" 

# generate the log file(s)
generate_log = (pid) ->
	bhr=0
	pmu_value=0
	file = (line pc, bhr=update_bhr(bhr), pmu_value=pmu_value+random_int(0,20) for pc in [0..INST_NUMBER-1])

	# print out the file
	#console.log l for l in file

	# create a log file
	s=""
	create_log = (f) -> s+="#{l}\n" for l in file

	create_log(file)
	fs.writeFile "log/#{pid}_#{random_int(0,10000)}", s

generate_log pid  for i in [0..NUMBER_OF_LOG-1]  for pid in PID
