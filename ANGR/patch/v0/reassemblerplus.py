import collections
import logging
import os
import re
import subprocess
import tempfile

from angr.analyses.reassembler import BinaryError, FunctionLabel, Reassembler

log = logging.getLogger('boeing.reassembler')

functions_blacklist = {
    '_start',
    '_init',
    '_fini',
    '__gmon_start__',
    '__do_global_dtors_aux',
    'frame_dummy',
    'atexit',
    'deregister_tm_clones',
    'register_tm_clones',
    '__x86.get_pc_thunk.bx',
    '__libc_csu_init',
    '__libc_csu_fini',
}

data_blacklist = {
    '__TMC_END__',
    '_GLOBAL_OFFSET_TABLE_',
    '__JCR_END__',
    '__dso_handle',
    '__init_array_start',
    '__init_array_end',
    'stdout',
    'stderr',
    'stdin',
    'program_invocation_short_',
    'program_invocation_short_name',
    'program_invocation_name',
    '__progname_full',
    '_IO_stdin_used',
    'obstack_alloc_failed_hand',
    'program_invocation_short_',
    'optind',
    'optarg',
    '__progname',
    '_environ',
    'environ',
    '__environ',
    '_end', # ARM
}

class ReassemblerPlus(Reassembler):

    def __init__(self, debug=True):
        self._debug = debug

        try:
            Reassembler.__init__(self, syntax='at&t', collect_data_references=True)
            self.symbolize()
            self.static_init = list()
            self.deleted_functions = []
            reducedfunctions = []
            reduceddata = []

            {
                "AMD64": self.setup_x64,
                  "X86": self.setup_x86,
                "ARMEL": self.setup_arm32,
            }[self.project.arch.name]()

            # Filter out procedures in a similar fashion to the remove_unncessary_stuff() function from angr
            # We save the deleted functions for the CFI analysis
            # CLE only stores one label per address so we check section names as well to get it all
            # FINISH ME, we may need to search .fini for things to carry over, just removing the blank entry for now
            # FINISH ME, the original code also did a clear of pointer-array, not sure what for
            # FINISH ME, try and remove sub_ generated function names
            for p in self.procedures:
                if p.name not in functions_blacklist and p.section not in ('.plt', '.plt.got', '.init', '.fini'): 
                    reducedfunctions.append(p)
                else:
                    self.deleted_functions.append(p.name)
            self.procedures = reducedfunctions
            log.debug("removed functions: " + ",".join(self.deleted_functions))

            # Do the same thing for data while also making a copy of pointers in the .init_array section
            for d in self.data:
                if d.section_name == '.init_array':
                    for x in d.content:
                        if isinstance(x, FunctionLabel) and x.name not in ('frame_dummy'):
                            log.debug("static initializer " + x.name)
                            self.static_init.append(x.name)

                elif d.section_name in ('.ARM.exidx', ): # black listed sections
                    pass

                elif not len(d.labels): # No labels means noone can reference it
                    pass

                else:
                    # Check all the labels
                    ok = True
                    for _,lbl in d.labels:
                        if lbl.name in data_blacklist:
                            ok = False
                        # I think this will filter out bss things that the dynamic linker should do for us
                        esym = self.project.loader.main_object.hashtable.get(lbl.name)
                        if hasattr(esym, 'entry') and esym['st_info']['bind'] == 'STB_GLOBAL':
                            ok = False
                    if ok:
                        reduceddata.append(d)
            self.data = reduceddata
            

        except BinaryError as ex:
            raise ReassemblerError('Reassembler failed to load the binary. Here is the exception we caught: %s' % str(ex))


    def setup_x64(self):
        self.monkeypatch = self.patch_x64
        self.gccargs = ["-m64"] 

    def setup_x86(self):
        self.monkeypatch = self.patch_x86
        self.gccargs = ["-m32"] 

    def setup_arm32(self):
        self.monkeypatch = self.patch_arm32
        self.gccargs = []
        self.saveassembly() # Debug

    def patch_x64(self, insn):
        """ The current assembly output is slightly broken, we monkey patch it here """
        s = insn.split("\t")
        if len(s) > 2:
            # Turn orq,andq,xorq negative immediates back into 64 bits
            if s[1] in ('orq', 'andq', 'xorq') and s[2].startswith('$0x'):
                parts = s[2].split(", ")
                imm = int(parts[0][1:], 16)
                if (imm & 0x80000000) != 0:
                    imm += 0xFFFFFFFF00000000
                s[2] = "$0x%x, %s" % (imm, ", ".join(parts[1:]))

            elif s[1].startswith('mov'):
                if s[2] == '$0, ':  # Change "mov $0, " to "mov $0,0 "
                    s[2] = '$0,0 '
                if ":," in s[2]:    # Change "mov %fs:,<reg>" to "mov %fs:0,<reg>"
                    s[2] = s[2].replace(":,", ":0,")

        return '\t'.join(s) + '\n'


    def patch_x86(self, insn):
        return insn+'\n'


    def patch_arm32(self, insn):
        if "\t.type" in insn:
            insn = insn.replace('@', '%')  # ARM gcc wants %function not @function
        return insn.replace('$', '') +'\n'  # Weird no $ thing


    def saveassembly(self):
        assembly = self.assembly(comments=True, symbolized=True)
        # Save the assembly onto a temporary path
        fd, tmp_file_path = tempfile.mkstemp(prefix=os.path.basename(self.project.filename), suffix=".s")
        log.info("Generating assembly at %s", tmp_file_path)

        # Apply monkeypatching
        for line in assembly.split("\n"):
            os.write(fd, self.monkeypatch(line))

        # Add the required init_array seciton back in
        if len(self.static_init) > 0:
            os.write(fd, '\t.section .init_array\n')
            for s in self.static_init:
                if self.project.arch.bits == 64:
                    os.write(fd, '\t.quad {}\n'.format(s))
                else:
                    os.write(fd, '\t.long {}\n'.format(s))

        os.write(fd, "\n")
        os.close(fd)
        return tmp_file_path


    def generate(self, compiler, filename=None):
        # Get the assembly
        tmp_file_path = self.saveassembly()
        try:
            os.makedirs(os.path.dirname(filename), 0755)
        except OSError:
            pass

        # compile it with some version of gcc, arguments are:
        #   1. the target bit size
        #   2. the generated assembly file from above
        #   3. the output filename
        #   4. any shared libraries found in original binary that are not libc or libgcc
        compilecmd = [compiler, "-g"] + self.gccargs + [tmp_file_path, "-o", filename]
        for lib in self.project.loader.main_object.deps:
            if lib.startswith('libc.') or lib.startswith('libgcc_s') or lib.startswith('ld-linux'):
                continue
            compilecmd.append("-l"+re.sub(r'lib(\S+).so.*', '\\1', lib)) # this only works on Linux right now

        log.info("Running " + " ".join(compilecmd))
        retcode = subprocess.call(compilecmd)
        if not self._debug:
            os.remove(tmp_file_path)
        return retcode

