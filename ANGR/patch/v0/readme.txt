Again, I haven’t work on this for a while so the patches are against older version of the upstream sources but attached is:

 

·         angr.diff – diff between our angr and the upstream

·         cle.diff – diff between our cle and the upstream

o   hack method to deal with their 1:1 mapping of address to label which causes issue if two labels exist at the same address, we want it to pick the one related to shared library use

·         reassemblerplus.py – builds on Reassembler analysis tool and is base of our other work

o   setup_* - functions for setup based on architecture

o   patch_* - functions for monkeypatching the assembly output for issues
