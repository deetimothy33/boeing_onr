#include <stdio.h>
#include <stdint.h>
//#include <openssl/err.h>
//#include <openssl/evp.h>

#include <iostream>

using namespace std;

struct myclass { 
    public:
        myclass(int x);
        int geti(void);
    private:
        int i;
        std::ios_base::Init mInitializer;
};

myclass::myclass(int x) {
    i = x;
}

int myclass::geti(void) {
    return this->i;
}

std::ostream &operator<<(std::ostream &os, myclass &m) { 
    printf("os is %p", &os);
    return os << m.geti();
}

void simplecall()
{
    std::cout << "4\n";
}

int main()
{
    std::cout << "1\n";
    long long int ii = 0;
    long long int jj = 0;
    long int kk = 0;

    ii = 1;
    jj = 2;
    kk = 3;

    /* Test the disassembly errors from capstone x64 */
    ii = ii & 0xFFFFFFFFFFFFFFF0;    
    jj = jj | 0xFFFFFFFFFFFFFF00;    
    kk = kk ^ 0xFFFFFFFFFFFFF000;

    std::cout << "2\n";

    /* Couple calls to libc */
    printf("hello world %lld %lld %ld\n", ii, jj, kk);    
    printf("secondcall %lld\n", ii);
    printf("stdout is %p\n", &std::cout);
    /* Calls into c++ code */

    myclass x(10);
    std::cout << "3\n";

    //ERR_load_crypto_strings();
    //EVP_cleanup();

    simplecall();
    puts("End of program.");
}

