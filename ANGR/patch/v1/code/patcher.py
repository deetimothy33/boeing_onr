#!/usr/bin/env python

import argparse
import logging
import sys

from angr import Project
from cfi import CFIApplicator

logging.getLogger().setLevel(logging.INFO)
logging.getLogger("boeing").setLevel(logging.DEBUG)

parser = argparse.ArgumentParser(description="Applies CFI to a binary project")
parser.add_argument('--compiler', default='gcc', help='a specific compiler version to use during reassembly, i.e. gcc-4.7, the default is gcc')
parser.add_argument('--output', help='an optional output name, defaults to the same name with ".patched" added to the end')
parser.add_argument('input',  help='the input binary to apply CFI to')
args = parser.parse_args()
if not args.output:
    args.output = args.input+".patched"

project = Project(args.input, load_options={'auto_load_libs': False })
cfi = project.analyses.CFIApplicator()
sys.exit(cfi.generate(args.compiler, args.output))

