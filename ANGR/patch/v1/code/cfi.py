from collections import defaultdict
import logging
import pdb

from reassemblerplus import ReassemblerPlus
from angr.analyses import register_analysis

log = logging.getLogger("boeing.cfi")

class CFIApplicator(ReassemblerPlus):

    def __init__(self, debug=True):
        ReassemblerPlus.__init__(self, debug=debug)
        self.labelcounter = 0

        # Add our report string and do arch specific setup
        report = "CFI failed.  Halting program.\n\0"
        self.append_data("reportstring", report, len(report), readonly=True)
        self.map_returnpaths()
        self.apply_cfi()


    # FINISH ME, these exits are Linux specific

    def setup_x64(self):
        ReassemblerPlus.setup_x64(self)
        self.append_procedure("checkbad", """
            movl $reportstring, %edi
            call puts
            mov  $1, %rax
            mov  $1, %rbx
            int  $0x80
        """)
        self.generate_check_code = self.generate_check_code_x64


    def setup_x86(self):
        ReassemblerPlus.setup_x86(self)
        self.append_procedure("checkbad", """
            movl $reportstring, (%esp)
            call puts
            mov  $1, %eax 
            mov  $1, %ebx 
            int  $0x80
        """)
        self.generate_check_code = self.generate_check_code_x86


    def setup_arm32(self):
        ReassemblerPlus.setup_arm32(self)
        self.append_procedure("checkbad", """
            #ldr  r0, $reportstring
            #bl   $puts
            mov  r7, #1
            mov  r0, #0
            svc  #0
        """)
        self.generate_check_code = self.generate_check_code_x86


    def get_ret(self, ff):
        start = ff.startpoint
        ends = set()
        for ret_site in ff.ret_sites:
            bb = self.project.factory.block(ret_site.addr)
            last_instruction = bb.capstone.insns[-1]
            if last_instruction.mnemonic != u"ret":
                log.debug("bb at %s does not terminate with a ret in function %s" % (hex(int(bb.addr)),ff.name))
                break
            else:
                if last_instruction.op_str == "":
                    offset = 0
                else:
                    offset = int(last_instruction.op_str,16)
                ends.add((int(last_instruction.address),offset))
        else:
            if len(ends) == 0:
                log.debug("cannot find any ret in function %s" % ff.name)
            else:
                return ends #avoid "long" problems

        return []


    def map_returnpaths(self):
        self.inv_callsites = defaultdict(set)
        for f in self.cfg.functions.values():
            for callsite in f.get_call_sites():
                if f.get_call_return(callsite) is None:
                    continue
                self.labelcounter += 1
                lbl = "callreturn_{}".format(self.labelcounter)
                self.add_label(lbl, f.get_call_return(callsite))
                self.inv_callsites[f.get_call_target(callsite)].add(lbl)


    def generate_check_code_x86(self, validlabels, retlbl):
        ins = [""]
        ins.append("# Generated checks")
        ins.append("mov (%esp), %eax")
        for lbl in validlabels:
            ins.append("cmp $.{}, %eax".format(lbl))
            ins.append("je .{}".format(retlbl))
        ins.append("jmp checkbad")
        return "\n\t\t".join(ins)


    def generate_check_code_x64(self, validlabels, retlbl):
        ins = [""]
        ins.append("# Generated checks")
        ins.append("mov (%rsp), %rax")
        for lbl in validlabels:
            ins.append("cmp $.{}, %rax".format(lbl))
            ins.append("je .{}".format(retlbl))
        ins.append("jmp checkbad")
        return "\n\t\t".join(ins)


    def generate_check_code_arm32(self, validlabels, retlbl):
        ins = [""]
        ins.append("# Generated checks")
        ins.append("mov (%rsp), %rax")
        for lbl in validlabels:
            ins.append("cmp $.{}, %rax".format(lbl))
            ins.append("je .{}".format(retlbl))
        ins.append("jmp checkbad")
        return "\n\t\t".join(ins)


    def apply_cfi(self):
        for k,ff in self.cfg.functions.iteritems():
            print ff.name
            if ff.name in self.deleted_functions: # These remain in the cfg even though the reassembler isn't writing them back to file
                continue
            ## if ff.addr >= 0x1000000: # CFG location for PLT entries? Does not work for x86
            ##    continue
            if len(ff.jumpout_sites): # The other location for second copies of PLT things?
                continue
            if not ff.returning or not len(ff.ret_sites): # This seems to catch the leftover garbage blocks from byte alignments that get decoded anyways
                continue
            if ff.is_syscall:  # Syscall fakes
                continue

            log.debug("processing " +  ff.name)
            ends = self.get_ret(ff)
            for end,offset in ends:
                log.info("adding cfi call to function %s, ret %s, offset %s", ff.name, hex(end), hex(offset))
                returnto = self.inv_callsites[ff.addr]
                if len(returnto) > 0:
                    lbl = "retok_{}".format(self.labelcounter)
                    self.labelcounter += 1
                    self.add_label(lbl, end)
                    self.insert_asm(end, self.generate_check_code(returnto, lbl), before_label=True)
                elif ff.name not in ('main'):
                    # Without any valid call in locations, should we just halt if we get here then? No, init_functions fail if you do this
                    self.insert_asm(ff.addr, "\t# No valid callins to this function\n")

register_analysis(CFIApplicator, 'CFIApplicator')

