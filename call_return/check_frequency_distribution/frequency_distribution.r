#! /usr/bin/Rscript

#GOAL: 
# determine the how frequently call, return are able to be checked for integrity
# also of interest is what type of integrity check can be performed

#RESOURCES:
# https://stackoverflow.com/questions/29712225/number-of-call-and-ret-instruction-tracking-mismatch-while-using-intel-pin-on-x
# https://groups.yahoo.com/neo/groups/pinheads/conversations/topics/5212 


# CONSTANTS
N_WINDOW <- 10
N_INTEGRITY <- 2
FILE <- "../../asm/0_5886"

# FUNCTIONS
read_file = function(filepath) {
	con = file(filepath, "r")

	while ( TRUE ) {
		line = readLines(con, n = 1)
		if ( length(line) == 0 ) {
			break
		}
		print(line)
	}

	close(con)
}

# return true if the line contains a call instruction
contains_call = function(line){
	if(grepl("syscall", line)){
		return(FALSE)
	}

	return(grepl("^call", line))
}

# return true if the line contains a ret instruction
contains_ret = function(line){	
	if(grepl("sysret", line)){
		return(FALSE)
	}

	return(grepl("^ret", line))
}

process_file = function(filepath) {
	# matrix data[i][j]
	#	j := column, window number (first 100/N_WINDOW instructions)
	#	i := row, integrity check type
	data <- matrix(0, ncol=N_WINDOW, nrow=N_INTEGRITY)

	# get the number of instructions in the file
	n_inst <- file_lines(FILE)
	#print(n_inst)
	
	con = file(filepath, "r")
	line_count <- 0

	# registers to track
	n_count <- 0
	n_call <- 0
	n_ret <- 0

	# for each line
	while ( TRUE ) {
		line = readLines(con, n = 1)
		if ( length(line) == 0 ) {
			break
		}

		# determine the current window number
		window_n <- ceiling(line_count / (n_inst / N_WINDOW))

		# determine necessary information
		is_call <- contains_call(line)
		is_ret <- contains_ret(line)

		# update variables
		if( is_call ){
			n_count <- n_count + 1
			n_call <- n_call + 1
		}
		if( is_ret ){
			n_count <- n_count - 1
			n_ret <- n_ret + 1
		}

		# increment the appropriate row (integrity check number)
		# in the matrix
		# if the corresponding integrity check is possible
		
		# (1) count == 0
		check_n <- 1
		if( is_call ){
			data[check_n, window_n] <- data[check_n, window_n] + 1
		}
		
		# (2) count < 0
		check_n <- 2
		if( is_ret ){
			data[check_n, window_n] <- data[check_n, window_n] + 1
		}
		
		# (3) count > 0
		#check_n <- 3
		#if( FALSE ){
		#	data[check_n, window_n] <- data[check_n, window_n] + 1
		#}

		line_count <- line_count + 1
	}

	close(con)

	return(data)
}

file_lines = function(filepath){
	con <- file(filepath) 
	n_inst <- length(readLines(con))
	close(con)

	return(n_inst)
}


# SCRIPT
# process asm file
data <- process_file(FILE)
print(data)

pdf("graphics/check_frequency.pdf")
colors <- c("red","orange")
#colors <- c("red","orange","blue")

barplot(data, beside=TRUE, ylab="frequency", xlab="window number", col=colors, names.arg=1:N_WINDOW)
legend("topleft", c("call","return"), fill=colors)
dev.off()
