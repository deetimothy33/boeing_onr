#include <fstream>
#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <dirent.h>
#include <sys/stat.h>
#include <algorithm>
#include <sstream>
#include <time.h>

#include <boost/filesystem.hpp>

#include "PattYehPredictor.h"
#include "macro.h"
#include "stat.h"

#define PRINT_REPORT true
#define POOP true

// NOTE
// the following provide options if they are defined
// UNCOMMENT to define them
// NOTE

// use the base of the log folder
//#define BASE 1

// use buffer overflow logs
#define BUFFER_OVERFLOW 1

// use ROP logs
//#define ROP 1

// dump signature table to log file
#define SIGNATURE_LOG true
// dump logs incrementally during training
#define SIGNATURE_LOG_INCREMENTAL false
// fraction of training at which to dump signature table
// FRACTION = (SIGNATURE_LOG_FRACTION / 100) is the 
#define SIGNATURE_LOG_FRACTION 20

// skip the main classification and only perform the dump of signature logs
#define DUMP_ONLY true

using namespace std;

/**
 * given a set of log files, determine the accuracy of this scheme.
 * set of log files includes:
 * 	1. several log files for the same program
 * 	2. several log files for other programs
 * log file name format:
 * 	pid_rand.csv
 * 		process id
 * 		underscore character
 * 		random number
 * log files contents:
 * 	PC,BHR,pmu_value
 * 	PC,BHR,pmu_value
 * 	...
 * 	PC := program counter
 * 	BHR := branch history register
 * 	pmu_value := pmu value
 * accuracy is determined by:
 * 	1. develop the signatures using 1/2 the log files
 * 	2. test the prediction accuray with the other half of the log files
 * two forms of accuracy
 * 	1. when the same program generated signature and log file, prediction should be true
 * 	2. when the a different program generated signature and log file, prediction should be false
 */


//TODO
// update PYP.compare_signatures
// 	to compare based on lbr values
// 	this should be done once we have some intuition
// 		for how to compare based on signatures
//
// 1. create a function to aggregate LBR values in a signature
// 2. decide on the implementation of this function
//
// create incremental signature logs
//TODO


// log the PYP signature table to a log file
// This will create one log file per input file
#define SIGNATURE_LOG_FOLDER_REFERENCE "out/signature_table_log/reference"
#define SIGNATURE_LOG_FOLDER_ATTACK "out/signature_table_log/attack"
void log_signature_table(vector<log_data_t*> v, string dir){
	PattYehPredictor *pyp;
	ofstream output;
	clock_t t,start_t;
	vector<double> train_time_v,signature_time_v,classification_time_v;

	for(vector<log_data_t*>::iterator it=v.begin(); it!=v.end(); ++it){
		// START TIMING
		start_t=clock();
		// create instance of PYP
		pyp = new PattYehPredictor();
		// train the PYP with this log file
		pyp->train(*it);
		t=clock()-start_t;
		train_time_v.push_back((double)t/CLOCKS_PER_SEC);
		
		// added this to include mu, sigma computation in the time
		t=clock();
		pyp->compute_mu_sigma_values();
		t=clock()-t;
		signature_time_v.push_back((double)t/CLOCKS_PER_SEC);
		
		// time classification
		t=clock();
		pyp->classify(*it);
		t=clock()-t;
		classification_time_v.push_back((double)t/CLOCKS_PER_SEC);
		// END TIMING
		
		// create output file path
		boost::filesystem::path file_path((*it)->file_name);
		boost::filesystem::path log_dir_path(dir);
		boost::filesystem::path log_path = log_dir_path / file_path.filename();
		
		cout << "dumping signature log: " << log_path.string() << endl;
	
		// create directorys on file path
		string s=log_path.string();
		create_directory_path(s);

		// open output file
		// output the signature table
		output.open(log_path.string());
		output << pyp->signature_log();	
		output.close();

		delete pyp;
	}

	// print out timing report for signature generation
	double mean,sd;
	int count;
	cout<<"SIGNATURE LOG TIME DATA"<<endl;
	
	mean=mu(train_time_v);
	sd=sigma(train_time_v,mean);
	count=train_time_v.size();
	cout<<"TRAININING TIME"<<endl;
	cout<<"mu\t"<<mean<<endl;
	cout<<"standard deviation\t"<<sd<<endl;
	cout<<"count\t"<<count<<endl;
	
	mean=mu(signature_time_v);
	sd=sigma(signature_time_v,mean);
	count=signature_time_v.size();
	cout<<"SIGNATURE TABLE GENERATION TIME"<<endl;
	cout<<"mu\t"<<mean<<endl;
	cout<<"standard deviation\t"<<sd<<endl;
	cout<<"count\t"<<count<<endl;

	mean=mu(classification_time_v);
	sd=sigma(classification_time_v,mean);
	count=classification_time_v.size();
	cout<<"CLASSIFICATION TIME"<<endl;
	cout<<"mu\t"<<mean<<endl;
	cout<<"standard deviation\t"<<sd<<endl;
	cout<<"count\t"<<count<<endl;
}

// create a new log data with a fraction of the log file lines
// fraction -- a value between 1 and 100 denoting what percent of log file lines
// 	should be copied to the temp log
void create_fractional_log_data(log_data_t *log_data, log_data_t *fractional_log_data, int fraction){
	ofstream file;

	// create a new log file in a temporary folder (or memory)
	//cout << "temp/"+log_data->file_name << endl;
	
	// name the temp file
	fractional_log_data->file_name="out/temp/"+to_string(fraction)+"/"+log_data->file_name.substr(3);
	fractional_log_data->pid=log_data->pid;

	// create directorys on file path
	create_directory_path(fractional_log_data->file_name);
	
	// create a new file ( the TEMP file )
	// write FRACTION lines to the temp file
	file.open(fractional_log_data->file_name);
	if(file.is_open()){
		string line;

		// go back to beginning of file
		log_data->file.clear();
		log_data->file.seekg(0, ios::beg);

		// compute the total number of lines in a file
		int total=0;
		while(getline(log_data->file, line)) total++;
		
		// go back to beginning of file
		log_data->file.clear();
		log_data->file.seekg(0, ios::beg);

		// write FRACTION lines to the temp file
		float count=0.f;
		while(total!=0 && (count*100/total<=fraction) && getline(log_data->file,line)){
			file << line << endl;
			count++;
		}

		file.close();
	}else cout << "unable to open file" << endl;

	// open the temp file for reading
	fractional_log_data->file=ifstream(fractional_log_data->file_name);
}

// generate incremental signature table logs
void log_signature_table_incremental(vector<log_data_t*> &v, string dir){
	log_data_t* ld;
	vector<log_data_t*> fraction_v;
	
	//TODO I think there might be invalid elements of v
	//TODO this would explain the segfault

	// split the log data into sets
	// each element of the set corresponds to
	// PYP trained with a multiple of SIGNATURE_LOG_FRACTION% of the file

	for(int f=100; f>0; f-=SIGNATURE_LOG_FRACTION){
		// generate set of log data containing x*FRACTION
		fraction_v.clear();
		for(log_data_t* o : v){
			ld=new log_data_t;
			//TODO
			cout << o->file_name << endl;
			create_fractional_log_data(o, ld, f);
			cout << ld->file_name << endl;
			
			fraction_v.push_back(ld);
		}

		// create the folder where fraction_v will go
		string folder=dir+"/"+to_string(f);

		// output PYP training with this fraction of the data to the folder
		log_signature_table(fraction_v, folder);
	}
}

// given two vectors
// use training vector (tv) to train the PattYeh predictor
// use classification vector (cv) as logs classified against the predictor
// return accuracy
// (returned accuracy is per-line based.)
// (answers the question, what percentage of the log file lines were predicted correctly)
//
// NOTE: it is assumed only 0 PID is used to train the predictor TODO
double train_classify_accuracy(vector<log_data_t*> tv, vector<log_data_t*> cv, report_t &report){
	PattYehPredictor pyp;
	unsigned pid = 0l;
	stringstream report_ss;

	// train the predictor with a (or many) log files from the same program
	// (or different runs of the same program)
	for(vector<log_data_t*>::iterator it=tv.begin(); it!=tv.end(); ++it){
		pyp.train(*it);
		pid = (*it)->pid;
	}

	report.train_pid = pid;

	report_ss << "TRAINING PROGRAM PID: " << pid << endl;
	report_ss << "FILE\t|\tCLASSIFY" << endl;
	report_ss << "PID\t|\tVALUE" << endl;
	report_ss << "-------\t|\t-------" << endl;

	// classify all log files against the signature log file
	// compute an average accuracy
	double sum =0;
	int count =0;
	double value=0;
	for(vector<log_data_t*>::iterator it=cv.begin(); it!=cv.end(); ++it){
		value = pyp.classify(*it);

		// the classification value should be 100 if the PID are the same
		// it should be 0 if the PID are not the same.
		//
		// compute the respective distance from the value it should be
		sum += (pid==(*it)->pid) ? value : 1.0-value;
		count++;

		report.pid_v.push_back((*it)->pid);
		report.classify_value_v.push_back(value);

		report_ss << (*it)->pid << "\t|\t" << value << endl;
	}

	// print the report if requested
	if(PRINT_REPORT) cout << report_ss.str();

	// defecate if requested
	if(POOP) pyp.poop_self();

	return (sum / count);
}

//TODO why does classify always return 0?
//	BECAUSE
//		1. signature_accessor is always the same (0)
//		2. this is caused by local_accessor never being the same (not found in local_history table)
//		3. the result is that the signature_table[0] is overwritten by the most recent signature
//
//	self_signature always returns the same value when ./main.x is run
//		is this becuase the value is not found?
//		it means predict always returns the same value.

//TODO ensure the difference signature is computed correctly

int main(){
	//TODO
}

int old_main(){
	// given a set of program logs
	vector<string> sv;
	vector<log_data_t*> v;
	vector<log_data_t*> tv; // training vector
	vector<log_data_t*> cv; // classification vector

	// BASE OF LOG FOLDER
#ifdef BASE	
	// put all the log names into the vector 
	get_files_in_directory(sv, LOG_FOLDER);

	// create log_data_t from each log file
	create_log_data_vector(v, sv);

	// split into training vector and test vector
	for(vector<log_data_t*>::iterator it=v.begin(); it!=v.end(); ++it){
		if((*it)->pid == TRAIN_PID && tv.size()==0){
		//if(tv.size()==0){
		//if(cv.size()==v.size()){
			// training vector
			tv.push_back(*it);
		}else{
			// testing vector
			cv.push_back(*it);
		}
	}
#else
	// BUFFER OVERFLOW FOLDER
	// reference logs
	sv.clear();
#if BUFFER_OVERFLOW
	get_files_in_directory(sv, LOG_BUFFER_OVERFLOW_REFERENCE);
#elif ROP
	get_files_in_directory(sv, LOG_ROP_REFERENCE);
#endif
	create_log_data_vector(v, sv);

	// assign PID==0 for reference logs
	for(vector<log_data_t*>::iterator it=v.begin(); it!=v.end(); ++it){ (*it)->pid = 0; }
		
	// attack logs
	sv.clear();
#if BUFFER_OVERFLOW
	get_files_in_directory(sv, LOG_BUFFER_OVERFLOW_ATTACK);
#elif ROP
	get_files_in_directory(sv, LOG_ROP_ATTACK);
#endif
	create_log_data_vector(cv, sv);
	
	// assign PID==1 for attack logs
	for(vector<log_data_t*>::iterator it=cv.begin(); it!=cv.end(); ++it){ (*it)->pid = 1; }
	
	// take 1/2 reference logs in training vector
	unsigned int count=0;
	for(vector<log_data_t*>::iterator it=v.begin(); it!=v.end(); ++it){
		if(count < v.size()/2){ tv.push_back(*it); }
		else{ cv.push_back(*it); }

		count++;
	}
#endif

	// choose to skip the main portion if all I want to do is output logs
	if(!DUMP_ONLY){
		// compute the accuracy of the prediction scheme
		report_t report;
		double accuracy = train_classify_accuracy(tv, cv, report);

		// REPORT ACCURACY
		// per-measurement:	accuracy of correct classification of lines in log files
		// per-attack:		accuracy of correct classification of an entire log file (one attack measurement)
		cout << "PER-MEASUREMENT ACCURACY: " << accuracy << endl;
		cout << "PER-ATTACK ACCURACY: " << compute_per_file_accuracy(report) << endl;
	}else cout << "SKIPPING tain_classify_accuracy(), DUMP_ONLY set to true." << endl;
	
	// print out a log of the signature table if requested
	if(SIGNATURE_LOG && tv.size() > 0){
		// create a vector of all logs
		vector<log_data_t*> all_log_vector;
		vector<log_data_t*> rv;
		vector<log_data_t*> av;

		// create a vector for all the logs ( assumes tv and cv are disjoint (which they should be))
		for(vector<log_data_t*>::iterator it=tv.begin(); it!=tv.end(); ++it) all_log_vector.push_back(*it);
		for(vector<log_data_t*>::iterator it=cv.begin(); it!=cv.end(); ++it) all_log_vector.push_back(*it);

		// split into attack logs and reference logs
		// tv has the pid of the reference logs
		for(vector<log_data_t*>::iterator it=all_log_vector.begin(); 
				it!=all_log_vector.end(); ++it){
			// if it is a reference log
			if((*it)->pid == tv[0]->pid) rv.push_back(*it);
			else av.push_back(*it);
		}

		if(SIGNATURE_LOG_INCREMENTAL){
			// close all log files (poor design)
			//close_all_files(all_log_vector);

			// dump the signature table for each log file
			// INCREMENTALLY
			log_signature_table_incremental(rv, SIGNATURE_LOG_FOLDER_REFERENCE);
			log_signature_table_incremental(av, SIGNATURE_LOG_FOLDER_ATTACK);
		}else{
			// if we didn't do incremental logs, simply dump them at the end
			// dump the signature table for each log file
			log_signature_table(rv, SIGNATURE_LOG_FOLDER_REFERENCE);
			log_signature_table(av, SIGNATURE_LOG_FOLDER_ATTACK);
		}	
	}

	// free all log_data_t
	close_all_files(v);

	return 0;
}

