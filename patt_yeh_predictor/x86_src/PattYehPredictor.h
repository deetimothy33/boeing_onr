#ifndef PATT_YEH_PREDICTOR_H
#define PATT_YEH_PREDICTOR_H

#include <fstream>
#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <dirent.h>
#include <sys/stat.h>
#include <algorithm>
#include <sstream>

#include "PattYehPredictor.h"
#include "stat.h"
#include "macro.h"

using namespace std;

/**
 * OPPORTUNITIES FOR MODIFICATION
 *
 * update_local_history
 * 	1. what should local_history table contain
 * 	2. algorithm for replacing the current contents (if they already exist)
 *
 * update
 * 	1. what should signature_table contain
 * 	2. algorithm for replacing the current contents (if they already exist)
 *
 * extract_local_accessor
 * 	1. how is information in accessor_t used to access local_history_table
 *
 * signature_t
 * 	1. contents of signature?
 *
 * accessor_t
 * 	1. what information is used to access local history
 * 	2. what information is used to update local history
 */

// contains all information necessary to access PattYehPredictor signature
typedef struct{
	unsigned long long BHR;
	unsigned long long PC;
} accessor_t;

// holds (mu, sigma) information
typedef struct{
	// fixed counters
	double fc0;
	double fc1;
	double fc2;
	// programmable counters
	double pc0;
	double pc1;
	double pc2;
	double pc3;
} double_counter_t;

// holds lbr information
#define LBR_SIZE 16
typedef struct{
	unsigned long long from;
	unsigned long long to;
} lbr_t;

// signature information
// contains PMU values of some sort
//NOTE: if signature contains pointers, define operator=
struct signature_t;
typedef struct signature_t{
	unsigned long long TODO;
	// fixed counters
	unsigned long long fc0;
	unsigned long long fc1;
	unsigned long long fc2;
	// programmable counters
	unsigned long long pc0;
	unsigned long long pc1;
	unsigned long long pc2;
	unsigned long long pc3;
	// lbr mispredict register in order of (MSB==most resent) to (LSB==least resent)
	unsigned long long bhr_mispredict;
	lbr_t lbr[LBR_SIZE];
	// implementing (mu, sigma) scheme
	vector<signature_t> signature_v;
	double_counter_t mu;
	double_counter_t sigma;
	bool mu_sigma_computed;
} signature_t;

// this allows easy changes to what is used to access local history
typedef uint16_t local_accessor;

// this allows easy changes to what is stored in local history
typedef unsigned long long signature_accessor;
/*
typedef struct{
	unsigned long long value;
	unsigned long long ;
} signature_accessor;
*/

string poop_signature(signature_t &s);

//TODO added (&) to signature arguments
//TODO need to check that I did not assume they were coppied in the parameters anywhere
//TODO added to : update()

class PattYehPredictor{
	private:
		// the historys can be thought of as a mapping
		map<local_accessor, signature_accessor> local_history;
		map<signature_accessor, signature_t> signature_table;

		// transform the information in accessor into a local accessor
		local_accessor extract_local_accessor(accessor_t* accessor);

		// update local history
		void update_local_history(accessor_t* accessor);

		// populate accessor, signature given one line of a log file
		void populate_from_string(accessor_t &accessor, signature_t &signature, string line);

		// populate the signature based on the difference between two lines of a log file (line1 - line0)
		// accessor is returned for line1
		// "" may be provided for line0 to simply get signature for line 1
		void populate_difference(accessor_t &accessor, signature_t &signature, string line0, string line1);

		// compute whether the values approximatly match
		bool approximate_match(unsigned long long v0, unsigned long long v1);

		// true if value falls within MATCH_DEVIATION*sigma of mu
		bool mu_sigma_match(double mu, double sigma, double value);

		// compare predicted signature against a new signature
		// true => signatures match
		// false => no match
		bool compare_signature(signature_t *self_signature, signature_t *other_signature);

	public:
		PattYehPredictor();

		// update the branch prediction signature
		void update(accessor_t* accessor, signature_t &signature);

		// **signature is given a pointer to signature
		void predict(accessor_t* accessor, signature_t **signature);

		// update a predictor given a log file
		void train(log_data_t* l);

		// classify a log file using the predictor
		// The value returned is a % match
		double classify(log_data_t* l);

		// print out all predictor information in textual form
		void poop_self();

		// compute uncomputed (mu, sigma) values
		void compute_mu_sigma_values();

		// return a string containing a log of the signature table
		string signature_log();
};

#endif
