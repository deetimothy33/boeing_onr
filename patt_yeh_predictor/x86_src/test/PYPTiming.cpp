#include "gtest/gtest.h"

#include <chrono>
#include <vector>
#include <stdlib.h>

#include "PattYehPredictor.h"
#include "macro.h"

#define TRAIN_LOG "out/time/train.csv"
#define SIGNATURE_TABLE_LOG "out/time/signature_table.csv"

#define TRAINING_ITERATIONS 1000
#define TIMED_ITERATIONS 1000

#define TABLE_GENERATION_INTERVAL 10000
#define NUMBER_INTERVAL 100

using namespace std;
using namespace std::chrono;

void randomize_accessor(accessor_t &accessor){
	accessor.BHR = rand();
	accessor.PC = rand();
}

void randomize_signature(signature_t &signature){
	signature.fc0=rand();
	signature.fc1=rand();
	signature.fc2=rand();
	signature.pc0=rand();
	signature.pc1=rand();
	signature.pc2=rand();
	signature.pc3=rand();
		
	// LBR
	signature.bhr_mispredict=rand()%2;
	for(int i=0; i<LBR_SIZE; ++i){
		signature.lbr[i].from=rand();
		signature.lbr[i].to=rand();
	}
}

void output_time_metric(vector<double> &time_v){
	// compute average and std_deviation
	double average=mu(time_v);
	double std_dev=sigma(time_v,average);

	cout << "(average, std_deviation)" << endl;
	cout << "(" << average << ", " << std_dev << ") microseconds" << endl;
}

TEST(Time, Training){
	high_resolution_clock::time_point t1, t2;
	vector<double> time_v;
	
	PattYehPredictor pyp;
	accessor_t accessor;
	signature_t signature;

	t1=t2=high_resolution_clock::now();
	auto duration = duration_cast<microseconds>( t2 - t1 ).count();

	srand(101237846);

	// perform the training this number of times to derive (mu, sigma)
	for(int i=0;i<TRAINING_ITERATIONS;i++){
		// generate value to train PYP with
		randomize_accessor(accessor);
		randomize_signature(signature);
			
		t1 = high_resolution_clock::now();
		for(int j=0;j<TIMED_ITERATIONS;j++){
			// train PYP with value
			pyp.update(&accessor,signature);
		}
		t2 = high_resolution_clock::now();

		// store duration
		duration = duration_cast<microseconds>( t2 - t1 ).count() / TIMED_ITERATIONS;
		time_v.push_back(duration);
	}

	output_time_metric(time_v);
}

TEST(Time, SignatureTableGeneration){
	high_resolution_clock::time_point t1, t2;
	vector<double> time_v;
	
	PattYehPredictor pyp;
	accessor_t accessor;
	signature_t signature;

	t1=t2=high_resolution_clock::now();
	auto duration = duration_cast<microseconds>( t2 - t1 ).count();

	srand(101237846);

	cout << "(number trainings total), (generation time)" << endl;
	for(int i=0;i<NUMBER_INTERVAL;i++){
		// perform trainings
		for(int i=0;i<TABLE_GENERATION_INTERVAL;i++){
			randomize_accessor(accessor);
			randomize_signature(signature);
			pyp.update(&accessor,signature);
		}
		
		// time the signature table generation
		t1 = high_resolution_clock::now();
		pyp.signature_log();
		//TODO could also time the mu, sigma computations as an alternative
		t2 = high_resolution_clock::now();
		duration = duration_cast<microseconds>( t2 - t1 ).count();
		time_v.push_back(duration);

		// output time taken to generate signature table at each interval to a log file
		// 	a graph will be generated from this log file
		// (number trainings total) / (generation time)
		cout <<((i+1)*TABLE_GENERATION_INTERVAL)<<","<<(duration)<<endl;
	}
	
	output_time_metric(time_v);
}
