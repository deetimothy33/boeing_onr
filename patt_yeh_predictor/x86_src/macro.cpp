#include "macro.h"

void get_files_in_directory(vector<string> &out, const string &directory){
	DIR *dir;
	class dirent *ent;
	class stat st;
		
	dir = opendir(directory.c_str());
	while ((ent = readdir(dir)) != NULL) {
		const string file_name = ent->d_name;
		const string full_file_name = directory + "/" + file_name;

		if (file_name[0] == '.')
			continue;

		if (stat(full_file_name.c_str(), &st) == -1)
			continue;

		const bool is_directory = (st.st_mode & S_IFDIR) != 0;

		if (is_directory)
			continue;

		out.push_back(full_file_name);
	}

	closedir(dir);
}

void create_log_data_vector(vector<log_data_t*> &v, vector<string> &sv){
	log_data_t* ld;
	for(vector<string>::iterator it=sv.begin(); it!=sv.end(); ++it){
		ld = new log_data_t;
		ld->file_name = *it;

		string s = ld->file_name;
		string pid_s(find(s.begin(), s.end(), 'g')+2, find(s.begin(), s.end(), '_'));
		ld->pid = atoi(pid_s.c_str());

		//cout << atoi(pid_s.c_str()) << endl;

		ld->file = ifstream(*it);
		v.push_back(ld);
	}
}

void close_all_files(vector<log_data_t*> &v){
	for(vector<log_data_t*>::iterator it=v.begin(); it!=v.end(); ++it){
		(*it)->file.close();
		delete *it;
	}
}

void create_directory_path(string &file){
	boost::filesystem::path f(file);
	boost::filesystem::path dir(f.parent_path());
	boost::filesystem::path parent_dir(dir.parent_path());

	// this is keep tracking back up the heirarchy until it finds a directory that exists

	// create parent directory if it doesn't exist
	if(!(boost::filesystem::exists(parent_dir))){
		string s=dir.string();
		create_directory_path(s);
	}
	
	// create directory if it doesn't exist
	if(!(boost::filesystem::exists(dir))){
		boost::filesystem::create_directory(dir);
		
		//std::cout<<"Doesn't Exists"<<std::endl;
		//if (boost::filesystem::create_directory(dir))
			//std::cout << "....Successfully Created !" << std::endl;
	}
}
