#ifndef STAT_H
#define STAT_H

#include <cmath>
#include <vector>

#include "PattYehPredictor.h"

struct signature_t;

using namespace std;

typedef struct{
	int train_pid;
	vector<int> pid_v;
	vector<double> classify_value_v;
} report_t;

/**
 * compute standard deviation
 */
double sigma(vector<double> &v, double mu);

/**
 * compute average
 */
double mu(vector<double> &v);

/**
 * compute (mu, sigma) for a signature
 */
void compute_mu_sigma(signature_t *s);

double compute_per_file_accuracy(report_t &report);

#endif
