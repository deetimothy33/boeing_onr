#include "gtest/gtest.h"

#include "PattYehPredictor.h"
#include "macro.h"

// expect that the signature at pyp[pc,bhr] == value
void validate(PattYehPredictor &pyp, unsigned long long pc, unsigned long long bhr, unsigned long long value){
	accessor_t accessor;
	signature_t signature;
	signature_t *sp;
	
	accessor.PC = pc;
	accessor.BHR = bhr; 
	signature.TODO = value!=0 ? 0 : 1;
	pyp.predict(&accessor, &sp);
	signature = *sp;
	EXPECT_EQ(signature.TODO, value);
}

TEST(PattYehPredictor, Defecate){
	vector<string> sv;
	vector<log_data_t*> v;
	PattYehPredictor pyp;

	// put all the log names into the vector 
	//get_files_in_directory(sv, TEST_LOG_FOLDER);
	sv.push_back("src/test/log/0_9245");

	// create log_data_t from each log file
	create_log_data_vector(v, sv);

	// train the predictor with a (or many) log files from the same program
	// (or different runs of the same program)
	for(vector<log_data_t*>::iterator it=v.begin(); it!=v.end(); ++it){
		pyp.train(*it);
	}

	// present textual PYP information
	pyp.poop_self();	
}

TEST(PattYehPredictor, PopulateDifference){
	vector<string> sv;
	vector<log_data_t*> v;
	PattYehPredictor pyp;

	// put all the log names into the vector 
	//get_files_in_directory(sv, TEST_LOG_FOLDER);
	sv.push_back("src/test/log/0_9245");

	// create log_data_t from each log file
	create_log_data_vector(v, sv);

	// train the predictor with a (or many) log files from the same program
	// (or different runs of the same program)
	for(vector<log_data_t*>::iterator it=v.begin(); it!=v.end(); ++it){
		pyp.train(*it);
	}

	//TODO AH! the 3,0,6 replaces the first two because BHR is being used to access local history

	// ensure the training values match the logs
	validate(pyp,1,0,2); //TODO 1 xor 0 = 1
	validate(pyp,2,0,7); //TODO 2 xor 0 = 2
	validate(pyp,3,0,6); // 3 xor 0 = 3
	validate(pyp,4,1,10); 
	validate(pyp,5,2,7); 
	validate(pyp,6,4,14);
	validate(pyp,7,9,17); 
	validate(pyp,8,18,10); // 8 xor 10 = 26
	validate(pyp,9,36,10); 
	validate(pyp,10,72,2); // 10 xor 72 = 66
	validate(pyp,11,145,7);
	validate(pyp,15,2326,4);
	validate(pyp,17,9307,18);
	validate(pyp,19,37228,13);
	
	close_all_files(v);

}

TEST(PattYehPredictor, TrainAverage){
	// training takes an average of signature values across runs of a program
	//TODO
}

//TODO skip this test. 
// I am using a differencing mechanism for signatures now.
// This test provides a strait line by line signature.
TEST(PattYehPredictor, DISABLED_TrainReplace){
	vector<string> sv;
	vector<log_data_t*> v;
	PattYehPredictor pyp;

	// put all the log names into the vector 
	//get_files_in_directory(sv, TEST_LOG_FOLDER);
	sv.push_back("src/test/log/0_9245");

	// create log_data_t from each log file
	create_log_data_vector(v, sv);

	// train the predictor with a (or many) log files from the same program
	// (or different runs of the same program)
	for(vector<log_data_t*>::iterator it=v.begin(); it!=v.end(); ++it){
		pyp.train(*it);
	}

	// ensure the training values match the logs
	validate(pyp,0,0,5);
	validate(pyp,3,0,20);
	validate(pyp,6,4,51);
	validate(pyp,11,145,97);
	validate(pyp,15,2326,141);
	validate(pyp,17,9307,177);
	validate(pyp,19,37228,194);
	
	close_all_files(v);
}

TEST(PattYehPredictor, ClassifySelf){
	vector<string> sv;
	vector<log_data_t*> v;
	PattYehPredictor pyp;

	// put all the log names into the vector 
	//get_files_in_directory(sv, TEST_LOG_FOLDER);
	//sv.push_back("src/test/log/0_9245");
	sv.push_back("../../log/buffer_overflow/reference/5_1414");
	

	// create log_data_t from each log file
	create_log_data_vector(v, sv);

	// train the predictor with a (or many) log files from the same program
	// (or different runs of the same program)
	for(vector<log_data_t*>::iterator it=v.begin(); it!=v.end(); ++it){
		if((*it)->pid == TRAIN_PID) pyp.train(*it);
	}
	
	// classify a file against itself
	for(vector<log_data_t*>::iterator it=v.begin(); it!=v.end(); ++it){
		EXPECT_EQ(pyp.classify(*it), 1.0);
	}
	
	close_all_files(v);
}

TEST(PattYehPredictor, ClassifyReplace){
	vector<string> sv;
	vector<log_data_t*> v;
	PattYehPredictor pyp;

	// put all the log names into the vector 
	get_files_in_directory(sv, TEST_LOG_FOLDER);

	// create log_data_t from each log file
	create_log_data_vector(v, sv);

	// train the predictor with a (or many) log files from the same program
	// (or different runs of the same program)
	for(vector<log_data_t*>::iterator it=v.begin(); it!=v.end(); ++it){
		if((*it)->pid == TRAIN_PID) pyp.train(*it);
	}

	//TODO
	
	close_all_files(v);
}

TEST(PattYehPredictor, UpdatePredict){
	PattYehPredictor pyp;
	accessor_t accessor;
	signature_t signature;
	signature_t *sp = &signature;

	accessor.BHR = 0x1;
	accessor.PC = 0xF;
	signature.TODO = 101;
	pyp.update(&accessor, signature);

	// test if a value can be set and subsequently read
	signature.TODO = 0;
	pyp.predict(&accessor, &sp);
	EXPECT_EQ(signature.TODO, 101);

	// test that signature becomes 0 when accessing a new area
	accessor.BHR = 0x1;
	accessor.PC = 0xE;
	signature.TODO = 1;
	pyp.predict(&accessor, &sp);
	EXPECT_EQ(signature.TODO, 0);

	accessor.PC += 1;
	accessor.BHR += 1;
	signature.TODO = 1;
	pyp.predict(&accessor, &sp);
	EXPECT_EQ(signature.TODO, 0);
}
