#include "gtest/gtest.h"

#include <cmath>

#include "stat.h"

class StatFixture : public ::testing::Test {
	protected:
		vector<signature_t> sv;
		vector<double> v;

		virtual void SetUp() {
			//TODO sv

			v.push_back(2.0);
			v.push_back(3.0);
			v.push_back(5.0);
			v.push_back(10.0);
		}
};

//TODO check that mu, sigma are computed correctly

TEST_F(StatFixture, Sigma){
	// 9 + 4 + 0 + 25 = 38
	// sqrt(38/4)
	EXPECT_EQ(sigma(v, mu(v)), sqrt(38.0/4.0));
}

TEST_F(StatFixture, Mu){
	EXPECT_EQ(mu(v), 5.0);
}

TEST_F(StatFixture, ComputeMuSigma){
}
