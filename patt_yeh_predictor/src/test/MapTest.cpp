#include "gtest/gtest.h"

#include <map>

using namespace std;

TEST(Map, NewValue){
	map<string,string> string_map;

	EXPECT_EQ(string_map["a"], "");

	string_map["a"] = "first";
	EXPECT_EQ(string_map["a"], "first");

	EXPECT_EQ(string_map["b"], "");
}
