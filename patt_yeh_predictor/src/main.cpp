#include <fstream>
#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <dirent.h>
#include <sys/stat.h>
#include <algorithm>
#include <sstream>
#include <time.h>

#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>

#include "PattYehPredictor.h"
#include "macro.h"
#include "stat.h"

#define PRINT_REPORT true
#define POOP true

// NOTE
// the following provide options if they are defined
// UNCOMMENT to define them
// NOTE

// use the base of the log folder
//#define BASE 1

// use buffer overflow logs
#define BUFFER_OVERFLOW 1

// use ROP logs
//#define ROP 1

// dump signature table to log file
#define SIGNATURE_LOG true
// dump logs incrementally during training
#define SIGNATURE_LOG_INCREMENTAL false
// fraction of training at which to dump signature table
// FRACTION = (SIGNATURE_LOG_FRACTION / 100) is the 
#define SIGNATURE_LOG_FRACTION 20

// skip the main classification and only perform the dump of signature logs
#define DUMP_ONLY true

using namespace std;
//using namespace boost::filesystem;

/**
 * given a set of log files, determine the accuracy of this scheme.
 * set of log files includes:
 * 	1. several log files for the same program
 * 	2. several log files for other programs
 * log file name format:
 * 	pid_rand.csv
 * 		process id
 * 		underscore character
 * 		random number
 * log files contents:
 * 	PC,BHR,pmu_value
 * 	PC,BHR,pmu_value
 * 	...
 * 	PC := program counter
 * 	BHR := branch history register
 * 	pmu_value := pmu value
 * accuracy is determined by:
 * 	1. develop the signatures using 1/2 the log files
 * 	2. test the prediction accuray with the other half of the log files
 * two forms of accuracy
 * 	1. when the same program generated signature and log file, prediction should be true
 * 	2. when the a different program generated signature and log file, prediction should be false
 */


//TODO
// update PYP.compare_signatures
// 	to compare based on lbr values
// 	this should be done once we have some intuition
// 		for how to compare based on signatures
//
// 1. create a function to aggregate LBR values in a signature
// 2. decide on the implementation of this function
//
// create incremental signature logs
//TODO


// log the PYP signature table to a log file
// This will create one log file per input file
#define SIGNATURE_LOG_FOLDER_REFERENCE "out/signature_table_log/reference"
#define SIGNATURE_LOG_FOLDER_ATTACK "out/signature_table_log/attack"
void log_signature_table(vector<log_data_t*> v, string dir){
	PattYehPredictor *pyp;
	ofstream output;
	clock_t t,start_t;
	vector<double> train_time_v,signature_time_v,classification_time_v;

	for(vector<log_data_t*>::iterator it=v.begin(); it!=v.end(); ++it){
		// START TIMING
		start_t=clock();
		// create instance of PYP
		pyp = new PattYehPredictor();
		// train the PYP with this log file
		pyp->train(*it);
		t=clock()-start_t;
		train_time_v.push_back((double)t/CLOCKS_PER_SEC);

		// added this to include mu, sigma computation in the time
		t=clock();
		pyp->compute_mu_sigma_values();
		t=clock()-t;
		signature_time_v.push_back((double)t/CLOCKS_PER_SEC);

		// time classification
		t=clock();
		pyp->classify(*it);
		t=clock()-t;
		classification_time_v.push_back((double)t/CLOCKS_PER_SEC);
		// END TIMING

		// create output file path
		boost::filesystem::path file_path((*it)->file_name);
		boost::filesystem::path log_dir_path(dir);
		boost::filesystem::path log_path = log_dir_path / file_path.filename();

		cout << "dumping signature log: " << log_path.string() << endl;

		// create directorys on file path
		string s=log_path.string();
		create_directory_path(s);

		// open output file
		// output the signature table
		output.open(log_path.string());
		output << pyp->signature_log();	
		output.close();

		delete pyp;
	}

	// print out timing report for signature generation
	double mean,sd;
	int count;
	cout<<"SIGNATURE LOG TIME DATA"<<endl;

	mean=mu(train_time_v);
	sd=sigma(train_time_v,mean);
	count=train_time_v.size();
	cout<<"TRAININING TIME"<<endl;
	cout<<"mu\t"<<mean<<endl;
	cout<<"standard deviation\t"<<sd<<endl;
	cout<<"count\t"<<count<<endl;

	mean=mu(signature_time_v);
	sd=sigma(signature_time_v,mean);
	count=signature_time_v.size();
	cout<<"SIGNATURE TABLE GENERATION TIME"<<endl;
	cout<<"mu\t"<<mean<<endl;
	cout<<"standard deviation\t"<<sd<<endl;
	cout<<"count\t"<<count<<endl;

	mean=mu(classification_time_v);
	sd=sigma(classification_time_v,mean);
	count=classification_time_v.size();
	cout<<"CLASSIFICATION TIME"<<endl;
	cout<<"mu\t"<<mean<<endl;
	cout<<"standard deviation\t"<<sd<<endl;
	cout<<"count\t"<<count<<endl;
}

// create a new log data with a fraction of the log file lines
// fraction -- a value between 1 and 100 denoting what percent of log file lines
// 	should be copied to the temp log
void create_fractional_log_data(log_data_t *log_data, log_data_t *fractional_log_data, int fraction){
	ofstream file;

	// create a new log file in a temporary folder (or memory)
	//cout << "temp/"+log_data->file_name << endl;

	// name the temp file
	fractional_log_data->file_name="out/temp/"+to_string(fraction)+"/"+log_data->file_name.substr(3);
	fractional_log_data->pid=log_data->pid;

	// create directorys on file path
	create_directory_path(fractional_log_data->file_name);

	// create a new file ( the TEMP file )
	// write FRACTION lines to the temp file
	file.open(fractional_log_data->file_name);
	if(file.is_open()){
		string line;

		// go back to beginning of file
		log_data->file.clear();
		log_data->file.seekg(0, ios::beg);

		// compute the total number of lines in a file
		int total=0;
		while(getline(log_data->file, line)) total++;

		// go back to beginning of file
		log_data->file.clear();
		log_data->file.seekg(0, ios::beg);

		// write FRACTION lines to the temp file
		float count=0.f;
		while(total!=0 && (count*100/total<=fraction) && getline(log_data->file,line)){
			file << line << endl;
			count++;
		}

		file.close();
	}else cout << "unable to open file" << endl;

	// open the temp file for reading
	fractional_log_data->file=ifstream(fractional_log_data->file_name);
}

// generate incremental signature table logs
void log_signature_table_incremental(vector<log_data_t*> &v, string dir){
	log_data_t* ld;
	vector<log_data_t*> fraction_v;

	//TODO I think there might be invalid elements of v
	//TODO this would explain the segfault

	// split the log data into sets
	// each element of the set corresponds to
	// PYP trained with a multiple of SIGNATURE_LOG_FRACTION% of the file

	for(int f=100; f>0; f-=SIGNATURE_LOG_FRACTION){
		// generate set of log data containing x*FRACTION
		fraction_v.clear();
		for(log_data_t* o : v){
			ld=new log_data_t;
			//TODO
			cout << o->file_name << endl;
			create_fractional_log_data(o, ld, f);
			cout << ld->file_name << endl;

			fraction_v.push_back(ld);
		}

		// create the folder where fraction_v will go
		string folder=dir+"/"+to_string(f);

		// output PYP training with this fraction of the data to the folder
		log_signature_table(fraction_v, folder);
	}
}

// given two vectors
// use training vector (tv) to train the PattYeh predictor
// use classification vector (cv) as logs classified against the predictor
// return accuracy
// (returned accuracy is per-line based.)
// (answers the question, what percentage of the log file lines were predicted correctly)
//
// NOTE: it is assumed only 0 PID is used to train the predictor TODO
double train_classify_accuracy(vector<log_data_t*> tv, vector<log_data_t*> cv, report_t &report){
	PattYehPredictor pyp;
	unsigned pid = 0l;
	stringstream report_ss;

	// train the predictor with a (or many) log files from the same program
	// (or different runs of the same program)
	for(vector<log_data_t*>::iterator it=tv.begin(); it!=tv.end(); ++it){
		pyp.train(*it);
		pid = (*it)->pid;
	}

	report.train_pid = pid;

	report_ss << "TRAINING PROGRAM PID: " << pid << endl;
	report_ss << "FILE\t|\tCLASSIFY" << endl;
	report_ss << "PID\t|\tVALUE" << endl;
	report_ss << "-------\t|\t-------" << endl;

	// classify all log files against the signature log file
	// compute an average accuracy
	double sum =0;
	int count =0;
	double value=0;
	for(vector<log_data_t*>::iterator it=cv.begin(); it!=cv.end(); ++it){
		value = pyp.classify(*it);

		// the classification value should be 100 if the PID are the same
		// it should be 0 if the PID are not the same.
		//
		// compute the respective distance from the value it should be
		sum += (pid==(*it)->pid) ? value : 1.0-value;
		count++;

		report.pid_v.push_back((*it)->pid);
		report.classify_value_v.push_back(value);

		report_ss << (*it)->pid << "\t|\t" << value << endl;
	}

	// print the report if requested
	if(PRINT_REPORT) cout << report_ss.str();

	// defecate if requested
	if(POOP) pyp.poop_self();

	return (sum / count);
}


// read file line-by-line
// create one log_line_t for each line
// 	push this into vector v
// difference_consecutive==true => take the difference between consecutive line counter values
void read_file(vector<log_line_t> &v, const string &file){
	// read the log file into memory
	ifstream infile;

	infile.open(file);
	if(!infile) { cout << "unable to open file." << endl; exit(1); }

	string line;
	while(infile>>line){
		stringstream stream_line(line);
		string value;
		int i=0;
		log_line_t log_line;
		while(getline(stream_line,value,',')){
			// parse the ith element in the log file line
			if(i==0){
				log_line.attack_site=stoi(value);
			}else if(i==7){
				//log_line.branch_history=stoul(value);
				log_line.branch_history=stoul(value);
			}else{
				log_line.counter[i-1]=stoul(value);
			}
			i++;
		}

		v.push_back(log_line);
	}
}

void read_file_difference_consecutive(vector<log_line_t> &v, const string &file){
	// read the log file into memory
	ifstream infile;

	infile.open(file);
	if(!infile) { cout << "unable to open file." << endl; exit(1); }

	string line;
	log_line_t prev_log_line;
	// gets rid of compiler error
	prev_log_line.counter[0]=0;
	bool first_line=true;
	while(infile>>line){
		stringstream stream_line(line);
		string value;
		int i=0;
		log_line_t log_line;
		while(getline(stream_line,value,',')){
			// parse the ith element in the log file line
			if(i==0){
				log_line.attack_site=stoi(value);
			}else if(i==7){
				log_line.branch_history=stoul(value);
			}else{
				//cout << value << "\t";
				log_line.counter[i-1]=stoul(value);
				//cout << stoul(value)<< "\t";
				if(!first_line){
					// has the counter rolled over?
					if(log_line.counter[i-1]<prev_log_line.counter[i-1]){
						// roll over
						// MAX_COUNTER_VALUE - prev_log_line + log_line
						// (2^32-1) -...
						log_line.counter[i-1]+=4294967295-prev_log_line.counter[i-1];
					}else{
						// no roll over
						// subtract prev_log_line from current
						log_line.counter[i-1]=log_line.counter[i-1]-prev_log_line.counter[i-1];
					}
				}
				//cout << log_line.counter[i-1] << "\t"<< prev_log_line.counter[i-1]<< endl;
				prev_log_line.counter[i-1]=stoul(value);
			}
			i++;
		}

		if(first_line){
			first_line=false;
		}else{
			v.push_back(log_line);
		}
	}
}

void train_pyp(PattYehPredictor &pyp, vector<log_line_t> &v){
	// train the PYP with each element of v
	for(auto &value : v){
		//cout << value.counter[3] << endl;
		pyp.train(value);
	}
}

void classify_pyp(PattYehPredictor &pyp, vector<log_line_t> &v){
	// classify each element of v using PYP

	//TODO
}

#define INPUT_FILE "../log/ardupilot/50_attack_frequency/aggregate_data.csv"
#define ML_OUTPUT_FILE "ml_aggregate_data.csv"
#define MU_SIGMA_OUTPUT_FILE "mu_sigma_aggregate_data.csv"
#define ATTACK_SITE_TO_INDEX_FILE "attack_site_to_index.csv"

#define INPUT_FOLDER_BASE "../log/ardupilot/50_attack_frequency/site"

int main_separate(){
	ofstream outfile;
	stringstream ss;
	string d;
	
	vector<log_line_t> v;
	vector<log_line_t> v_attack;
	vector<log_line_t> v_non_attack;

	PattYehPredictor* pyp_a;
	PattYehPredictor* pyp_n;

	// generate separate tables 
	// 	for multiple percentages of attack signatures
	// 	for each data file
	// 	for attack and non-attack

	// percentages of attack signatures in attack_tables
	for(int attack_percent=0;attack_percent<=100;attack_percent+=10){
		// create directory if it does not exist
		ss.str("");
		ss<<"out/"<<attack_percent<<"/";
		string ddd=ss.str();
		boost::filesystem::path dir2(ddd);
		boost::filesystem::create_directory(dir2);

		// for each attack site
		for(int site=0;site<4;site++){
			// create directory if it does not exist
			ss.str("");
			ss<<ddd<<"site"<<site<<"/";
			string td=ss.str();
			boost::filesystem::path dir(td);
			boost::filesystem::create_directory(dir);

			// for all signature table sizes
			for(int i=4;i<1025;i=i*2){
				SIGNATURE_TABLE_SIZE=i;

				// create directory if it does not exist
				ss.str("");
				ss<<td<<i<<"/";
				string directory=ss.str();
				boost::filesystem::path dir0(directory);
				boost::filesystem::create_directory(dir0);

				// for all raw data files
				ss.str("");
				ss<<INPUT_FOLDER_BASE<<site;
				d=ss.str();
				boost::filesystem::path in_dir(d);
				int file_n=0;
				for(auto& input_file : boost::make_iterator_range(boost::filesystem::directory_iterator(in_dir), {})){
					pyp_a=new PattYehPredictor();
					pyp_n=new PattYehPredictor();
					PattYehPredictor& pyp_attack=*pyp_a;
					PattYehPredictor& pyp_non_attack=*pyp_n;
					v.clear();
					v_attack.clear();
					v_non_attack.clear();

					// read the input file
					read_file_difference_consecutive(v,input_file.path().string());

					// split vector into attack and not attack vectors 
					for(auto const& val : v){
						if(val.attack_site>=0) v_attack.push_back(val);
						else v_non_attack.push_back(val);
					}

					// replace some of the v_attack rows with non_attack
					// depends on attack percent
					// no rows are replaced for attack_percent==100
					int vn_index=0;
					for(unsigned int va_index=0;va_index<v_attack.size();va_index++){
						if((rand()%100)>=attack_percent){
							// attack row is replaced with non_attack row
							v_attack[va_index]=v_non_attack[vn_index];
							vn_index++;
						}
					}
					//TODO the above was added, is it correct?

					// train PYP
					train_pyp(pyp_attack,v_attack);
					train_pyp(pyp_non_attack,v_non_attack);

					// create output folder
					boost::filesystem::path dir1(directory+"/"+to_string(file_n));
					boost::filesystem::create_directory(dir1);

					// output signature table
					outfile.open(directory+"/"+to_string(file_n)+"/"+"attack_"+ML_OUTPUT_FILE);
					outfile << pyp_attack.signature_log_ml();
					outfile.close();
					outfile.open(directory+"/"+to_string(file_n)+"/"+"non_attack_"+ML_OUTPUT_FILE);
					outfile << pyp_non_attack.signature_log_ml();
					outfile.close();

					// output (mu,sigma) table
					outfile.open(directory+"/"+to_string(file_n)+"/"+"attack_"+MU_SIGMA_OUTPUT_FILE);
					outfile << pyp_attack.mu_sigma_log();
					outfile.close();
					outfile.open(directory+"/"+to_string(file_n)+"/"+"non_attack_"+MU_SIGMA_OUTPUT_FILE);
					outfile << pyp_non_attack.mu_sigma_log();
					outfile.close();

					// output attack site to index mapping
					outfile.open(directory+"/"+to_string(file_n)+"/"+"attack_"+ATTACK_SITE_TO_INDEX_FILE);
					outfile << pyp_attack.attack_site_mapping_log();
					outfile.close();
					outfile.open(directory+"/"+to_string(file_n)+"/"+"non_attack_"+ATTACK_SITE_TO_INDEX_FILE);
					outfile << pyp_non_attack.attack_site_mapping_log();
					outfile.close();

					// this output appears to be correct
					//pyp_attack.poop_self();
					//pyp_non_attack.poop_self();

					// delete old pyp structures
					delete pyp_a;
					delete pyp_n;

					file_n++;
				}
			}
		}
	}

	return 0;
}

int main_aggregate(){
	vector<log_line_t> v;
	ofstream outfile;

	// read the input file
	read_file(v,INPUT_FILE);

	for(int i=4;i<1025;i=i*2){
		SIGNATURE_TABLE_SIZE=i;

		PattYehPredictor pyp;
		stringstream ss;
		ss<<"out/"<<i<<"/";
		string directory=ss.str();

		// create directory if it does not exist
		boost::filesystem::path dir(directory);
		boost::filesystem::create_directory(dir);

		//TODO make sure this isn't cumulative over all data read
		// train PYP
		train_pyp(pyp,v);

		// output signature table
		outfile.open(directory+ML_OUTPUT_FILE);
		outfile << pyp.signature_log_ml();
		outfile.close();

		// output (mu,sigma) table
		outfile.open(directory+MU_SIGMA_OUTPUT_FILE);
		outfile << pyp.mu_sigma_log();
		outfile.close();

		// output attack site to index mapping
		outfile.open(directory+ATTACK_SITE_TO_INDEX_FILE);
		outfile << pyp.attack_site_mapping_log();
		outfile.close();

		// classify using signature table
		//TODO will need to split v into
		//	training vector
		//	test vector
		//classify_pyp(pyp,v);

		// testing
		//pyp.poop_self();
	}

	return 0;
}


int SIGNATURE_TABLE_SIZE;

int main(){
	return main_separate();
	//return main_aggregate();
}
