#include "stat.h"

//TODO why is sigma always 0
//TODO why is value-mu always 0?

//TODO in other words, why is the value for every signature in the vector always the same?

// tests show that mu,sigma are computed correctly

double sigma(vector<double> &v, double mu){
	double var=0.0;
	for(auto const& value : v){
		var += (value - mu) * (value - mu);
		//cout << (value-mu);
	}
	var /= v.size();
	
	return sqrt(var);
}

double mu(vector<double> &v){
	double sum=0.0;
	int count=0;
	for(auto const& value : v){
		sum += value;
		count++;
	}

	if(sum/count<0){
		cout<<"error: negative mu value"<<endl;
	}
	
	return sum/count;
}

void compute_mu_sigma(signature_t *s){
	// 0. clear old values from vector
	// 1. collect values into vector
	// 2. compute mu
	// 3. compute sigma

	vector<double> v;

	// for each counter
	for(int i=0;i<6;i++){
		// for each thing in signature_v
		for(auto const& value : s->signature_v) v.push_back(value.counter[i]);
		s->mu[i] = mu(v);
		s->sigma[i] = sigma(v, s->mu[i]);
		v.clear();
	}

	s->mu_sigma_computed = true;

	
	/*
	vector<double> v;

	v.clear();
	for(auto const& value : s->signature_v) v.push_back(value.fc0);
	s->mu.fc0 = mu(v);
	s->sigma.fc0 = sigma(v, s->mu.fc0);

	v.clear();
	for(auto const& value : s->signature_v) v.push_back(value.fc1);
	s->mu.fc1 = mu(v);
	s->sigma.fc1 = sigma(v, s->mu.fc1);

	v.clear();
	for(auto const& value : s->signature_v) v.push_back(value.fc2);
	s->mu.fc2 = mu(v);
	s->sigma.fc2 = sigma(v, s->mu.fc2);
	
	v.clear();
	for(auto const& value : s->signature_v) v.push_back(value.pc0);
	s->mu.pc0 = mu(v);
	s->sigma.pc0 = sigma(v, s->mu.pc0);
	
	v.clear();
	for(auto const& value : s->signature_v) v.push_back(value.pc1);
	s->mu.pc1 = mu(v);
	s->sigma.pc1 = sigma(v, s->mu.pc1);
	
	v.clear();
	for(auto const& value : s->signature_v) v.push_back(value.pc2);
	s->mu.pc2 = mu(v);
	s->sigma.pc2 = sigma(v, s->mu.pc2);
	
	v.clear();
	for(auto const& value : s->signature_v) v.push_back(value.pc3);
	s->mu.pc3 = mu(v);
	s->sigma.pc3 = sigma(v, s->mu.pc3);
	*/

	//cout << "size: " << s->signature_v.size() << endl;
}

double compute_per_file_accuracy(report_t &report){
	double correct_class=0, incorrect_class=0;

	for(unsigned int i=0; i<report.pid_v.size(); i++){
		// if the attack file was classified correctly...
		if( 	(report.train_pid==report.pid_v[i] && report.classify_value_v[i] > CLASSIFY_THRESHOLD) ||
			(report.train_pid!=report.pid_v[i] && report.classify_value_v[i] < CLASSIFY_THRESHOLD)
		){
			correct_class++;
		}else{
			incorrect_class++;
		}
	}

	double accuracy = correct_class / (correct_class + incorrect_class);

	// NAN check
	return accuracy!=accuracy ? 0.0 : accuracy;
}
