#include "PattYehPredictor.h"
	
//TODO difference schemes for extracting a local accress can be devised

local_accessor PattYehPredictor::extract_local_accessor(accessor_t* accessor){
	// XOR the lowest 16 bits of BHR with PC	
	//cout << (accessor->BHR ^ accessor->PC) << endl;	
	
	//TODO
	return accessor->BHR ^ accessor->PC;
	//return accessor->BHR;
}

//TODO rethink the local history

//TODO devise schemes for updating signature_accessor

void PattYehPredictor::update_local_history(accessor_t *accessor){
	local_accessor la = extract_local_accessor(accessor);

	signature_accessor sa = local_history[la];
	
#if BH_BASED_LOCAL_HISTORY
	// change the signature accessor based on accessor

	// COUNTER SCHEME
	// a simple scheme is a 1-bit counter based on BHR
	
	// another is to just use BHR
	//sa = accessor->BHR;
	
	// another is a 2-bit predictor
	//sa += accessor->BHR & 0x2;
	
	// IF ANY BRANCH TAKEN SCHEME
	// each time this block of code is reached
	// it is updated with 1 if anything in BHR is 1
	//sa = (sa << 1) & 0x7; // 3 long
	//sa = (sa << 1) & 0xF; // 4 long
	//sa = (sa << 1) & 0x1F; // 5 long
	//sa = (sa << 1) & 0x3F; // 6 long
	//sa = (sa << 1) & 0xFF; // 8 long
	//sa |= (accessor->BHR > 0) ? 1 : 0;
	
	// COUNT IF ANY BRANCH TAKEN 
	// (out of last n branches)
	// (max out at 32)
	//
	// increment if a branch has been taken
	unsigned long mask;
	unsigned limit = SIGNATURE_TABLE_SIZE;
	// mask==the number of previous branches to consider
	//mask = 0x1F; // 5
	mask = 0xFF; // 8
	//mask = 0xFFFF; // 16
	if(accessor->BHR & mask) sa++;
	// decrement if a branch has not been taken
	else sa--;
	// make sure sa remains within range
	// remember, sa becomes an index to the table
	// 	thus limit-1 is appropriate
	sa = sa<0 ? 0 : sa;
	sa = sa>=limit ? limit-1 : sa;
#else
	//accessor->log_line->counter[0];

	// counter value based branch history
	// take MSB of counter values
	unsigned limit = SIGNATURE_TABLE_SIZE;

	srand(accessor->log_line->attack_site);
	if(rand() % 2 == 0) sa++;
	// decrement if a branch has not been taken
	else sa--;
	// make sure sa remains within range
	// remember, sa becomes an index to the table
	// 	thus limit-1 is appropriate
	sa = sa<0 ? 0 : sa;
	sa = sa>=limit ? limit-1 : sa;
#endif

	local_history[la] = sa;
}

void PattYehPredictor::populate_from_string(accessor_t &accessor, signature_t &signature, string line){
/*
	stringstream ss = stringstream(line);
	string value;

	// read while you geta  ,
	int pos = 0;
	while(getline(ss, value, ',')){
		// do stuff based on the position within the line
		// NOTE: I do it this way to support multiple versions of the log files
		switch(pos){
			case 0: accessor.PC = stoll(value);
				break;
			case 1: accessor.BHR = stoll(value);
				break;
			case 2: signature.fc0 = stoll(value);
				// signature.TODO gets the first counter for testing purposes
				signature.TODO = stoll(value);
				break;
			case 3: signature.fc0 = stoll(value);
				break;
			case 4: signature.fc1 = stoll(value);
				break;
			case 5: signature.fc2 = stoll(value);
				break;
			case 6: signature.pc0 = stoll(value);
				break;
			case 7: signature.pc1 = stoll(value);
				break;
			case 8: signature.pc2 = stoll(value);
				break;
			case 9: signature.pc3 = stoll(value);
				break;
			case 10: signature.bhr_mispredict=stoll(value); break;
			//default: cerr<<"Unused Log Entries."<<endl;
		}

		// positions greater than 10 contain lbr information	
		int index=(pos-11)/2;
		if(pos>=11 && index<LBR_SIZE){
			// odd positions contain from register
			if(pos%2==1) signature.lbr[index].from=stoll(value);
			// even positions contain to register
			else signature.lbr[index].to=stoll(value);
		}

		//TODO the above may not be grabbing the last LBR value... come back to this?

		pos++;
	}
*/
}

void PattYehPredictor::populate_difference(accessor_t &accessor, signature_t &signature, string line0, string line1){
/*
	// Acquire the difference between lines
	// (line1 - line0)
	//
	// the accessor returned is for line1
	signature_t s0 = {0};
	signature_t s1 = {0};

	// get the line values into signature format
	if(!(line0 == "")) populate_from_string(accessor,s0,line0);
	populate_from_string(accessor,s1,line1);

	// take the difference between the signatures
	// this value is stored in the returned signature
	signature.fc0 = s1.fc0 - s0.fc0;
	signature.fc1 = s1.fc1 - s0.fc1;
	signature.fc2 = s1.fc2 - s0.fc2;
	signature.pc0 = s1.pc0 - s0.pc0;
	signature.pc1 = s1.pc1 - s0.pc1;
	signature.pc2 = s1.pc2 - s0.pc2;
	signature.pc3 = s1.pc3 - s0.pc3;

	// NOTE: computing differences for { bhr_mispredict, LBR } doesn't make sense

	//cout << "s1: " << s1.TODO << " - s0: " << s0.TODO << endl;

	// for testing
	signature.TODO = s1.TODO - s0.TODO;
*/
}
		
bool PattYehPredictor::approximate_match(unsigned long long v0, unsigned long long v1){
	//cout << "v0: " << v0 << "\t";
	//cout << "v1: " << v1 << "\t";
	//cout << "res: " << ((v1>v0) ? (v1-v0)<MATCH_WINDOW : (v0-v1)<MATCH_WINDOW) << endl;
	return ((v1>v0) ? (v1-v0)<MATCH_WINDOW : (v0-v1)<MATCH_WINDOW);
}
		
bool PattYehPredictor::mu_sigma_match(double mu, double sigma, double value){
	// compute the allowed window into which value can fall
	double window = sigma*MATCH_DEVIATION;

	//cout << "mu: " << mu << "\tsigma: " << sigma << "\tvalue: " << value << endl;

	// determine if value falls within this range
	return value > mu-window && value < mu+window;
}

//TODO invent more schemes for comparing signatures

bool PattYehPredictor::compare_signature(signature_t *self_signature, signature_t *other_signature){
/*
	// if self_signature == 0, then it was not found by the predictor. Return false.
	if(
			self_signature->fc0 == 0 &&
			self_signature->fc1 == 0 &&
			self_signature->fc2 == 0 &&
			self_signature->pc0 == 0 &&
			self_signature->pc1 == 0 &&
			self_signature->pc2 == 0 &&
			self_signature->pc3 == 0 
			) return false;

	// only the self_signature contains mu_sigma values 
	// (and vector of previous signatures
	if(!self_signature->mu_sigma_computed) compute_mu_sigma(self_signature);
*/	
	/*
	cout << "fc0: " << mu_sigma_match(self_signature->mu.fc0, self_signature->sigma.fc0, other_signature->fc0)
		<<"\tfc1: "<< mu_sigma_match(self_signature->mu.fc1, self_signature->sigma.fc1, other_signature->fc1)
		<<"\tfc2: "<< mu_sigma_match(self_signature->mu.fc2, self_signature->sigma.fc2, other_signature->fc2)
		<<"\tpc0: "<< mu_sigma_match(self_signature->mu.pc0, self_signature->sigma.pc0, other_signature->pc0)
		<<"\tpc1: "<< mu_sigma_match(self_signature->mu.pc1, self_signature->sigma.pc1, other_signature->pc1)
		<<"\tpc2: "<< mu_sigma_match(self_signature->mu.pc2, self_signature->sigma.pc2, other_signature->pc2)
		<<"\tpc3: "<< mu_sigma_match(self_signature->mu.pc3, self_signature->sigma.pc3, other_signature->pc3)
		<< endl;
	*/
/*	
	// EXPERIMENT 4
	//return mu_sigma_match(self_signature->mu.fc0, self_signature->sigma.fc0, other_signature->fc0);
	//return mu_sigma_match(self_signature->mu.fc1, self_signature->sigma.fc1, other_signature->fc1);
	//return mu_sigma_match(self_signature->mu.fc2, self_signature->sigma.fc2, other_signature->fc2);
	//return mu_sigma_match(self_signature->mu.pc0, self_signature->sigma.pc0, other_signature->pc0);
	//return mu_sigma_match(self_signature->mu.pc1, self_signature->sigma.pc1, other_signature->pc1);
	//return mu_sigma_match(self_signature->mu.pc2, self_signature->sigma.pc2, other_signature->pc2);
	//return mu_sigma_match(self_signature->mu.pc3, self_signature->sigma.pc3, other_signature->pc3);
	
	// ANY PROGRAMMABLE
	return 
		mu_sigma_match(self_signature->mu.pc0, self_signature->sigma.pc0, other_signature->pc0) ||
		mu_sigma_match(self_signature->mu.pc1, self_signature->sigma.pc1, other_signature->pc1) ||
		mu_sigma_match(self_signature->mu.pc2, self_signature->sigma.pc2, other_signature->pc2) ||
		mu_sigma_match(self_signature->mu.pc3, self_signature->sigma.pc3, other_signature->pc3);
	
	// ALL PROGRAMMABLE
	return 
		mu_sigma_match(self_signature->mu.pc0, self_signature->sigma.pc0, other_signature->pc0) &&
		mu_sigma_match(self_signature->mu.pc1, self_signature->sigma.pc1, other_signature->pc1) &&
		mu_sigma_match(self_signature->mu.pc2, self_signature->sigma.pc2, other_signature->pc2) &&
		mu_sigma_match(self_signature->mu.pc3, self_signature->sigma.pc3, other_signature->pc3);

	// ALL
	return 
		//mu_sigma_match(self_signature->mu.fc0, self_signature->sigma.fc0, other_signature->fc0) &&
		//mu_sigma_match(self_signature->mu.fc1, self_signature->sigma.fc1, other_signature->fc1) &&
		//mu_sigma_match(self_signature->mu.fc2, self_signature->sigma.fc2, other_signature->fc2) &&
		mu_sigma_match(self_signature->mu.pc0, self_signature->sigma.pc0, other_signature->pc0);
		//mu_sigma_match(self_signature->mu.pc1, self_signature->sigma.pc1, other_signature->pc1);
		//mu_sigma_match(self_signature->mu.pc2, self_signature->sigma.pc2, other_signature->pc2) &&
		//mu_sigma_match(self_signature->mu.pc3, self_signature->sigma.pc3, other_signature->pc3);

	// ANY
	return 
		mu_sigma_match(self_signature->mu.fc0, self_signature->sigma.fc0, other_signature->fc0) ||
		mu_sigma_match(self_signature->mu.fc1, self_signature->sigma.fc1, other_signature->fc1) ||
		mu_sigma_match(self_signature->mu.fc2, self_signature->sigma.fc2, other_signature->fc2) ||
		mu_sigma_match(self_signature->mu.pc0, self_signature->sigma.pc0, other_signature->pc0) || 
		mu_sigma_match(self_signature->mu.pc1, self_signature->sigma.pc1, other_signature->pc1) || 
		mu_sigma_match(self_signature->mu.pc2, self_signature->sigma.pc2, other_signature->pc2) ||
		mu_sigma_match(self_signature->mu.pc3, self_signature->sigma.pc3, other_signature->pc3);

	

	// EXPERIMENT 3		
	// only fixed counter 1
	return mu_sigma_match(self_signature->mu.fc1, self_signature->sigma.fc1, other_signature->fc1);

	// (mu, sigma) comparison ALL
	return 
		mu_sigma_match(self_signature->mu.fc0, self_signature->sigma.fc0, other_signature->fc0) &&
		mu_sigma_match(self_signature->mu.fc1, self_signature->sigma.fc1, other_signature->fc1) &&
		mu_sigma_match(self_signature->mu.fc2, self_signature->sigma.fc2, other_signature->fc2);

	// (mu, sigma) comparison ANY
	return 
		mu_sigma_match(self_signature->mu.fc0, self_signature->sigma.fc0, other_signature->fc0) ||
		mu_sigma_match(self_signature->mu.fc1, self_signature->sigma.fc1, other_signature->fc1) ||
		mu_sigma_match(self_signature->mu.fc2, self_signature->sigma.fc2, other_signature->fc2);
	
	// approximate match of ALL fixed counters
	return 
		approximate_match(self_signature->fc0, other_signature->fc0) &&
		approximate_match(self_signature->fc1, other_signature->fc1) &&
		approximate_match(self_signature->fc2, other_signature->fc2);
	
	// EXPERIMENT 1
	// approximate match of ANY fixed counters
	return 
		approximate_match(self_signature->fc0, other_signature->fc0) ||
		approximate_match(self_signature->fc1, other_signature->fc1) ||
		approximate_match(self_signature->fc2, other_signature->fc2);

	// EXPERIMENT 2
	// only use inst retired
	return 
		approximate_match(self_signature->fc0, other_signature->fc0);

	// TESTING	
	// the simpliest comparison is to expect exact equality
	return self_signature->TODO == other_signature->TODO;
*/
	return true;
}

PattYehPredictor::PattYehPredictor(){}
	
// TODO some algorithm for replacing a previously existing signature

// TODO why does signature table only contain one non-zero set of things?

//TODO self-classification is not 1.0!!! it is 0.0!!!

void PattYehPredictor::update(accessor_t* accessor, signature_t &signature){
	//TODO
	/*
	signature_t old_s;
	signature_t new_s;
	signature_accessor sa;

	sa = local_history[extract_local_accessor(accessor)];

	// update the local history
	update_local_history(accessor);

	// update the signature table
	signature.mu_sigma_computed = false;
	
	// push the signature into the vector of signatures at this location
	signature_table[sa].signature_v.push_back(signature);
	*/
}

void PattYehPredictor::predict(accessor_t* accessor, signature_t **signature){
	// what should be returned if the signature is not found?
	//TODO

	//TODO AH! this is where all the time is getting spent copying the vector!

	*signature = &signature_table[local_history[extract_local_accessor(accessor)]];

	//TODO I shouldn't need to assign each value in the struct individually should i?
	/*
	signature_t *s = &signature_table[local_history[extract_local_accessor(accessor)]];

	signature->fc0 = s->fc0;
	signature->fc1 = s->fc1;
	signature->fc2 = s->fc2;
	signature->pc0 = s->pc0;
	signature->pc1 = s->pc1;
	signature->pc2 = s->pc2;
	signature->pc3 = s->pc3;
	*/
}

//TODO this could be changed to check that only the counters we are interested in are non-zer0

bool nonzero(signature_t &signature){
/*
	return !( 
		signature.fc0==0 &&
		signature.fc1==0 &&
		signature.fc2==0 &&
		signature.pc0==0 &&
		signature.pc1==0 &&
		signature.pc2==0 &&
		signature.pc3==0
		);
*/
	return false;
}

void PattYehPredictor::train(log_data_t* l){
	string value;
	string line0="";
	string line1="";
	accessor_t accessor;// = {0,0};
	signature_t signature;// = {0};

	// go back to beginning of file
	l->file.clear();
	l->file.seekg(0, ios::beg);
	
	while(getline(l->file, value)){
		// line0 is the previous line
		line0=line1;	
		line1=value;

		// get the values from the line
		//populate_from_string(accessor, signature, value);
		populate_difference(accessor, signature, line0, line1);

		// AH!
		// nonzero() has an effect on mu, sigma because it changes the number of signatures in the vector

		//TODO This shouldn't have actually changed things because fc0 increases every line
		//	WHY? is populate difference not working?
		//
		//TODO this may not be beneficial
		// IDEA: omit anything with 0 difference from the training!
		// 	the rationale is that the PMU measurements may have been too close together
		// 	to extract any meaningful information
		// 	this will prevent signatures from having mu, sigma of 0?
		//
		//TODO why does this (nonzero) have such a large impact? IT SHOULD HAVE NONE!
		if(nonzero(signature)){
			// perform the training
			update(&accessor, signature);
		}
	}

	//cout << value << endl;	
	//cout << l->file_name << "\t" << l->pid << endl;
}

// returns true if the signature is sufficiently large as to be meaningless
bool large_mu_sigma(signature_t &signature){
/*
	// making arbitrary choice calling anything over 1 Mil large
	double LARGE = 1000000.0;
	return signature.mu.fc0 > LARGE || signature.sigma.fc0 > LARGE;
*/
	return false;
}

double PattYehPredictor::classify(log_data_t* l){
	unsigned int true_compare =0;
	unsigned int total_compare =0;
	string value;
	string line0="";
	string line1="";
	accessor_t accessor = {0,0};
	signature_t *self_signature;
	signature_t other_signature;// = {0};

	// go back to beginning of file
	l->file.clear();
	l->file.seekg(0, ios::beg);
	
	while(getline(l->file, value)){
		// line0 is the previous line
		line0=line1;	
		line1=value;

		// generate no signature for the first line
		if(line0=="") continue;

		// get the values from the line
		//populate_from_string(accessor, other_signature, value);
		populate_difference(accessor, other_signature, line0, line1);
	
		// discard lines which map to BHR 0
		//TODO what was the rationale for this?
		//if(accessor.BHR == 0) continue;

		// get the predicted signature for this accessor 
		predict(&accessor, &self_signature);
		
		//TODO consider whether discarding the following make sense
		
		// discard prediction for large (mu, sigma) they are meaningless
		//TODO this has a large effect
		if(large_mu_sigma(*self_signature)) continue;

		// discard prediction if not non-zero
		if(!nonzero(other_signature)) continue;

		// compare the signatures of self and other to see if they match
		true_compare += compare_signature(self_signature, &other_signature) ? 1 : 0;

		total_compare++;
	}

	//cout << true_compare << "    " << total_compare << endl;

	// NOTE: 
	// 	'accuracy' is not actually accuracy
	// 	instead it represents how closely the given log (l) matches the predicted signatures
	double accuracy = ((double)true_compare) / ((double)total_compare);

	// test for nan
	return accuracy!=accuracy ? 0 : accuracy;
}

#define POOP_LOCAL 1
void PattYehPredictor::poop_self(){
#ifdef POOP_LOCAL
	// print local history table
	cout << "LOCAL HISTORY TABLE" << endl;
	cout << "___________________" << endl;
	for(map<local_accessor, signature_accessor>::iterator it=local_history.begin(); 
			it != local_history.end(); ++it){
		cout << "[ " << it->first << ",\t" << it->second << " ]" << endl;
	}
	cout << "___________________" << endl << endl;
#endif

	// print signature table
	cout << "SIGNATURE TABLE" << endl;
	cout << "___________________" << endl;
	for(map<signature_accessor, signature_t>::iterator it=signature_table.begin(); 
			it != signature_table.end(); ++it){
		cout << "[ " << it->first << ",\t" << poop_signature(it->second) << " ]" << endl;
	}
	cout << "___________________" << endl;
}

//TODO rewrite poop signature
//	for testing
string poop_signature(signature_t &signature){
	stringstream ss;

	// ensure mu, sigma are computed
	if(!signature.mu_sigma_computed) compute_mu_sigma(&signature);

	ss << "[size: " << signature.signature_v.size() << "] ";

	ss << "[(mu,sigma,sigma/mu): ";
	for(int j=0;j<6;j++){
		ss<<"(";
		ss<<signature.mu[j];
		ss<<",";
		ss<<signature.sigma[j];
		ss<<",";
		ss<<signature.sigma[j] / signature.mu[j];
		ss<<"),";
	}
	ss << "]";

	return ss.str();
}
		
void PattYehPredictor::compute_mu_sigma_values(){
	signature_t *s;

	// do this for each signature in the signature table
	for(unsigned int i=0; i<signature_table.size(); ++i){
		s = &(signature_table[i]);

		// ensure mu, sigma are computed
		if(!s->mu_sigma_computed) compute_mu_sigma(s);
	}
}

// log file format:
// [signature table index, number of signatures mapped to this index, mu, sigma]
string PattYehPredictor::signature_log(){
	signature_t *s;
	stringstream ss;

	// do this for each signature in the signature table
	for(unsigned int i=0; i<signature_table.size(); ++i){
		s = &(signature_table[i]);

		// ensure mu, sigma are computed
		if(!s->mu_sigma_computed) compute_mu_sigma(s);

		// index
		ss << i;
		ss << ",";

		// number of signatures mapped to this index
		ss << s->signature_v.size();
		ss << ",";

		// counter mu values
		for(int j=0;j<6;j++){
			ss<<s->mu[j];
			ss<<",";
		}

		// counter sigma values
		for(int j=0;j<6;j++){
			ss<<s->sigma[j];
			if(j<5) ss<<",";
		}

		/*
		// mu values for all 7 counters
		ss << s->mu.fc0 << ",";
		ss << s->mu.fc1 << ",";
		ss << s->mu.fc2 << ",";
		ss << s->mu.pc0 << ",";
		ss << s->mu.pc1 << ",";
		ss << s->mu.pc2 << ",";
		ss << s->mu.pc3 << ",";

		// sigma values for all 7 counters
		ss << s->sigma.fc0 << ",";
		ss << s->sigma.fc1 << ",";
		ss << s->sigma.fc2 << ",";
		ss << s->sigma.pc0 << ",";
		ss << s->sigma.pc1 << ",";
		ss << s->sigma.pc2 << ",";
		ss << s->sigma.pc3 << ",";
		
		//TODO 
		// all these values are 0
		// this makes sense.
		// What would make sense to be logged is an aggregate value from
		// all signatures which have been alaised to this location.
		//
		// what is here right now is simply the values in the last signature to be placed
		// into the signature_v
		// in other words, 
		// the last thing to be alaised to this location
		// in the signature table
		//
		// what should be the aggregate measurement for LBR?
		// it is aggregated from all LBR registers alaised to a location.
		//TODO

		// bhr_mispredict register
		ss << s->bhr_mispredict << ",";

		// lbr values [from, to] for each register
		for(int index=0; index<LBR_SIZE; ++index){
			// from
			ss << s->lbr[index].from << ",";
			// to
			ss << s->lbr[index].to;
			// do not put a ',' on the last item
			if(i!=LBR_SIZE-1) ss << ",";
		}
		*/

		ss << endl;	
	}

	return ss.str();
}


// ARM VERSION


//TODO is this updating the signature table correctly?
//	the signature_v seem to be getting a cumulative vector
//	of all things trained by the pyp so far.
//	it is observed in the log files that their length is monotonically increasing.
void PattYehPredictor::update(accessor_t* accessor, log_line_t &l){
	signature_t old_s;
	signature_t new_s;
	signature_accessor sa;

	sa = local_history[extract_local_accessor(accessor)];

	// update the local history
	accessor->log_line=&l;
	update_local_history(accessor);

	// update the signature table
	signature_table[sa].mu_sigma_computed = false;
	
	// push the signature into the vector of signatures at this location
	signature_table[sa].signature_v.push_back(l);
}

void PattYehPredictor::train(log_line_t &l){
	// train the predictor with a single line from the log file
	
	// translates log_line_t into an accessor and signature
	accessor_t accessor;
	signature_t signature;

	//TODO does using attack site as PC make sense?
	//	note .. for negative values this will interpret it as mostly 1's
	accessor.BHR=l.branch_history;
	accessor.PC=(unsigned long long)l.attack_site;

	// update the signature table
	update(&accessor,l);
}
		
int PattYehPredictor::classify(log_line_t &l){
	// classify a line from the log file against an existing signature table
	// the classification is the attack site
	//TODO
	
	return 0; // TODO
}

string PattYehPredictor::signature_log_ml(){
	signature_t *s;
	stringstream ss;

	// do this for each signature in the signature table
	for(unsigned int i=0; i<signature_table.size(); ++i){
		s = &(signature_table[i]);

		// ensure mu, sigma are computed
		if(!s->mu_sigma_computed) compute_mu_sigma(s);

		// log data -- for each element of signature log
		for(auto const &value : s->signature_v){
			// index
			ss << i;
			ss << ",";

			// log_data_t
			ss<<value.attack_site;
			ss<<",";
			for(int j=0;j<6;j++){
				ss<<value.counter[j];
				ss<<",";
			}
			ss<<value.branch_history;
			ss<<endl;
		}
	}
	return ss.str();
}

string PattYehPredictor::mu_sigma_log(){
	signature_t *s;
	signature_t attack_s;
	signature_t non_attack_s;
	stringstream ss;

	// do this for each signature in the signature table
	for(unsigned int i=0; i<signature_table.size(); ++i){
		// clear previous vectors
		attack_s.signature_v.clear();
		non_attack_s.signature_v.clear();

		s = &(signature_table[i]);
		
		// separate signature_v into attack and non-attack
		for(auto const &value : s->signature_v){
			if(value.attack_site>=0){
				// attack
				attack_s.signature_v.push_back(value);
			}else{
				// non-attack
				non_attack_s.signature_v.push_back(value);
			}
		}

		// compute mu, sigma for each
		compute_mu_sigma(&attack_s);
		compute_mu_sigma(&non_attack_s);
			
		// log data -- non-attack
			
		// index
		ss << i;
		ss << ",";
		// attack / non-attack
		ss << 0;
		ss << ",";
		// signature vector size 
		ss<<non_attack_s.signature_v.size();
		ss<<",";
		// mu
		for(int j=0;j<6;j++){
			ss<<std::fixed<<non_attack_s.mu[j];
			ss<<",";
		}
		// sigma
		for(int j=0;j<6;j++){
			ss<<std::fixed<<non_attack_s.sigma[j];
			if(j!=5) ss<<",";
		}
		ss<<endl;

		// log data -- attack
		
		// index
		ss << i;
		ss << ",";
		// attack / non-attack
		ss << 1;
		ss << ",";
		// signature vector size 
		ss<<attack_s.signature_v.size();
		ss<<",";
		// mu
		for(int j=0;j<6;j++){
			ss<<std::fixed<<attack_s.mu[j];
			ss<<",";
		}
		// sigma
		for(int j=0;j<6;j++){
			ss<<std::fixed<<attack_s.sigma[j];
			if(j!=5) ss<<",";
		}
		ss<<endl;
	}
	return ss.str();
}

string PattYehPredictor::attack_site_mapping_log(){
	signature_t *s;
	map<int,map<int,int>> attack_site_index_count_map_map;
	stringstream ss;

	// do this for each signature in the signature table
	for(unsigned int i=0; i<signature_table.size(); ++i){
		// for a particular signature table index
		// split the signature vector into vectors for each attack site
		s = &(signature_table[i]);
		for(auto const &value : s->signature_v){
			attack_site_index_count_map_map[value.attack_site][i]+=1;
		}
	}

	// for each attack site
	double probability=0.0;
	for(auto const &attack_site_index_count_map : attack_site_index_count_map_map){
		// sum of number of mappings for this attack site	
		int count_sum=0;
		for(auto const &index_count : attack_site_index_count_map.second){
			count_sum+=index_count.second;
		}
		// for each index
		for(auto const &index_count : attack_site_index_count_map.second){
			// compute probability that an attack maps to this index
			// P(attack site x -> index i) = (# attack site x mapped to index i) / (# attack site x mappings)
			probability=(1.0*index_count.second)/(1.0*count_sum);
			// output
			ss << attack_site_index_count_map.first;
			ss << ",";
			ss << index_count.first;
			ss << ",";
			ss << probability;
			ss<<endl;
		}
	}
	return ss.str();
}
