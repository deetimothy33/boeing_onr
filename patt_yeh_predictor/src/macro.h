#ifndef MACRO_H
#define MACRO_H

#include <fstream>
#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <dirent.h>
#include <sys/stat.h>
#include <algorithm>
#include <sstream>

#include <boost/filesystem.hpp>

using namespace std;

// IDEA: make some of these things parameters to the
// PattYehPredictor constructor.
// This will enable the design space to be explored programatically.

// PID of the program who's signatures all others will be compared against
#define TRAIN_PID 0
#define LOG_FOLDER "../log"
#define TEST_LOG_FOLDER "src/test/log"
// buffer overflow
#define LOG_BUFFER_OVERFLOW_ATTACK "../log/buffer_overflow/attack"
#define LOG_BUFFER_OVERFLOW_REFERENCE "../log/buffer_overflow/reference"
// ROP
#define LOG_ROP_ATTACK "../log/rop/attack"
#define LOG_ROP_REFERENCE "../log/rop/reference"

// defines how big the match window on PYP is
#define MATCH_WINDOW 1000
// 0.2 seems to work alright (for differentiation)
#define MATCH_DEVIATION 0.25

// determine the classification threshold for 
// computing attack classification accuracy
// (in file stat.cpp)
#define CLASSIFY_THRESHOLD 0.75

typedef struct{
	string file_name;
	unsigned pid;
	ifstream file;
} log_data_t;

/**
 * get all files in directory
 */
void get_files_in_directory(vector<string> &out, const string &directory);

/**
 * create log_data_t vector given file names
 */
void create_log_data_vector(vector<log_data_t*> &v, vector<string> &sv);

/**
 * close all files in the log_data_t vector
 */
void close_all_files(vector<log_data_t*> &v);

/**
 * create directories along the path to
 * the given file
 *
 * does not create the BASENAME file
 */
void create_directory_path(string &file);

#endif
