var indexSectionsWithContent =
{
  0: "abcdfglmnprstu",
  1: "adlprs",
  2: "mps",
  3: "cglmnpstu",
  4: "bcfmpst",
  5: "ls",
  6: "bclmpst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Macros"
};

