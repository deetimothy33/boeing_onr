var searchData=
[
  ['sigma',['sigma',['../structsignature__t.html#a3f3ae66a7758c8c65205a2ff0cac9392',1,'signature_t::sigma()'],['../stat_8cpp.html#ab59819a24980aef337e9544e6cb57394',1,'sigma(vector&lt; double &gt; &amp;v, double mu):&#160;stat.cpp'],['../stat_8h.html#ab59819a24980aef337e9544e6cb57394',1,'sigma(vector&lt; double &gt; &amp;v, double mu):&#160;stat.cpp']]],
  ['signature_5faccessor',['signature_accessor',['../_patt_yeh_predictor_8h.html#ac61f7ac05b09874b6080fcdb49a6c22a',1,'PattYehPredictor.h']]],
  ['signature_5flog',['signature_log',['../class_patt_yeh_predictor.html#a65f6e62447804ddf83a24427aa3072ce',1,'PattYehPredictor::signature_log()'],['../main_8cpp.html#ac17594acac1f00c54b184fd46cf087fd',1,'SIGNATURE_LOG():&#160;main.cpp']]],
  ['signature_5flog_5ffolder_5fattack',['SIGNATURE_LOG_FOLDER_ATTACK',['../main_8cpp.html#a2f4285a024ddcb46ebaafe18d5dcc108',1,'main.cpp']]],
  ['signature_5flog_5ffolder_5freference',['SIGNATURE_LOG_FOLDER_REFERENCE',['../main_8cpp.html#adf08e2a3cb8be997e0f882d119294b28',1,'main.cpp']]],
  ['signature_5ft',['signature_t',['../structsignature__t.html',1,'signature_t'],['../_patt_yeh_predictor_8h.html#a2fe039f4569b72bb30efec7c42475efe',1,'signature_t():&#160;PattYehPredictor.h']]],
  ['signature_5fv',['signature_v',['../structsignature__t.html#a61b36fe4bfbc2eeebf3195b65fe35c85',1,'signature_t']]],
  ['stat_2ecpp',['stat.cpp',['../stat_8cpp.html',1,'']]],
  ['stat_2eh',['stat.h',['../stat_8h.html',1,'']]]
];
