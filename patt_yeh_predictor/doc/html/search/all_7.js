var searchData=
[
  ['macro_2ecpp',['macro.cpp',['../macro_8cpp.html',1,'']]],
  ['macro_2eh',['macro.h',['../macro_8h.html',1,'']]],
  ['main',['main',['../main_8cpp.html#ae66f6b31b5ad750f1fe042a706a4e3d4',1,'main.cpp']]],
  ['main_2ecpp',['main.cpp',['../main_8cpp.html',1,'']]],
  ['match_5fdeviation',['MATCH_DEVIATION',['../macro_8h.html#a2efcf11f91f16c9dac88710af2275f96',1,'macro.h']]],
  ['match_5fwindow',['MATCH_WINDOW',['../macro_8h.html#ada769b901a37a314572172ff2416da80',1,'macro.h']]],
  ['mu',['mu',['../structsignature__t.html#a2d5eb3e688893fe8e5871d9b1437e39c',1,'signature_t::mu()'],['../stat_8cpp.html#a60cd99193e5daea8ed436fdceb3b0d3d',1,'mu(vector&lt; double &gt; &amp;v):&#160;stat.cpp'],['../stat_8h.html#a60cd99193e5daea8ed436fdceb3b0d3d',1,'mu(vector&lt; double &gt; &amp;v):&#160;stat.cpp']]],
  ['mu_5fsigma_5fcomputed',['mu_sigma_computed',['../structsignature__t.html#a209d8917de71886e41f4ac0b48b2c1a7',1,'signature_t']]]
];
