var searchData=
[
  ['classify',['classify',['../class_patt_yeh_predictor.html#a90a39d379d3af6338d3272bd8fc24143',1,'PattYehPredictor']]],
  ['close_5fall_5ffiles',['close_all_files',['../macro_8cpp.html#ad38b7eb8e4e455b3cb580f2d7f994b63',1,'close_all_files(vector&lt; log_data_t *&gt; &amp;v):&#160;macro.cpp'],['../macro_8h.html#ad38b7eb8e4e455b3cb580f2d7f994b63',1,'close_all_files(vector&lt; log_data_t *&gt; &amp;v):&#160;macro.cpp']]],
  ['compute_5fmu_5fsigma',['compute_mu_sigma',['../stat_8cpp.html#a3c4d834c7f914ecf97517e29f3a2c9f3',1,'compute_mu_sigma(signature_t *s):&#160;stat.cpp'],['../stat_8h.html#a3c4d834c7f914ecf97517e29f3a2c9f3',1,'compute_mu_sigma(signature_t *s):&#160;stat.cpp']]],
  ['compute_5fper_5ffile_5faccuracy',['compute_per_file_accuracy',['../stat_8cpp.html#a687ecd1dc5d89ecc69e687f271576687',1,'compute_per_file_accuracy(report_t &amp;report):&#160;stat.cpp'],['../stat_8h.html#a687ecd1dc5d89ecc69e687f271576687',1,'compute_per_file_accuracy(report_t &amp;report):&#160;stat.cpp']]],
  ['create_5flog_5fdata_5fvector',['create_log_data_vector',['../macro_8cpp.html#a9ff9476516c3ad558bf62d322ef10d9b',1,'create_log_data_vector(vector&lt; log_data_t *&gt; &amp;v, vector&lt; string &gt; &amp;sv):&#160;macro.cpp'],['../macro_8h.html#a9ff9476516c3ad558bf62d322ef10d9b',1,'create_log_data_vector(vector&lt; log_data_t *&gt; &amp;v, vector&lt; string &gt; &amp;sv):&#160;macro.cpp']]]
];
