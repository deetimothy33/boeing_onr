var macro_8h =
[
    [ "log_data_t", "structlog__data__t.html", "structlog__data__t" ],
    [ "CLASSIFY_THRESHOLD", "macro_8h.html#a719d0174ac3b656a7e85dfec9a58d239", null ],
    [ "LOG_BUFFER_OVERFLOW_ATTACK", "macro_8h.html#a6f8385fae73cb090252d796b62f44edd", null ],
    [ "LOG_BUFFER_OVERFLOW_REFERENCE", "macro_8h.html#a2ad94dc9f13ad8fa5ef45ee33399d461", null ],
    [ "LOG_FOLDER", "macro_8h.html#a55d2d6d3e142179fde12db3235b28706", null ],
    [ "LOG_ROP_ATTACK", "macro_8h.html#a31efa9400c7caf4714980b65409ef5dd", null ],
    [ "LOG_ROP_REFERENCE", "macro_8h.html#a26460cfd6d3f7d6b0f46f8091b4fc7ec", null ],
    [ "MATCH_DEVIATION", "macro_8h.html#a2efcf11f91f16c9dac88710af2275f96", null ],
    [ "MATCH_WINDOW", "macro_8h.html#ada769b901a37a314572172ff2416da80", null ],
    [ "TEST_LOG_FOLDER", "macro_8h.html#add27ba8e0f39892b5c27c7401e66195a", null ],
    [ "TRAIN_PID", "macro_8h.html#a73051936e03f8e19b0e9f7a78af40137", null ],
    [ "close_all_files", "macro_8h.html#ad38b7eb8e4e455b3cb580f2d7f994b63", null ],
    [ "create_log_data_vector", "macro_8h.html#a9ff9476516c3ad558bf62d322ef10d9b", null ],
    [ "get_files_in_directory", "macro_8h.html#aedf09ad0fcf7d7879dcd56823004f3b1", null ]
];