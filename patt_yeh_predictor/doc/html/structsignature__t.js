var structsignature__t =
[
    [ "fc0", "structsignature__t.html#ac29d0c3c577bc0e1091c4a6ff0d27311", null ],
    [ "fc1", "structsignature__t.html#a1cc709a338166dace597f98bfdf325a2", null ],
    [ "fc2", "structsignature__t.html#aff64805ec24a400fda97c57d8875cb9f", null ],
    [ "mu", "structsignature__t.html#a2d5eb3e688893fe8e5871d9b1437e39c", null ],
    [ "mu_sigma_computed", "structsignature__t.html#a209d8917de71886e41f4ac0b48b2c1a7", null ],
    [ "pc0", "structsignature__t.html#a37d06f64b76b52fdb00637f6a7094d12", null ],
    [ "pc1", "structsignature__t.html#ae0495b406184efa90cded8570c0567a2", null ],
    [ "pc2", "structsignature__t.html#a509b1ab5db9aa3b4ce2a9fdaa5ad6415", null ],
    [ "pc3", "structsignature__t.html#a5422498e97d345a4386849e3d2b92d2e", null ],
    [ "sigma", "structsignature__t.html#a3f3ae66a7758c8c65205a2ff0cac9392", null ],
    [ "signature_v", "structsignature__t.html#a61b36fe4bfbc2eeebf3195b65fe35c85", null ],
    [ "TODO", "structsignature__t.html#a0466aa2a2cf9e501a89d143f871700c6", null ]
];