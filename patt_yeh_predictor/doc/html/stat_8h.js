var stat_8h =
[
    [ "report_t", "structreport__t.html", "structreport__t" ],
    [ "compute_mu_sigma", "stat_8h.html#a3c4d834c7f914ecf97517e29f3a2c9f3", null ],
    [ "compute_per_file_accuracy", "stat_8h.html#a687ecd1dc5d89ecc69e687f271576687", null ],
    [ "mu", "stat_8h.html#a60cd99193e5daea8ed436fdceb3b0d3d", null ],
    [ "sigma", "stat_8h.html#ab59819a24980aef337e9544e6cb57394", null ]
];